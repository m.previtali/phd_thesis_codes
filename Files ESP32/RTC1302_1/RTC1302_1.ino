/** GetDateTime.cpp
 *
 * Example of getting the date and time from the RTC.
 *
 * @version 1.0.1
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU Affero General Public License v3.0
 * @see https://github.com/Treboada/Ds1302
 *
 */
// Replace with your network credentials
const char* ssid = "hotspot";
const char* password = "hothot123";

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;


// Libraries to get time from NTP Server
#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <time.h>
#include <Arduino.h>
#include <Ds1302.h>

#define PIN_ENA 4
#define PIN_CLK 0
#define PIN_DAT 5
// DS1302 RTC instance
Ds1302 rtc(PIN_ENA, PIN_CLK, PIN_DAT);


const static char* WeekDays[] =
{
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
};



// Save reading number on RTC memory
RTC_DATA_ATTR int readingID = 0;

String dataMessage;
// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

// Variables to save date and time
String formattedDate;
String yearStamp;
String monthStamp;
String dayStamp;
String hourStamp;
String minuteStamp;
String secondStamp;


void setup(){
      Serial.begin(115200);

    // Set WiFi to station mode and disconnect from an AP if it was previously connected
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    delay(100);

    Serial.println("Setup done");

  initWiFi();
  Serial.println("");
  Serial.println("WiFi connected.");

  timeClient.begin();
  timeClient.setTimeOffset(3600);
  while(!timeClient.update()) {
    timeClient.forceUpdate();
  }
    Serial.println("Connected to the NTP Server.");

  formattedDate = timeClient.getFormattedDate();
  
  int splitT = formattedDate.indexOf("T");
  
  yearStamp = formattedDate.substring(0, 4);
  monthStamp = formattedDate.substring(5, 7);
  dayStamp = formattedDate.substring(8, splitT);
  //Serial.println(monthStamp);

  hourStamp = formattedDate.substring(splitT+1, splitT+3);
  minuteStamp = formattedDate.substring(splitT+4,splitT+6);
  secondStamp = formattedDate.substring(splitT+7, splitT+9);
  

  
   // initialize the RTC
    rtc.init();
    // test if clock is halted and set a date-time (see example 2) to start it

        Serial.println("Setting time...");

 //       Ds1302::DateTime dt = 
//           dt.year = yearStamp.toInt();
          /*
            .month = monthStamp.toInt(),//Ds1302::MONTH_OCT,
            .day = dayStamp.toInt(),
            .hour = hourStamp.toInt(),
            .minute = minuteStamp.toInt(),
            .second = secondStamp.toInt(),
            .dow = Ds1302::DOW_TUE
        */
  //      Serial.println(dt.year);
   //     rtc.setDateTime(&dt);

    
        // structure to manage date-time
        Ds1302::DateTime dt;

        dt.year = yearStamp.toInt();
        dt.month = monthStamp.toInt();//Ds1302::MONTH_OCT,
        dt.day = dayStamp.toInt();
        dt.hour = hourStamp.toInt();
        dt.minute = minuteStamp.toInt();
        dt.second = secondStamp.toInt();
        dt.dow = Ds1302::DOW_MON; //it's always tuesday

        // set the date and time
        rtc.setDateTime(&dt);



    
}
uint8_t parseDigits(char* str, uint8_t count)
{
    uint8_t val = 0;
    while(count-- > 0) val = (val * 10) + (*str++ - '0');
    return val;
}


void loop()
{
    // get the current time
    Ds1302::DateTime now;
    rtc.getDateTime(&now);

    static uint8_t last_second = 0;
    if (last_second != now.second)
    {
        last_second = now.second;

        Serial.print("20");
        Serial.print(now.year-8);    // it's not 2030 wtf
        Serial.print('-');
        if (now.month < 10) Serial.print('0');
        Serial.print(now.month);   // 01-12
        Serial.print('-');
        if (now.day < 10) Serial.print('0');
        Serial.print(now.day);     // 01-31
        Serial.print(' ');
        Serial.print(WeekDays[now.dow - 1]); // 1-7
        Serial.print(' ');
        if (now.hour < 10) Serial.print('0');
        Serial.print(now.hour);    // 00-23
        Serial.print(':');
        if (now.minute < 10) Serial.print('0');
        Serial.print(now.minute);  // 00-59
        Serial.print(':');
        if (now.second < 10) Serial.print('0');
        Serial.print(now.second);  // 00-59
        Serial.println();
    }

    delay(100);
}


void initWiFi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("Connecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println(WiFi.localIP());
}
