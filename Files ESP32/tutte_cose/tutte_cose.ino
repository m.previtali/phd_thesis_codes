#include <time.h>
#include <Arduino.h>
#include <Ds1302.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <Process.h>
#include <H3LIS331DL.h>
#include <Wire.h>

File file;

const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;
String fname;

// CLOCK PIN VARIABLES
#define PIN_ENA 4
#define PIN_CLK 0
#define PIN_DAT 5

// pins for the leds and switch
const byte led_gpio_i2c = 32;
const byte led_gpio_rec = 33;
int switch_gpio = 25;

// bool to tell whether to record or not
int bool_record = 0;
bool is_recording = false;

const int ITG_3701=0x68;  // I2C address of the ITG_3701

// DS1302 RTC instance
Ds1302 rtc(PIN_ENA, PIN_CLK, PIN_DAT);
const static char* WeekDays[] =
{
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
};


// Save reading number on RTC memory
RTC_DATA_ATTR int readingID = 0;

// Variables to save date and time
String formattedDate;
String yearStamp;
String monthStamp;
String dayStamp;
String hourStamp;
String minuteStamp;
String secondStamp;


// ACCELEROMETER VARIABLES
H3LIS331DL h3lis;

//please get these value by running H3LIS331DL_AdjVal Sketch.
#define VAL_X_AXIS  -81
#define VAL_Y_AXIS  -140
#define VAL_Z_AXIS  218


////////////////////////////
// ITG3701 Gyro Registers //
////////////////////////////
#define ITG3701_XG_OFFS_TC_H     0x04
#define ITG3701_XG_OFFS_TC_L     0x05
#define ITG3701_YG_OFFS_TC_H     0x07
#define ITG3701_YG_OFFS_TC_L     0x08
#define ITG3701_ZG_OFFS_TC_H     0x0A
#define ITG3701_ZG_OFFS_TC_L     0x0B
#define ITG3701_XG_OFFS_USRH     0x13  // User-defined trim values for gyroscope
#define ITG3701_XG_OFFS_USRL     0x14
#define ITG3701_YG_OFFS_USRH     0x15
#define ITG3701_YG_OFFS_USRL     0x16
#define ITG3701_ZG_OFFS_USRH     0x17
#define ITG3701_ZG_OFFS_USRL     0x18
#define ITG3701_SMPLRT_DIV       0x19
#define ITG3701_CONFIG           0x1A
#define ITG3701_GYRO_CONFIG      0x1B
#define ITG3701_FIFO_EN          0x23
#define ITG3701_INT_PIN_CFG      0x37
#define ITG3701_INT_ENABLE       0x38
#define ITG3701_INT_STATUS       0x3A
#define ITG3701_TEMP_OUT_H       0x41
#define ITG3701_TEMP_OUT_L       0x42
#define ITG3701_GYRO_XOUT_H      0x43
#define ITG3701_GYRO_XOUT_L      0x44
#define ITG3701_GYRO_YOUT_H      0x45
#define ITG3701_GYRO_YOUT_L      0x46
#define ITG3701_GYRO_ZOUT_H      0x47
#define ITG3701_GYRO_ZOUT_L      0x48
#define ITG3701_USER_CTRL        0x6A  
#define ITG3701_PWR_MGMT_1       0x6B // Device defaults to the SLEEP mode
#define ITG3701_PWR_MGMT_2       0x6C
#define ITG3701_FIFO_COUNTH      0x72
#define ITG3701_FIFO_COUNTL      0x73
#define ITG3701_FIFO_R_W         0x74
#define ITG3701_WHO_AM_I         0x75 // Should return 0x68
//#define ITG3701_WHO_AM_I         0x68 // Should return 0x68

// Using the ITG3701+LSM303D+MS5637 Teensy 3.1 Add-On shield, ADO is set to 0 
#define ADO 0
#if ADO
#define ITG3701_ADDRESS   0x69 // Address of gyro when ADO = 1
#else 
#define ITG3701_ADDRESS   0x68 // Address of gyro when ADO = 0
#endif

#define SerialDebug true  // set to true to get Serial output for debugging


enum Aodr {  // set of allowable gyro sample rates
  AODR_PowerDown = 0,
  AODR_3_125Hz,
  AODR_6_25Hz,
  AODR_12_5Hz,
  AODR_25Hz,
  AODR_50Hz,
  AODR_100Hz,
  AODR_200Hz,
  AODR_400Hz,
  AODR_800Hz,
  AODR_1600Hz
};


enum Gscale {
  GFS_500DPS = 0,
  GFS_1000DPS,
  GFS_2000DPS,
  GFS_4000DPS
};


enum Godr {  // set of allowable gyro sample rates
  GODR_95Hz = 0,
  GODR_190Hz,
  GODR_380Hz,
  GODR_760Hz
};

enum Gbw {   // set of allowable gyro data bandwidths
  GBW_low = 0,  // 12.5 Hz at Godr = 95 Hz, 12.5 Hz at Godr = 190 Hz,  30 Hz at Godr = 760 Hz
  GBW_med,      // 25 Hz   at Godr = 95 Hz, 25 Hz   at Godr = 190 Hz,  35 Hz at Godr = 760 Hz
  GBW_high,     // 25 Hz   at Godr = 95 Hz, 50 Hz   at Godr = 190 Hz,  50 Hz at Godr = 760 Hz
  GBW_highest   // 25 Hz   at Godr = 95 Hz, 70 Hz   at Godr = 190 Hz, 100 Hz at Godr = 760 Hz
};

// Specify sensor full scale
uint8_t Gscale = GFS_500DPS; // gyro full scale
uint8_t Godr = GODR_190Hz;   // gyro data sample rate
uint8_t Gbw = GBW_low;       // gyro data bandwidth
float gRes;      // scale resolutions per LSB for the sensors
  


unsigned char nCRC;       // calculated check sum to ensure PROM integrity
double dT, OFFSET, SENS,OFFSET2, SENS2;  // First order and second order corrections for raw S5637 temperature and pressure data
int16_t gyroCount[3];  // Stores the 16-bit signed accelerometer, gyro, and mag sensor output
float gyroBias[3] = {0, 0, 0}, accelBias[3] = {0, 0, 0},  magBias[3] = {0, 0, 0}; // Bias corrections for gyro, accelerometer, and magnetometer
float SelfTest[3] = {0., 0., 0.};

// global constants for 9 DoF fusion and AHRS (Attitude and Heading Reference System)
float GyroMeasError = PI * (40.0f / 180.0f);   // gyroscope measurement error in rads/s (start at 40 deg/s)
float GyroMeasDrift = PI * (0.0f  / 180.0f);   // gyroscope measurement drift in rad/s/s (start at 0.0 deg/s/s)
// There is a tradeoff in the beta parameter between accuracy and response speed.
// In the original Madgwick study, beta of 0.041 (corresponding to GyroMeasError of 2.7 degrees/s) was found to give optimal accuracy.
// However, with this value, the LSM9SD0 response time is about 10 seconds to a stable initial quaternion.
// Subsequent changes also require a longish lag time to a stable output, not fast enough for a quadcopter or robot car!
// By increasing beta (GyroMeasError) by about a factor of fifteen, the response time constant is reduced to ~2 sec
// I haven't noticed any reduction in solution accuracy. This is essentially the I coefficient in a PID control sense; 
// the bigger the feedback coefficient, the faster the solution converges, usually at the expense of accuracy. 
// In any case, this is the free parameter in the Madgwick filtering and fusion scheme.
float beta = sqrt(3.0f / 4.0f) * GyroMeasError;   // compute beta
float zeta = sqrt(3.0f / 4.0f) * GyroMeasDrift;   // compute zeta, the other free parameter in the Madgwick scheme usually set to a small or zero value
#define Kp 2.0f * 5.0f // these are the free parameters in the Mahony filter and fusion scheme, Kp for proportional feedback, Ki for integral
#define Ki 0.0f

uint32_t delt_t = 0, count = 0, sumCount = 0;  // used to control display output rate
float pitch, yaw, roll;
float deltat = 0.0f, sum = 0.0f;          // integration interval for both filter schemes
uint32_t lastUpdate = 0, firstUpdate = 0; // used to calculate integration interval
uint32_t Now = 0;                         // used to calculate integration interval

float ax, ay, az, gx, gy, gz, mx, my, mz; // variables to hold latest sensor data values 
float q[4] = {1.0f, 0.0f, 0.0f, 0.0f};    // vector to hold quaternion
float eInt[3] = {0.0f, 0.0f, 0.0f};       // vector to hold integral error for Mahony method




void setup()
{
  // init I2C
    Wire.begin();
    Wire.beginTransmission(ITG_3701);
    delay(400);
      Serial.begin(115200);

   if(!SD.begin(5)){
    Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();

  if(cardType == CARD_NONE){
    Serial.println("No SD card attached");
    return;
  }

  Serial.print("SD Card Type: ");
  if(cardType == CARD_MMC){
    Serial.println("MMC");
  } else if(cardType == CARD_SD){
    Serial.println("SDSC");
  } else if(cardType == CARD_SDHC){
    Serial.println("SDHC");
  } else {
    Serial.println("UNKNOWN");
  }

  uint64_t cardSize = SD.cardSize() / (1024 * 1024);
  Serial.printf("SD Card Size: %lluMB\n", cardSize);


  // init the leds and switch
  const byte led_gpio_i2c = 32;
  const byte led_gpio_rec = 33;
  int switch_gpio = 25;
  pinMode(led_gpio_i2c,OUTPUT);
  pinMode(led_gpio_rec,OUTPUT);
  pinMode(switch_gpio,INPUT);
  

  while (!Serial){}

  byte error, address;
  int nDevices;
  nDevices = 0;
  for(address = 1; address < 127; address++ ) {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0) {
      nDevices++;
    }  
  }
  if (nDevices>0) 
  digitalWrite(led_gpio_i2c, HIGH);   


  // Read the WHO_AM_I registers, this is a good test of communication
  byte c = readByte(ITG3701_ADDRESS, ITG3701_WHO_AM_I);  // Read WHO_AM_I register for ITG3701 gyro


  // init the accelerometer
    h3lis.init();
    h3lis.importPara(VAL_X_AXIS, VAL_Y_AXIS, VAL_Z_AXIS);

    
  // init the gyroscope
   initITG3701(); 
 
   // get sensor resolutions, only need to do this once
  getGres();
  gyrocalITG3701(gyroBias); 
}

void loop()
{  

  bool_record = digitalRead(switch_gpio);

  if (bool_record){
 //   Serial.println("im recording im recording");
    if (!is_recording){
  File d = SD.open( "/" );
  int count_files = 0;
  
  while( true )
  {
    File entry =  d.openNextFile();
    if( !entry )
    {
      break;
    }
    String file_name = entry.name();  //Get file name so that we can check 
              //if it's a duplicate
    if( file_name.indexOf('~') != 0 )   //Igrnore filenames with a ~. It's a mac thing.
    {         //Just don't have file names that have a ~ in them
      count_files++;
    }
  }
    fname = "/"+String(count_files+1) +".txt";
    digitalWrite(led_gpio_rec, HIGH);
    is_recording = true;
    char copy[10];


  fname.toCharArray(copy, 8);
  writeFile(SD, copy, "year;month;day;hour;minute;second;millis;gx;gy;gz;ax;ay;az\n ");


  }
  }
  else if (is_recording){
      is_recording = false;
      file.close();
      digitalWrite(led_gpio_rec, LOW);
    Serial.println("Stopped recording");
}


  if (readByte(ITG3701_ADDRESS, ITG3701_INT_STATUS) & 0x01) {  // check if new gyro data is ready  
    readGyroData(gyroCount);  // Read the x/y/z adc values

    // Calculate the gyro value into actual degrees per second
    gx = (float)gyroCount[0]*gRes - gyroBias[0];  // get actual gyro value, this depends on scale being set
    gy = (float)gyroCount[1]*gRes - gyroBias[1];  
    gz = (float)gyroCount[2]*gRes - gyroBias[2];   
  }
  

  
  Now = micros();
  deltat = ((Now - lastUpdate)/1000000.0f); // set integration time by time elapsed since last filter update
  lastUpdate = Now;

  sum += deltat; // sum for averaging filter update rate
  sumCount++;

    double xyz[3];
    h3lis.getAcceleration(xyz);




    Ds1302::DateTime now;
    rtc.getDateTime(&now);
    int milliseconds = millis();


    char buffer1[50], buffer2[22], sgx[15],sgy[15],sgz[15],sax[15],say[15],saz[15];
    char mega_buffer[250];
    sprintf( buffer2, "%u;%u;%u;%u;%u;%u;%u;",now.year,now.month, now.day,now.hour, now.minute, now.second, milliseconds);
    dtostrf(gx,6,6,sgx);
    dtostrf(gy,6,6,sgy);
    dtostrf(gz,6,6,sgz);
    dtostrf(xyz[0],6,6,sax);
    dtostrf(xyz[1],6,6,say);
    dtostrf(xyz[2],6,6,saz);
    strcpy(mega_buffer,"");
    strcat(mega_buffer,sgx);
    strcat(mega_buffer,";");
    strcat(mega_buffer,sgy);
    strcat(mega_buffer,";");
    strcat(mega_buffer,sgz);
    strcat(mega_buffer,";");
    strcat(mega_buffer,sax);
    strcat(mega_buffer,";");
    strcat(mega_buffer,say);
    strcat(mega_buffer,";");
    strcat(mega_buffer,saz);
    strcat(mega_buffer,"\n");

        if (is_recording){

       char copy[7];

  fname.toCharArray(copy, 8);
        appendFile(SD, copy, buffer2);
        appendFile(SD, copy, mega_buffer);
            Serial.println("recording..");
  }

//  Serial.println("bue");

}


//===================================================================================================================
//====== Set of useful function to access gyroscope data
//===================================================================================================================


void getGres() {
  switch (Gscale)
  {
  // Possible gyro scales (and their register bit settings) are:
  // 500 DPS (00), 1000 DPS (01), 2000 DPS (10) and 4000 DPS  (11). 
    case GFS_500DPS:
          gRes = 500.0/32768.0;
          break;
    case GFS_1000DPS:
          gRes = 1000.0/32768.0;
          break;
    case GFS_2000DPS:
          gRes = 2000.0/32768.0;
          break;
    case GFS_4000DPS:
          gRes = 4000.0/32768.0;
          break;
  }
}



void readGyroData(int16_t * destination)
{
  uint8_t rawData[6];  // x/y/z gyro register data stored here
  readBytes(ITG3701_ADDRESS, ITG3701_GYRO_XOUT_H, 6, &rawData[0]);  // Read the six raw data registers sequentially into data array
  destination[0] = ((int16_t)rawData[0] << 8) | rawData[1] ;  // Turn the MSB and LSB into a signed 16-bit value
  destination[1] = ((int16_t)rawData[2] << 8) | rawData[3] ;  
  destination[2] = ((int16_t)rawData[4] << 8) | rawData[5] ; 
}
 
void initITG3701()
{  
 // wake up device
  writeByte(ITG3701_ADDRESS, ITG3701_PWR_MGMT_1, 0x00); // Clear sleep mode bit (6), enable all sensors 
  delay(100); // Wait for all registers to reset 

 // get stable time source
  writeByte(ITG3701_ADDRESS, ITG3701_PWR_MGMT_1, 0x01);  // Auto select clock source to be PLL gyroscope reference if ready else
  delay(200); 
  
 // Configure Gyro and Thermometer
 // Disable FSYNC and set thermometer and gyro bandwidth to 41 and 42 Hz, respectively; 
 // minimum delay time for this setting is 5.9 ms, which means sensor fusion update rates cannot
 // be higher than 1 / 0.0059 = 170 Hz
 // DLPF_CFG = bits 2:0 = 011; this limits the sample rate to 1000 Hz for both
 // With the ITG3701, it is possible to get gyro sample rates of 32 kHz (!), 8 kHz, or 1 kHz
  writeByte(ITG3701_ADDRESS, ITG3701_CONFIG, 0x03);  

 // Set sample rate = gyroscope output rate/(1 + SMPLRT_DIV)
  writeByte(ITG3701_ADDRESS, ITG3701_SMPLRT_DIV, 0x04);  // Use a 200 Hz rate; a rate consistent with the filter update rate 
                                    // determined inset in CONFIG above
 
 // Set gyroscope full scale range
 // Range selects FS_SEL and AFS_SEL are 0 - 3, so 2-bit values are left-shifted into positions 4:3
  uint8_t c = readByte(ITG3701_ADDRESS, ITG3701_GYRO_CONFIG);
//  writeRegister(GYRO_CONFIG, c & ~0xE0); // Clear self-test bits [7:5] 
  writeByte(ITG3701_ADDRESS, ITG3701_GYRO_CONFIG, c & ~0x02); // Clear Fchoice bits [1:0] 
  writeByte(ITG3701_ADDRESS, ITG3701_GYRO_CONFIG, c & ~0x18); // Clear AFS bits [4:3]
  writeByte(ITG3701_ADDRESS, ITG3701_GYRO_CONFIG, c | Gscale << 3); // Set full scale range for the gyro
 // writeRegister(GYRO_CONFIG, c | 0x00); // Set Fchoice for the gyro to 11 by writing its inverse to bits 1:0 of GYRO_CONFIG

 // Configure Interrupts and Bypass Enable
  // Set interrupt pin active high, push-pull, hold interrupt pin level HIGH until interrupt cleared,
  // clear on read of INT_STATUS, and enable I2C_BYPASS_EN so additional chips 
  // can join the I2C bus and all can be controlled by the Arduino as master
   writeByte(ITG3701_ADDRESS, ITG3701_INT_PIN_CFG, 0x20);    
   writeByte(ITG3701_ADDRESS, ITG3701_INT_ENABLE, 0x01);  // Enable data ready (bit 0) interrupt
   delay(100);
}


// Function which accumulates gyro data after device initialization. It calculates the average
// of the at-rest readings and then loads the resulting offsets into gyro bias registers.
void gyrocalITG3701(float * dest1)
{  
  uint8_t data[6]; // data array to hold gyro x, y, z, data
  uint16_t ii, packet_count, fifo_count;
  int32_t gyro_bias[3]  = {0, 0, 0};
  
 // reset device
  writeByte(ITG3701_ADDRESS, ITG3701_PWR_MGMT_1, 0x80); // Write a one to bit 7 reset bit; toggle reset device
  delay(100);
   
 // get stable time source; Auto select clock source to be PLL gyroscope reference if ready 
 // else use the internal oscillator, bits 2:0 = 001
  writeByte(ITG3701_ADDRESS, ITG3701_PWR_MGMT_1, 0x01);  
  writeByte(ITG3701_ADDRESS, ITG3701_PWR_MGMT_2, 0x00);
  delay(200);                                    

// Configure device for bias calculation
  writeByte(ITG3701_ADDRESS, ITG3701_INT_ENABLE, 0x00);   // Disable all interrupts
  writeByte(ITG3701_ADDRESS, ITG3701_FIFO_EN, 0x00);      // Disable FIFO
  writeByte(ITG3701_ADDRESS, ITG3701_PWR_MGMT_1, 0x00);   // Turn on internal clock source
  writeByte(ITG3701_ADDRESS, ITG3701_USER_CTRL, 0x00);    // Disable FIFO 
  writeByte(ITG3701_ADDRESS, ITG3701_USER_CTRL, 0x04);    // Reset FIFO 
  delay(15);
  
// Configure ITG3701 gyro for bias calculation
  writeByte(ITG3701_ADDRESS, ITG3701_CONFIG, 0x01);      // Set low-pass filter to 184 Hz
  writeByte(ITG3701_ADDRESS, ITG3701_SMPLRT_DIV, 0x00);  // Set sample rate to 1 kHz
  writeByte(ITG3701_ADDRESS, ITG3701_GYRO_CONFIG, 0x00); // Set gyro full-scale to 500 degrees per second, maximum sensitivity
 
  // Configure FIFO to capture accelerometer and gyro data for bias calculation
  writeByte(ITG3701_ADDRESS, ITG3701_USER_CTRL, 0x40);   // Enable FIFO  
  writeByte(ITG3701_ADDRESS, ITG3701_FIFO_EN, 0x70);     // Enable gyro sensors for FIFO  (max size 512 bytes in ITG3701)
  delay(100); // accumulate 80 samples in 80 milliseconds = 480 bytes

  // At end of sample accumulation, turn off FIFO sensor read
  writeByte(ITG3701_ADDRESS, ITG3701_FIFO_EN, 0x00);        // Disable gyro sensors for FIFO
  readBytes(ITG3701_ADDRESS, ITG3701_FIFO_COUNTH, 2, &data[0]); // read FIFO sample count
  fifo_count = (uint16_t)((uint16_t)data[0] << 8) | data[1];
//  packet_count = fifo_count/6; // How many sets of full gyro data for averaging
  packet_count = 128; // How many sets of full gyro data for averaging

  for (ii = 0; ii < packet_count; ii++) {
    int16_t gyro_temp[3] = {0, 0, 0};
//    readBytes(ITG3701_ADDRESS, ITG3701_FIFO_R_W, 6, &data[0]); // read data for averaging
    readBytes(ITG3701_ADDRESS, ITG3701_GYRO_XOUT_H, 6, &data[0]); // read data for averaging
    gyro_temp[0]  = (int16_t) (((int16_t)data[0] << 8) | data[1]) ;
    gyro_temp[1]  = (int16_t) (((int16_t)data[2] << 8) | data[3]) ;
    gyro_temp[2]  = (int16_t) (((int16_t)data[4] << 8) | data[5]) ;
    
    gyro_bias[0]  += (int32_t) gyro_temp[0];
    gyro_bias[1]  += (int32_t) gyro_temp[1];
    gyro_bias[2]  += (int32_t) gyro_temp[2];
    delay(5);
}
    gyro_bias[0]  /= (int32_t) packet_count;
    gyro_bias[1]  /= (int32_t) packet_count;
    gyro_bias[2]  /= (int32_t) packet_count;
      
// Construct the gyro biases for push to the hardware gyro bias registers, which are reset to zero upon device startup
  data[0] = (-gyro_bias[0]  >> 8) & 0xFF;  
  data[1] = (-gyro_bias[0])       & 0xFF; // Biases are additive, so change sign on calculated average gyro biases
  data[2] = (-gyro_bias[1]  >> 8) & 0xFF;
  data[3] = (-gyro_bias[1])       & 0xFF;
  data[4] = (-gyro_bias[2]  >> 8) & 0xFF;
  data[5] = (-gyro_bias[2])       & 0xFF;
  
// Push gyro biases to hardware registers
//  writeByte(ITG3701_ADDRESS, ITG3701_XG_OFFS_USRH, data[0]);
//  writeByte(ITG3701_ADDRESS, ITG3701_XG_OFFS_USRL, data[1]);
//  writeByte(ITG3701_ADDRESS, ITG3701_YG_OFFS_USRH, data[2]);
//  writeByte(ITG3701_ADDRESS, ITG3701_YG_OFFS_USRL, data[3]);
//  writeByte(ITG3701_ADDRESS, ITG3701_ZG_OFFS_USRH, data[4]);
//  writeByte(ITG3701_ADDRESS, ITG3701_ZG_OFFS_USRL, data[5]);
  
// Output scaled gyro biases for display in the main program
  dest1[0] = (float) gyro_bias[0]*gRes;  
  dest1[1] = (float) gyro_bias[1]*gRes;
  dest1[2] = (float) gyro_bias[2]*gRes;
}


// I2C read/write functions for the sensors

  void writeByte(uint8_t address, uint8_t subAddress, uint8_t data)
{
  Wire.beginTransmission(address);  // Initialize the Tx buffer
  Wire.write(subAddress);           // Put slave register address in Tx buffer
  Wire.write(data);                 // Put data in Tx buffer
  Wire.endTransmission(false);           // Send the Tx buffer
}

uint8_t readByte(uint8_t address, uint8_t subAddress)
{
  uint8_t data; // `data` will store the register data   
  Wire.beginTransmission(address);         // Initialize the Tx buffer
  Wire.write(subAddress);                  // Put slave register address in Tx buffer
  Wire.endTransmission(false);        // Send the Tx buffer, but send a restart to keep connection alive
//  Wire.endTransmission(false);             // Send the Tx buffer, but send a restart to keep connection alive
//  Wire.requestFrom(address, 1);  // Read one byte from slave register address 
  Wire.requestFrom(address, (size_t) 1);   // Read one byte from slave register address 
  data = Wire.read();                      // Fill Rx buffer with result
  return data;                             // Return data read from slave register
}

  void readBytes(uint8_t address, uint8_t subAddress, uint8_t count, uint8_t * dest)
{  
  Wire.beginTransmission(address);   // Initialize the Tx buffer
  Wire.write(0x80 | subAddress);     // Put slave register address in Tx buffer, include 0x80 for LSM303D multiple byte read
  Wire.endTransmission(false);  // Send the Tx buffer, but send a restart to keep connection alive
//  Wire.endTransmission(false);       // Send the Tx buffer, but send a restart to keep connection alive
  uint8_t i = 0;
//        Wire.requestFrom(address, count);  // Read bytes from slave register address 
        Wire.requestFrom(address, (size_t) count);  // Read bytes from slave register address 
  while (Wire.available()) {
        dest[i++] = Wire.read(); }         // Put read results in the Rx buffer
}

uint8_t parseDigits(char* str, uint8_t count)
{
    uint8_t val = 0;
    while(count-- > 0) val = (val * 10) + (*str++ - '0');
    return val;
}



// SD CARD FUNCTIONS

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if(!root){
    Serial.println("Failed to open directory");
    return;
  }
  if(!root.isDirectory()){
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while(file){
    if(file.isDirectory()){
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if(levels){
        listDir(fs, file.name(), levels -1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.println(file.size());
    }
    file = root.openNextFile();
  }
}

void readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\n", path);

  File file = fs.open(path);
  if(!file){
    Serial.println("Failed to open file for reading");
    return;
  }

  Serial.print("Read from file: ");
  while(file.available()){
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if(!file){
    Serial.println("Failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if(!file){
    Serial.println("Failed to open file for appending");
    return;
  }
  if(file.print(message)){
      Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}

void renameFile(fs::FS &fs, const char * path1, const char * path2){
  Serial.printf("Renaming file %s to %s\n", path1, path2);
  if (fs.rename(path1, path2)) {
    Serial.println("File renamed");
  } else {
    Serial.println("Rename failed");
  }
}

void deleteFile(fs::FS &fs, const char * path){
  Serial.printf("Deleting file: %s\n", path);
  if(fs.remove(path)){
    Serial.println("File deleted");
  } else {
    Serial.println("Delete failed");
  }
}
