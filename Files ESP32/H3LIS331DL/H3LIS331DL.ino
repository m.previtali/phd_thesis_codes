/*  get accelerate data of H3LIS331DL
    Auth : lawliet(lawliet.zou@gmail.com)
    version : 0.1
*/

#include <H3LIS331DL.h>
#include <Wire.h>

//please get these value by running H3LIS331DL_AdjVal Sketch.
#define VAL_X_AXIS  -81
#define VAL_Y_AXIS  -140
#define VAL_Z_AXIS  218

H3LIS331DL h3lis;

void setup() {
    Serial.begin(9600);
    h3lis.init();
    h3lis.importPara(VAL_X_AXIS, VAL_Y_AXIS, VAL_Z_AXIS);
}

void loop() {
  findI2C();
  //Serial.println(H3LIS331DL_MEMS_I2C_ADDRESS,HEX);
    int16_t x, y, z;
    h3lis.readXYZ(&x, &y, &z);
    Serial.print("x, y, z = ");
    Serial.print(x);
    Serial.print("\t");
    Serial.print(y);
    Serial.print("\t");
    Serial.println(z);

    double xyz[3];
    h3lis.getAcceleration(xyz);
    Serial.print("accelerate of x, y, z = ");
    Serial.print(xyz[0]);
    Serial.print("g");
    Serial.print("\t");
    Serial.print(xyz[1]);
    Serial.print("g");
    Serial.print("\t");
    Serial.print(xyz[2]);
    Serial.println("g");

    delay(2500);
}

void findI2C(){
   byte error, address;
  int nDevices;
  Serial.println("Scanning...");
  nDevices = 0;
  for(address = 1; address < 127; address++ ) {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0) {
      Serial.print("I2C device found at address 0x");
      if (address<16) {
        Serial.print("0");
      }
      Serial.println(address,HEX);
      nDevices++;
    }
    else if (error==4) {
      Serial.print("Unknow error at address 0x");
      if (address<16) {
        Serial.print("0");
      }
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0) {
    Serial.println("No I2C devices found");
  }
  else {
    Serial.println("done");
  }
  
  
  }
