from builtins import range
from yade import pack, qt, plot
import math as ma, numpy as np
import matplotlib.pyplot as plt
from yade.gridpfacet import *
import sys

def checkUnbalanced():
	print('unbalancedForce: ' + str(unbalancedForce()))
	if unbalancedForce()<0.05:
		O.engines[-1].dead=True
		O.save('ring_wire_balanced.yade.gz')
        O.pause()
	e#xit
        sys.exit()

ring_vert_b = 0.0714 #[m] # triangle shape, main axis
ring_vert_t = 0.0284 #[m] # circular shape, vertically oriented radius
ring_horiz  = 0.065  #[m] # circular shape, horizontal diameter
        

steel_density = 8050    #[kg/m3]
steel_poisson = 0.265   #[-]
steel_young = 301.949801453*1e6/0.0147744945568 # from thoeni et al. 2013, i consider only the first segment since strain is very limited
contact_fric = 0.6

selvedge_thickness = 0.018      #[m]
selvedge_horizDist = 0.08       #[m] 0.08 for vertical setups, 0.12 for horizontal ones
square_wire_thickness = 0.01    #[m] do i even need this
ring_wire_thickness = 0.014     #[m]
selvedge_section_area = ma.pi*(selvedge_thickness/2)**2

number_balls_wire = 4           #[-] an higher value causes issues

ball_rad = ring_wire_thickness/2
angle = ma.atan(ring_vert_b/(ring_horiz/2))
dist = ma.sqrt((ring_horiz/2)**2+ring_vert_b**2)
num_balls_bottom = int(dist/ball_rad/2)
 

angle = ma.atan(ring_vert_b/(ring_horiz/2))
dist = ma.sqrt((ring_horiz/2)**2+ring_vert_b**2)
num_balls_bottom = int(dist/ball_rad/2)

arc_length = ma.pi * ring_horiz/2
num_balls_top = int(arc_length/ball_rad/2)-1

top_half_ring_volume = (ring_horiz/4*ma.pi*2)*(ma.pi*(ring_wire_thickness/2)**2)
bottom_half_ring_volume = (ma.pi*(ring_wire_thickness/2)**2)*(ring_horiz**2 + (2*ring_vert_b)**2)**0.5

weight_top_ring = top_half_ring_volume * steel_density
weight_bottom_ring = bottom_half_ring_volume * steel_density

total_ring_weight = weight_top_ring + weight_bottom_ring
total_ring_num_balls = num_balls_top + num_balls_bottom

ring_ball_density = (total_ring_weight/total_ring_num_balls)/((4.0/3)*ma.pi*(ring_wire_thickness/2)**3)




#### Engines ####
O.engines=[
	ForceResetter(),
	InsertionSortCollider([Bo1_GridConnection_Aabb(),Bo1_Sphere_Aabb()]),
	InteractionLoop(
		[		
		Ig2_Sphere_Sphere_ScGeom(),
		Ig2_Box_Sphere_ScGeom(),
		Ig2_GridNode_GridNode_GridNodeGeom6D(),
		Ig2_Sphere_GridConnection_ScGridCoGeom(),
		Ig2_GridConnection_GridConnection_GridCoGridCoGeom()
		],
		[Ip2_CohFrictMat_CohFrictMat_CohFrictPhys(setCohesionNow=True,setCohesionOnNewContacts=False),Ip2_FrictMat_FrictMat_FrictPhys()],
		[
		Law2_ScGeom_FrictPhys_CundallStrack(),	# contact law for sphere-sphere
		Law2_ScGridCoGeom_FrictPhys_CundallStrack(),	# contact law for cylinder-sphere
		Law2_ScGeom6D_CohFrictPhys_CohesionMoment(),	# contact law for "internal" cylinder forces
		Law2_GridCoGridCoGeom_FrictPhys_CundallStrack()	# contact law for cylinder-cylinder interaction
		]
	),
	GlobalStiffnessTimeStepper(timestepSafetyCoefficient=0.1,label='ts'), 
	NewtonIntegrator(gravity=(0,0,-9.81),damping=0.5,label='newton'),
	PyRunner(command='checkUnbalanced()',realPeriod=1)
]


O.materials.append(CohFrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=10,normalCohesion=1e20,shearCohesion=1e20,momentRotationLaw=True,label='cableMat'))
O.materials.append(FrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=ma.tan(contact_fric),label='contactMat')) 

nodesIds=[]
cylIds=[]

myWire = []
for i in range(0,number_balls_wire+1):
    y = - selvedge_horizDist/2 + (selvedge_horizDist/number_balls_wire) * i
    myWire.append([0,y,0])

cylinderConnection(myWire,selvedge_thickness/2,nodesIds,cylIds,fixed=False,color=[1,0,0],highlight=False,intMaterial='cableMat',extMaterial='contactMat')


#### Set boundary conditions ####
O.bodies[0].state.blockedDOFs='xyzY'#dynamic=False
O.bodies[number_balls_wire].state.blockedDOFs='xyzY'#dynamic=False

# the ring geometry
myRing = []
val = (dist/num_balls_bottom)*ma.cos(angle)
myRing.append([1.1*(dist/num_balls_bottom)*ma.cos(angle),0,-ring_vert_b])
for i in range(0,num_balls_top+1): #Make the circle
    x = (ring_horiz/2) *  ma.cos(ma.radians(180.0/num_balls_top*i))
    z = (ring_horiz/2) *  ma.sin(ma.radians(180.0/num_balls_top*i)) * (ring_vert_t/(ring_horiz/2))
    myRing.append([x,0,z])
myRing.append([-1.1*(dist/num_balls_bottom)*ma.cos(angle),0,-ring_vert_b])
ringNodeIds = []
ringCylIds = []

cylinderConnection(myRing,ring_wire_thickness/2,ringNodeIds,ringCylIds,fixed=False,color=[0,0,1],highlight=False,intMaterial='cableMat',extMaterial='contactMat')


from yade import qt
qt.View()
rr=yade.qt.Renderer()
rr.wire = True
rr.intrPhys=True


O.run()
