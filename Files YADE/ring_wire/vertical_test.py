from builtins import range
from yade import pack, qt, plot
import math as ma, numpy as np
import matplotlib.pyplot as plt
from yade.gridpfacet import *
import sys


number_balls_wire = 4

O.load('ring_wire_balanced.yade.gz')

left_ball_id = 0
right_ball_id = number_balls_wire
bottom_ball_id1 = number_balls_wire*2+1
bottom_ball_id2 = (O.bodies[-1].id-bottom_ball_id1)/2+bottom_ball_id1

velocity = 1e-2
sim_time = 10

def bottom_ball_position():

    x1 = O.bodies[bottom_ball_id1].state.pos[0]
    y1 = O.bodies[bottom_ball_id1].state.pos[1]
    z1 = O.bodies[bottom_ball_id1].state.pos[2]
    
    x2 = O.bodies[bottom_ball_id2].state.pos[0]
    y2 = O.bodies[bottom_ball_id2].state.pos[1]
    z2 = O.bodies[bottom_ball_id2].state.pos[2]
    
    xb = (x1+x2)/2
    yb = (y1+y2)/2
    zb = (z1+z2)/2
    
    finalPos = [xb,yb,zb]
    return finalPos
    
def calcForces():
    # leftmost edge ball
    fnl = O.bodies[left_ball_id].intrs()[0].phys.normalForce
    fsl = O.bodies[left_ball_id].intrs()[0].phys.shearForce
    ftl = np.linalg.norm(fnl)+np.linalg.norm(fsl)
    
    # rightmost edge ball
    fnr = O.bodies[right_ball_id].intrs()[0].phys.normalForce
    fsr = O.bodies[right_ball_id].intrs()[0].phys.shearForce
    ftr = np.linalg.norm(fnr)+np.linalg.norm(fsr)
        
    
    # leftmost bottom ball
  
    fn1 = O.bodies[bottom_ball_id1].intrs()[0].phys.normalForce
    fs1 = O.bodies[bottom_ball_id1].intrs()[0].phys.shearForce

  
    # rightmost bottom ball
    
    fn2 = O.bodies[bottom_ball_id2].intrs()[0].phys.normalForce
    fs2 = O.bodies[bottom_ball_id2].intrs()[0].phys.shearForce


    # ideal central bottom ball
    displacement = (initial_pos[0] - bottom_ball_position()[0],initial_pos[1] - bottom_ball_position()[1],initial_pos[2] - bottom_ball_position()[2])
    fnb = np.dot(fn1,fn2)/2
    fsb = np.dot(fs1,fs2)/2
    
    ftb = np.linalg.norm(fnb)+np.linalg.norm(fsb)
    
    plot.addData(t=O.time,ftl = ftl, ftr = ftr, ftb = ftb, y = displacement[1],z = displacement[2])
    #print(O.time/sim_time*100)
    if O.time > sim_time:
        plot.saveDataTxt('output/bla.txt',vars=('t','ftl','ftr','ftb','y','z'))
        O.pause()


O.bodies[bottom_ball_id1].state.blockedDOFs='xyzXYZ'
O.bodies[bottom_ball_id2].state.blockedDOFs='xyzXYZ'

O.bodies[bottom_ball_id1].state.vel=(0,0,-velocity)
O.bodies[bottom_ball_id2].state.vel=(0,0,-velocity)
O.bodies[bottom_ball_id1].shape.color=[0,1,0]
O.bodies[bottom_ball_id2].shape.color=[0,1,0]


O.engines+=[PyRunner(command='calcForces()',iterPeriod=10)]

plot.plots={'t':('ftl','ftr','ftb',None,'y','z')} 

initial_pos = bottom_ball_position()

O.run(-1,True)
plot.plot()
