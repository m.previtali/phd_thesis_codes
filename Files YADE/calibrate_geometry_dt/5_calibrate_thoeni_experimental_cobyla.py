from builtins import range
from yade import pack, qt, plot
import math as ma, numpy as np, bisect as bi, scipy.optimize as opt
import matplotlib.pyplot as plt
from yade.gridpfacet import *
import sys
from scipy.optimize import minimize


# calibration data
data = np.loadtxt("experimental_data/thoeni_2013_double_twist.dat",delimiter=",")
vec_strain_dt = [row[0] for row in data]
vec_stress_dt = [row[1] for row in data]



# the contact parameters we need to calibrate. There are the reference values
initial_contact_fric = 5.0 #[deg] from Albaba et al., 2015
initial_dt_looseness = 1.3 #[-] my own parameter which governs how tighly locked the double-twists should be. default should be 1.1

initial_values = np.array([initial_contact_fric,initial_dt_looseness])

# bounds for the parameters
min_fric = 1.0
max_fric = 36.0
min_loose = 1.09
max_loose = 1.35

# calibration outputs
iteration_number = 0
curve_rmse = 0

# global variables I need to allocate beforehand because python
num_contacts = 0
vec_wire_variables = []
sim_time = []
bl = []
tl = []
br = []
tr = []
bdtl = []
tdtl = []
bdtr = []
tdtr = []

# model parameters which should be correct and not change
steel_density = 8050    #[kg/m3]
steel_poisson = 0.3   #[-]
steel_young = 301.949801453*1e6/0.0147744945568 # from thoeni et al. 2013, i consider only the first segment since strain is very limited
thickness = 0.0027
brad = thickness/2
section_area = ma.pi*brad**2

height = 0.04
total_height = 0.1
num_twists = 2
velocity = 1e-3
final_strain = 0.2 #[-]

data = np.loadtxt("experimental_data/thoeni_2013_single_wire.dat",delimiter=",")
vec_strain_sw = [row[0] for row in data]
vec_stress_sw = [row[1] for row in data]
global fid
fid = open('output/calibrate_thoeni/rmse.txt','w')


############################################################################
#                        AUXILIARY FUNCTIONS                               #
############################################################################
# export data at the simulation end
def exportForces():
    # we use the 2nd interaction because the model is generated from the bottom upwards
	blnf = bdtl.intrs()[1].phys.normalForce
	brnf = bdtr.intrs()[1].phys.normalForce
	    
	strain = final_strain*((O.time)/(sim_time))
	global iteration_number

	fzl = blnf[2]
	fzr = brnf[2]
	fyl = blnf[1]
	fyr = brnf[1]
	fxl = blnf[0]
	fxr = brnf[0]
	stress = (fzl+fzr)/(section_area*2)
	    
	fricEnergy = 0 #O.energy['plastDissip'] # no plastic dissip because the wires dont actually touch themselves
	plot.addData(t = O.time,strain=strain,fxl=fxl,fxr=fxr,fyl=fyl,fyr=fyr,fzl=fzl,fzr=fzr,fricEnergy=fricEnergy,stress=stress)
	print('Iteration: '+str(iteration_number)+', stress: '+str(stress)+', strain: '+str(strain)+" [-], "+str((O.time/sim_time)*100)+"% complete")

# the wire elasto-platic behaviour
def setPiecewiseFunction():
	for i in range (0,num_contacts):
	    myContact 		= O.interactions[vec_wire_variables[i][0],vec_wire_variables[i][1]]
	    distance 		= np.linalg.norm(O.bodies[myContact.id1].state.pos-O.bodies[myContact.id2].state.pos)
	    ref_distance 	= vec_wire_variables[i][2]
	    prev_distance 	= vec_wire_variables[i][3]
	    prev_force 		= vec_wire_variables[i][4]
	    
	    local_strain = (distance-ref_distance)/ref_distance
	    bin_idx = bi.bisect(vec_strain_sw,local_strain)-1
	    if bin_idx > len(vec_strain_sw)-2:
			#O.pause()
			#plot.plot()
			myContact.isActive = False
	    else:
		    delta_stress 	= vec_stress_sw[bin_idx+1]*1e6-vec_stress_sw[bin_idx]*1e6
		    delta_strain 	= vec_strain_sw[bin_idx+1]-vec_strain_sw[bin_idx]
		    young_modulus 	= delta_stress / (delta_strain+1e-9) # to avoid divide by 0 errors
		    kn = young_modulus*section_area/distance
		    normalForceModule = prev_force + (distance-prev_distance) * kn
		    myContact.phys.normalForce = normalForceModule * myContact.geom.normal
		    vec_wire_variables[i][3] = distance
		    vec_wire_variables[i][4] = normalForceModule

# calculate the RMSE from my simulation data and the experimental curve
def compareData(myStress,myStrain):
	vec_differences = []
	for i in range(0,len(vec_strain_dt)):
		idx_strain = int(min(myStrain,key=lambda x:abs(x-vec_strain_dt[i]))) #find the closest strain value
		stress = myStress[idx_strain] # get the associated stress value
		ref_stress = vec_stress_dt[i]*1e6 # get the equivalent stress value for the reference data
		vec_differences.append(ref_stress-stress) 
	vec_differences = np.array(vec_differences)
	rmse = np.sqrt(((vec_differences)**2).mean())
	print('RMSE: '+str(rmse))
	return rmse

############################################################################
#                        THE ACTUAL MODEL FUNCTION 	                       #
############################################################################
def mySimulation(initial_values):
	O.saveTmp() # save an empty model so i don't keep adding new stuff
	contact_fric = initial_values[0]
	dt_looseness = initial_values[1]
	#dt_looseness = initial_dt_looseness
	inter_wire_distance = brad * dt_looseness # how loose the twist should be. 1.1 = 10%

	#### Engines ####
	O.engines=[
	    ForceResetter(),
	    InsertionSortCollider([Bo1_GridConnection_Aabb(),Bo1_Sphere_Aabb()]),
	    InteractionLoop(
	        [		
	        Ig2_Sphere_Sphere_ScGeom(),
	        Ig2_Box_Sphere_ScGeom(),
	        Ig2_GridNode_GridNode_GridNodeGeom6D(),
	        Ig2_Sphere_GridConnection_ScGridCoGeom(),
	        Ig2_GridConnection_GridConnection_GridCoGridCoGeom()
	        ],
	        [Ip2_CohFrictMat_CohFrictMat_CohFrictPhys(setCohesionNow=True,setCohesionOnNewContacts=False),Ip2_FrictMat_FrictMat_FrictPhys()],
	        [
	        Law2_ScGeom_FrictPhys_CundallStrack(),          # contact law for sphere-sphere
	        Law2_ScGridCoGeom_FrictPhys_CundallStrack(),	# contact law for cylinder-sphere
	        Law2_ScGeom6D_CohFrictPhys_CohesionMoment(),	# contact law for "internal" cylinder forces
	        Law2_GridCoGridCoGeom_FrictPhys_CundallStrack()	# contact law for cylinder-cylinder interaction
	        ]
	    ),
	   # GlobalStiffnessTimeStepper(timestepSafetyCoefficient=0.7,label='ts'), 
	    PyRunner(command='setPiecewiseFunction()',iterPeriod=50),
	    NewtonIntegrator(gravity=(0,0,0),damping=0.5,label='damper'),
	    PyRunner(command='exportForces()',realPeriod=0.5),
	]
	        
	#### Materials ####
	        
	O.materials.append(CohFrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=10,normalCohesion=1e33,shearCohesion=1e33,momentRotationLaw=True,label='cableMat'))
	O.materials.append(FrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=ma.radians(contact_fric),label='contactMat')) 


	#### Geometry ####
	nodePos = []
	nodePos2 = []


	Pos1 = [-height/2,  0, -total_height/2]
	nodePos.append(Pos1)

	Pos2 = [height/2,  0, -total_height/2]
	nodePos2.append(Pos2)    

	ut_ratio = height/(3*ma.pi)


	num_dots = int(floor(15*(inter_wire_distance/brad)/1.3))
	deltaAngle = (360*num_twists-180)/num_dots
	for i in range (0,num_dots+1):
	    angle = i*ma.radians(deltaAngle);
	    displacement = angle*ut_ratio;
	    nodePos.append([inter_wire_distance*ma.sin(angle),inter_wire_distance*ma.cos(angle),-height/2+displacement])
	    nodePos2.append([-inter_wire_distance*ma.sin(angle),-inter_wire_distance*ma.cos(angle),-height/2+displacement])

	nodesIds =[]
	cylIds = []

	nodePos.append([-height/2,  0, total_height/2])
	nodePos2.append([height/2,  0, total_height/2])

	cylinderConnection(nodePos,thickness/2,nodesIds,cylIds,fixed=False,color=[0.5,0.5,0.5],highlight=False,intMaterial='cableMat',extMaterial='contactMat')
	cylinderConnection(nodePos2,thickness/2,nodesIds,cylIds,fixed=False,color=[0.5,0.5,0.5],highlight=False,intMaterial='cableMat',extMaterial='contactMat')

	### Store important ball positions ###

	# global extremities 
	global bl
	global tl
	global br
	global tr
	
	bl = O.bodies[0]                            #bottom left ball
	tl = O.bodies[num_dots+2]                   #top left ball
	br = O.bodies[(num_dots+2)*2+1]             #bottom right ball
	tr = O.bodies[(num_dots+2)*2+num_dots+3]    #top right ball

	bl.shape.color = [1,0,0]
	br.shape.color = [1,0,0]

	tl.shape.color = [0,0,1]
	tr.shape.color = [0,0,1]

	bl.state.blockedDOFs='xyzXYZ'
	br.state.blockedDOFs='xyzXYZ'

	tl.state.blockedDOFs='xyzXYZ'
	tr.state.blockedDOFs='xyzXYZ'

	#actual DT extremities # actually not because they change when the wire is pulled # actually yes because you can also NOT pull them
	global bdtl
	global tdtl
	global bdtr
	global tdtr
	
	bdtl = O.bodies[1]
	tdtl = O.bodies[num_dots+1]
	bdtr = O.bodies[(num_dots+2)*2+2]
	tdtr = O.bodies[(num_dots+2)*2+num_dots+2]

	bdtl.shape.color = [1,0,1]
	tdtl.shape.color = [1,0,1]

	bdtr.shape.color = [1,0,1]
	tdtr.shape.color = [1,0,1]

	bdtl.state.blockedDOFs='xyzXYZ'
	bdtr.state.blockedDOFs='xyzXYZ'
	tdtl.state.blockedDOFs='xyzXYZ'
	tdtr.state.blockedDOFs='xyzXYZ'

	# pull the two opposite ends

	tl.state.vel = [0,0,velocity]
	tr.state.vel = [0,0,velocity]
	tdtl.state.vel = [0,0,velocity]
	tdtr.state.vel = [0,0,velocity]

	plot.plots={'t':('fxl','fxr','fyl','fyr','fzl','fzr',None,'strain',None,'fricEnergy','stress')} 

	#from yade import qt
	#qt.View()
	#rr=yade.qt.Renderer()
	#rr.wire = True
	#rr.intrPhys=True
	
	O.trackEnergy = True


	########################################################
	# SET UP THE CONTACT VARIABLES FOR PIECEWISE FUNCTIONS #
	########################################################
	global vec_wire_variables
	vec_wire_variables = [] 

	##############################
	# LIST OF VEC_WIRE_VARIABLES #
	##############################
	# END1
	# END2
	# REFERENCE DISTANCE
	# CURRENT DISTANCE 
	# CURRENT NORMAL FORCE
	##############################


	for i in O.interactions.all():
	    distance = np.linalg.norm(O.bodies[i.id1].state.pos-O.bodies[i.id2].state.pos)
	    force = 0
	    initial_distance = distance
	    vec_wire_variables.append([i.id1,i.id2,distance,initial_distance,force])
	    
	global num_contacts
	num_contacts = len(vec_wire_variables)

	##########################################################

	timestep = utils.PWaveTimeStep() * 0.7
	O.dt=timestep
	final_displacement = final_strain * height
	global sim_time
	global iteration_number
	sim_time = final_displacement/velocity
	num_steps = int(sim_time/timestep)
	#print(sim_time)
	print('DT LOOSENESS: ' +str(dt_looseness)+', Contact friction: '+str(contact_fric))
	O.run(num_steps,True)
	#O.loadTmp()
	plot.saveDataTxt('output/calibrate_thoeni/thoeni_results_cobyla_'+str(iteration_number)+'_loose_'+str(dt_looseness)+'_fric_'+str(contact_fric) +'.txt', vars=('t','strain','fxl','fxr','fyl','fyr','fzl','fzr','fricEnergy','stress'))
	tempData = plot.data
	curve_rmse = compareData(tempData['stress'],tempData['strain'])
	fid.write(str(iteration_number)+','+str(curve_rmse)+'\n')
	plot.reset()
	iteration_number+=1
	O.pause()
	O.loadTmp()
	print('------------------------------------------')
	return curve_rmse
	#O.stopAtTime = sim_time
	#O.wait()

#mySimulation(initial_values)
bnds = [[min_fric, max_fric],[min_loose, max_loose]]
#construct the bounds in the form of constraints
cons = []
for factor in range(len(bnds)):
    lower, upper = bnds[factor]
    l = {'type': 'ineq',
         'fun': lambda x, lb=lower, i=factor: x[i] - lb}
    u = {'type': 'ineq',
         'fun': lambda x, ub=upper, i=factor: ub - x[i]}
    cons.append(l)
    cons.append(u)
#res = minimize(mySimulation,initial_values,method='TNC', bounds=bnds,options={'disp': True,'maxiter':500})
res = minimize(mySimulation,initial_values,constraints=cons,method='COBYLA', options={'rhobeg': [0.2,20],'disp': True,'maxiter':500})
fid.close()
