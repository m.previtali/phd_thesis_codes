from builtins import range
from yade import pack, qt, plot
import math as ma, numpy as np, bisect as bi
import matplotlib.pyplot as plt
from yade.gridpfacet import *
import sys
from yade import qt
qt.View()

data = np.loadtxt("experimental_data/thoeni_2013_single_wire.dat",delimiter=",")
lista_strain = [row[0] for row in data]
lista_stress = [row[1] for row in data]

data_properties = np.loadtxt("sensitivity_analisys_data.dat",delimiter=",")

steel_density = 8050    #[kg/m3]
steel_poisson = 0.3   #[-]
steel_young = 200e6#301.949801453*1e6/0.0147744945568 # from thoeni et al. 2013, i consider only the first segment since strain is very limited
contact_fric = 5.0 #[deg] from Albaba et al., 2015
thickness = 0.0027
brad = thickness/2
section_area = ma.pi*brad**2
inter_wire_distance = brad * 1.1 # how loose the twist should be. 1.1 = 10%

height = 0.04
total_height = 0.1

num_twists = 2
velocity = 1e-3
final_strain = 0.25 #[-]
iteration_number = 0

############################################################################
#                        ALL THE METHODS                                   #
############################################################################

def exportForces():
	# we use the 2nd interaction because the model is generated from the bottom upwards
	blnf = bdtl.intrs()[1].phys.normalForce
	brnf = bdtr.intrs()[1].phys.normalForce

	strain = final_strain*((O.time)/(sim_time))
	global iteration_number

	fzl = blnf[2]
	fzr = brnf[2]
	fyl = blnf[1]
	fyr = brnf[1]
	fxl = blnf[0]
	fxr = brnf[0]
	stress = (fzl+fzr)/(section_area*2)

	fricEnergy = 0 #O.energy['plastDissip'] # no plastic dissip because the wires dont actually touch themselves
	plot.addData(t = O.time,strain=strain,fxl=fxl,fxr=fxr,fyl=fyl,fyr=fyr,fzl=fzl,fzr=fzr,fricEnergy=fricEnergy,stress=stress)

	print('Iteration: '+str(iteration_number)+', stress: '+str(stress)+', strain: '+str(strain)+" [-], "+str((O.time/sim_time)*100)+"% complete")



def setPiecewiseFunction():
	for i in range (0,num_contacts):
	    myContact 		= O.interactions[vec_wire_variables[i][0],vec_wire_variables[i][1]]
	    distance 		= np.linalg.norm(O.bodies[myContact.id1].state.pos-O.bodies[myContact.id2].state.pos)
	    ref_distance 	= vec_wire_variables[i][2]
	    prev_distance 	= vec_wire_variables[i][3]
	    prev_force 		= vec_wire_variables[i][4]
	    
	    local_strain = (distance-ref_distance)/ref_distance
	    bin_idx = bi.bisect(lista_strain,local_strain)-1
	    if bin_idx > len(lista_strain)-1:
			#O.pause()
			#plot.plot()
			myContact.isActive = False
	    else:
		    delta_stress 	= lista_stress[bin_idx+1]*1e6-lista_stress[bin_idx]*1e6
		    delta_strain 	= lista_strain[bin_idx+1]-lista_strain[bin_idx]
		    young_modulus 	= delta_stress / (delta_strain+1e-9) # to avoid divide by 0 errors
		    kn = young_modulus*section_area/distance
		    normalForceModule = prev_force + (distance-prev_distance) * kn
		    myContact.phys.normalForce = normalForceModule * myContact.geom.normal
		    vec_wire_variables[i][3] = distance
		    vec_wire_variables[i][4] = normalForceModule


############################################################################
#                        THE ACTUAL MODEL                                  #
############################################################################


for sim_idx in range(1,len(data_properties)):
	O.saveTmp()
	contact_fric = data_properties[sim_idx][1]
	dt_looseness = data_properties[sim_idx][0]
	final_displacement = final_strain * height
	global sim_time
	global iteration_number
	sim_time = final_displacement/velocity
	#### Engines ####
	O.engines=[
	    ForceResetter(),
	    InsertionSortCollider([Bo1_GridConnection_Aabb(),Bo1_Sphere_Aabb()]),
	    InteractionLoop(
	        [		
	        Ig2_Sphere_Sphere_ScGeom(),
	        Ig2_Box_Sphere_ScGeom(),
	        Ig2_GridNode_GridNode_GridNodeGeom6D(),
	        Ig2_Sphere_GridConnection_ScGridCoGeom(),
	        Ig2_GridConnection_GridConnection_GridCoGridCoGeom()
	        ],
	        [Ip2_CohFrictMat_CohFrictMat_CohFrictPhys(setCohesionNow=True,setCohesionOnNewContacts=False),Ip2_FrictMat_FrictMat_FrictPhys(),
		     Ip2_FrictMat_FrictMat_MindlinPhys()],
	        [
	        Law2_ScGeom_MindlinPhys_Mindlin(),
#	        Law2_ScGeom_FrictPhys_CundallStrack(),          # contact law for sphere-sphere
	        Law2_ScGridCoGeom_FrictPhys_CundallStrack(),	# contact law for cylinder-sphere
	        Law2_ScGeom6D_CohFrictPhys_CohesionMoment(),	# contact law for "internal" cylinder forces
	        Law2_GridCoGridCoGeom_FrictPhys_CundallStrack()	# contact law for cylinder-cylinder interaction
	        ]
	    ),
	    #GlobalStiffnessTimeStepper(timestepSafetyCoefficient=0.7,label='ts'), 
	    PyRunner(command='setPiecewiseFunction()',iterPeriod=10),
	    NewtonIntegrator(gravity=(0,0,0),damping=0.5,label='damper'),
	    PyRunner(command='exportForces()',realPeriod=1),
	    #qt.SnapshotEngine(realPeriod=int(sim_time/10),fileBase='/home/mprevitali/Pictures/double_twist/dt-'+str(sim_idx)+'_',label='snapshooter'),
	    VTKRecorder(fileName='/home/mprevitali/Pictures/double_twist/'+str(sim_idx)+'/sim_'+str(sim_idx)+'_',recorders=['all'],realPeriod=int(sim_time/10)),
	]
	        
	        
	O.materials.append(CohFrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=10,normalCohesion=1e33,shearCohesion=1e33,momentRotationLaw=True,label='cableMat'))
	O.materials.append(FrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=ma.radians(contact_fric),label='contactMat')) 



	nodePos = []
	nodePos2 = []


	Pos1 = [-height,  0, -total_height/2]
	nodePos.append(Pos1)

	Pos2 = [height,  0, -total_height/2]
	nodePos2.append(Pos2)    

	ut_ratio = height/(3*ma.pi)

	inter_wire_distance = brad * dt_looseness # how loose the twist should be. 1.1 = 10%

	num_dots = int(floor(15*(inter_wire_distance/brad)/1.3))
	deltaAngle = (360*num_twists-180)/num_dots
	for i in range (0,num_dots+1):
	    angle = i*ma.radians(deltaAngle);
	    displacement = angle*ut_ratio;
	    nodePos.append([inter_wire_distance*ma.sin(angle),inter_wire_distance*ma.cos(angle),-height/2+displacement])
	    nodePos2.append([-inter_wire_distance*ma.sin(angle),-inter_wire_distance*ma.cos(angle),-height/2+displacement])

	nodesIds =[]
	cylIds = []

	nodePos.append([-height,  0, total_height/2])
	nodePos2.append([height,  0, total_height/2])

	cylinderConnection(nodePos,thickness/2,nodesIds,cylIds,fixed=False,color=[0.5,0.5,0.5],highlight=False,intMaterial='cableMat',extMaterial='contactMat')
	cylinderConnection(nodePos2,thickness/2,nodesIds,cylIds,fixed=False,color=[0.5,0.5,0.5],highlight=False,intMaterial='cableMat',extMaterial='contactMat')

	# global extremities 
	bl = O.bodies[0]                            #bottom left ball
	tl = O.bodies[num_dots+2]                   #top left ball
	br = O.bodies[(num_dots+2)*2+1]             #bottom right ball
	tr = O.bodies[(num_dots+2)*2+num_dots+3]    #top right ball

	bl.shape.color = [1,0,0]
	br.shape.color = [1,0,0]

	tl.shape.color = [0,0,1]
	tr.shape.color = [0,0,1]

	bl.state.blockedDOFs='xyzXYZ'
	br.state.blockedDOFs='xyzXYZ'

	tl.state.blockedDOFs='xyzXYZ'
	tr.state.blockedDOFs='xyzXYZ'

	#actual DT extremities # actually not because they change when the wire is pulled # actually yes because you can also NOT pull them
	bdtl = O.bodies[1]
	tdtl = O.bodies[num_dots+1]
	bdtr = O.bodies[(num_dots+2)*2+2]
	tdtr = O.bodies[(num_dots+2)*2+num_dots+2]

	bdtl.shape.color = [1,0,1]
	tdtl.shape.color = [1,0,1]

	bdtr.shape.color = [1,0,1]
	tdtr.shape.color = [1,0,1]

	bdtl.state.blockedDOFs='xyzXYZ'
	bdtr.state.blockedDOFs='xyzXYZ'
	tdtl.state.blockedDOFs='xyzXYZ'
	tdtr.state.blockedDOFs='xyzXYZ'

	# pull the two opposite ends

	tl.state.vel = [0,0,velocity]
	tr.state.vel = [0,0,velocity]
	tdtl.state.vel = [0,0,velocity]
	tdtr.state.vel = [0,0,velocity]

	# actually just pull one end
	#bl.state.vel = [0,0,-velocity]
	#br.state.vel = [0,0,-velocity]
	#bdtl.state.vel = [0,0,-velocity]
	#bdtr.state.vel = [0,0,-velocity]

	plot.plots={'t':('fxl','fxr','fyl','fyr','fzl','fzr',None,'strain',None,'fricEnergy','stress')} 

	#from yade import qt
	#qt.View()
	#rr=yade.qt.Renderer()
	#rr.wire = True
	#rr.intrPhys=True
	O.trackEnergy = True







	########################################################
	# SET UP THE CONTACT VARIABLES FOR PIECEWISE FUNCTIONS #
	########################################################
	vec_wire_variables = [] 

	##############################
	# LIST OF VEC_WIRE_VARIABLES #
	##############################
	# END1
	# END2
	# REFERENCE DISTANCE
	# CURRENT DISTANCE 
	# CURRENT NORMAL FORCE
	##############################


	for i in O.interactions.all():
	    distance = np.linalg.norm(O.bodies[i.id1].state.pos-O.bodies[i.id2].state.pos)
	    force = 0
	    initial_distance = distance
	    vec_wire_variables.append([i.id1,i.id2,distance,initial_distance,force])
	    
	    
	num_contacts = len(vec_wire_variables)

	##########################################################

	
	timestep = utils.PWaveTimeStep() * 0.1
	O.dt=timestep
	final_displacement = final_strain * height

	num_steps = int(sim_time/timestep)
	#print(sim_time)
	print('DT LOOSENESS: ' +str(dt_looseness)+', Contact friction: '+str(contact_fric))
	O.run(num_steps,True)
	plot.saveDataTxt('output_sensitivity_thoeni/thoeni_results_hertz'+str(iteration_number)+'.txt', vars=('t','strain','fxl','fxr','fyl','fyr','fzl','fzr','fricEnergy','stress'))
	plot.reset()
	iteration_number+=1
	
	O.loadTmp()











