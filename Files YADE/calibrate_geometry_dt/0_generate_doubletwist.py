from builtins import range
from yade import pack, qt, plot
import math as ma, numpy as np, bisect as bi
import matplotlib.pyplot as plt
from yade.gridpfacet import *
import sys


dt_looseness = 1.09


steel_density = 8050    #[kg/m3]
steel_poisson = 0.3   #[-]
steel_young = 301.949801453*1e6/0.0147744945568 # from thoeni et al. 2013, i consider only the first segment since strain is very limited
contact_fric = 5.0 #[deg] from Albaba et al., 2015
thickness = 0.0027
brad = thickness/2
section_area = ma.pi*brad**2
inter_wire_distance = brad * dt_looseness # how loose the twist should be. 1.1 = 10%

height = 0.04
total_height = 0.1

num_twists = 2
velocity = 5e-2
final_strain = 0.2 #[-]

#### Engines ####
O.engines=[
    ForceResetter(),
    InsertionSortCollider([Bo1_GridConnection_Aabb(),Bo1_Sphere_Aabb()]),
    InteractionLoop(
        [		
        Ig2_Sphere_Sphere_ScGeom(),
        Ig2_Box_Sphere_ScGeom(),
        Ig2_GridNode_GridNode_GridNodeGeom6D(),
        Ig2_Sphere_GridConnection_ScGridCoGeom(),
        Ig2_GridConnection_GridConnection_GridCoGridCoGeom()
        ],
        [Ip2_CohFrictMat_CohFrictMat_CohFrictPhys(setCohesionNow=True,setCohesionOnNewContacts=False),Ip2_FrictMat_FrictMat_FrictPhys()],
        [
        Law2_ScGeom_FrictPhys_CundallStrack(),          # contact law for sphere-sphere
        Law2_ScGridCoGeom_FrictPhys_CundallStrack(),	# contact law for cylinder-sphere
        Law2_ScGeom6D_CohFrictPhys_CohesionMoment(),	# contact law for "internal" cylinder forces
        Law2_GridCoGridCoGeom_FrictPhys_CundallStrack()	# contact law for cylinder-cylinder interaction
        ]
    ),
    GlobalStiffnessTimeStepper(timestepSafetyCoefficient=0.7,label='ts'), 
   # PyRunner(command='setPiecewiseFunction()',iterPeriod=1),
    NewtonIntegrator(gravity=(0,0,0),damping=0.5,label='newton'),
   # PyRunner(command='exportForces()',realPeriod=1)
]
        
        
O.materials.append(CohFrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=10,normalCohesion=1e33,shearCohesion=1e33,momentRotationLaw=True,label='cableMat'))
O.materials.append(FrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=ma.radians(contact_fric),label='contactMat')) 



nodePos = []
nodePos2 = []


Pos1 = [-height/2,  0, -total_height/2]
nodePos.append(Pos1)

Pos2 = [height/2,  0, -total_height/2]
nodePos2.append(Pos2)    

ut_ratio = height/(3*ma.pi)


num_dots = int(floor(15*(inter_wire_distance/brad)/1.3))
deltaAngle = (360*num_twists-180)/num_dots
for i in range (0,num_dots+1):
    angle = i*ma.radians(deltaAngle);
    displacement = angle*ut_ratio;
    nodePos.append([inter_wire_distance*ma.sin(angle),inter_wire_distance*ma.cos(angle),-height/2+displacement])
    nodePos2.append([-inter_wire_distance*ma.sin(angle),-inter_wire_distance*ma.cos(angle),-height/2+displacement])

nodesIds =[]
cylIds = []

nodePos.append([-height/2,  0, total_height/2])
nodePos2.append([height/2,  0, total_height/2])

cylinderConnection(nodePos,thickness/2,nodesIds,cylIds,fixed=False,color=[0.5,0.5,0.5],highlight=False,intMaterial='cableMat',extMaterial='contactMat')
cylinderConnection(nodePos2,thickness/2,nodesIds,cylIds,fixed=False,color=[0.5,0.5,0.5],highlight=False,intMaterial='cableMat',extMaterial='contactMat')

# global extremities 
bl = O.bodies[0]                            #bottom left ball
tl = O.bodies[num_dots+2]                   #top left ball
br = O.bodies[(num_dots+2)*2+1]             #bottom right ball
tr = O.bodies[(num_dots+2)*2+num_dots+3]    #top right ball

bl.shape.color = [1,0,0]
br.shape.color = [1,0,0]

tl.shape.color = [0,0,1]
tr.shape.color = [0,0,1]

bl.state.blockedDOFs='xyzXYZ'
br.state.blockedDOFs='xyzXYZ'

tl.state.blockedDOFs='xyzXYZ'
tr.state.blockedDOFs='xyzXYZ'

#actual DT extremities # actually not because they change when the wire is pulled # actually yes because you can also NOT pull them
bdtl = O.bodies[1]
tdtl = O.bodies[num_dots+1]
bdtr = O.bodies[(num_dots+2)*2+2]
tdtr = O.bodies[(num_dots+2)*2+num_dots+2]

bdtl.shape.color = [1,0,1]
tdtl.shape.color = [1,0,1]

bdtr.shape.color = [1,0,1]
tdtr.shape.color = [1,0,1]

bdtl.state.blockedDOFs='xyzXYZ'
bdtr.state.blockedDOFs='xyzXYZ'
tdtl.state.blockedDOFs='xyzXYZ'
tdtr.state.blockedDOFs='xyzXYZ'

# pull the two opposite ends

tl.state.vel = [0,0,velocity]
tr.state.vel = [0,0,velocity]
tdtl.state.vel = [0,0,velocity]
tdtr.state.vel = [0,0,velocity]

# actually just pull one end
#bl.state.vel = [0,0,-velocity]
#br.state.vel = [0,0,-velocity]
#bdtl.state.vel = [0,0,-velocity]
#bdtr.state.vel = [0,0,-velocity]



from yade import qt
qt.View()
rr=yade.qt.Renderer()
rr.wire = True
rr.intrPhys=True
O.trackEnergy = True



O.run(1000,True)
