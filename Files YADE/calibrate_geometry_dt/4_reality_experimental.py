from builtins import range
from yade import pack, qt, plot
import math as ma, numpy as np, bisect as bi
import matplotlib.pyplot as plt
from yade.gridpfacet import *
import sys

data = np.loadtxt("experimental_data/thoeni_2013_single_wire.dat",delimiter=",")
lista_strain = [row[0] for row in data]
lista_stress = [row[1] for row in data]


steel_density = 8050    #[kg/m3]
steel_poisson = 0.3   #[-]
steel_young = 301.949801453*1e6/0.0147744945568 # from thoeni et al. 2013, i consider only the first segment since strain is very limited
contact_fric = 5 #[deg] from Albaba et al., 2015
thickness = 0.0027
brad = thickness/2
section_area = ma.pi*brad**2
inter_wire_distance = brad * 1.09 # how loose the twist should be

height = 0.04
total_height = 0.1

bl1 = 0
bl2 = 0
br1 = 0
br2 = 0
extraTime = 0
#global bl1 
#global bl2 
#global br1 
#global br2 

stress = []
strain = []

num_twists = 2
velocity = 1e-3
final_strain = 0.2 #[-]
tensile_angle_bin = [0,5,10,15,20,25,30,35,40,45,50,55,60]
O.saveTmp()

  
############################################################################
#                        ALL THE METHODS                                   #
############################################################################

def exportForces():
	# we use the 2nd interaction because the model is generated from the bottom upwards
	blnf = bdtl.intrs()[1].phys.normalForce
	brnf = bdtr.intrs()[1].phys.normalForce
	
	blnf2 = bdtl.intrs()[0].phys.normalForce
	brnf2 = bdtr.intrs()[0].phys.normalForce
	
	fzl2 = blnf2[2]
	fzr2 = brnf2[2]
	
	global stress
	global strain

	actual_displacement = abs((bdtl.state.pos[2]+bdtr.state.pos[2])/2-(tdtl.state.pos[2]+tdtr.state.pos[2])/2)
	strain = (actual_displacement-height)/height

	fzl = blnf[2]
	fzr = brnf[2]
	fyl = blnf[1]
	fyr = brnf[1]
	fxl = blnf[0]
	fxr = brnf[0]
	stress = (fzl+fzr)/(section_area*2)
	fricEnergy = 0#O.energy['plastDissip'] # no plastic dissip because the wires dont actually touch themselves
	plot.addData(t = O.time,strain=strain,fxl=fxl,fxr=fxr,fyl=fyl,fyr=fyr,fzl=fzl,fzr=fzr,fricEnergy=fricEnergy,stress=stress)
	#print('fzl:'+str(fzl)+' fzr:'+str(fzr)+'fzl2:'+str(fzl2)+' fzr2:'+str(fzr2))
	print('Simulation: realistic, stress: '+str(stress*1e-6)+' [MPa], strain: '+str(strain)+" [-], " + str(strain/final_strain*100) +"% complete")
    
    
	if strain > final_strain:
		plot.saveDataTxt('output/real_results.txt',vars=('t','strain','fxl','fxr','fyl','fyr','fzl','fzr','fricEnergy','stress'))
		O.pause()
		print('--------------')
		print('Done!')

def find_twist_start(vec_id_wire):
    maxRatios1 = 0
    myId1 = 0
    maxRatios2 = 0
    myId2 = 0
    for i in vec_id_wire:
        myIntrs = O.bodies[int(i)].intrs()
        myForces = []
#        O.bodies[int(i)].shape.color = [0,0,0]
        for j in myIntrs:
            myForces.append(np.linalg.norm(j.phys.normalForce))
        ratio = (max(myForces)+1e-3)/(min(myForces)+1e-3)
        if maxRatios2 < ratio:
            if ratio > maxRatios1:
                maxRatios2 = maxRatios1
                myId2 = myId1
                maxRatios1 = ratio
                myId1 = int(i)
            else:
                myRatios2 = ratio
                myId2 = int(i)
    
    if O.bodies[myId1].state.pos[2] > O.bodies[myId2].state.pos[2]:
        tempId = myId1
        myId1 = myId2
        myId2 = tempId

    return myId1, myId2

def setPiecewiseFunction():
	for i in range (0,num_contacts):
		myContact = O.interactions[vec_wire_variables[i][0],vec_wire_variables[i][1]]
		if myContact.isActive:
			distance 		= np.linalg.norm(O.bodies[myContact.id1].state.pos-O.bodies[myContact.id2].state.pos)
			ref_distance 	= vec_wire_variables[i][2]
			prev_distance 	= vec_wire_variables[i][3]
			prev_force 		= vec_wire_variables[i][4]

			local_strain = (distance-ref_distance)/ref_distance
			bin_idx = bi.bisect(lista_strain,local_strain)-1
			if bin_idx > len(lista_strain)-2:
				if vec_wire_variables[i][0] == bl.id or vec_wire_variables[i][0] == br.id or vec_wire_variables[i][1] == tl.id or vec_wire_variables[i][1] == tr.id:
					delta_stress 	= lista_stress[bin_idx]*1e6-lista_stress[bin_idx-1]*1e6
					delta_strain 	= lista_strain[bin_idx]-lista_strain[bin_idx-1]
					young_modulus 	= delta_stress / (delta_strain+1e-9) # to avoid divide by 0 errors
					kn = young_modulus*section_area/distance
					normalForceModule = prev_force + (distance-prev_distance) * kn
					myContact.phys.normalForce = normalForceModule * myContact.geom.normal
					vec_wire_variables[i][3] = distance
					vec_wire_variables[i][4] = normalForceModule
					print('CONTACT NOT BROKEN')
				else:
					myContact.isActive = False
			#		print('BROKEN CONTACT: id:'+str(i)+' end1: '+str(vec_wire_variables[i][0])+' end2: '+str(vec_wire_variables[i][1]))
					#O.pause()
			else:
				
				delta_stress 	= lista_stress[bin_idx+1]*1e6-lista_stress[bin_idx]*1e6
				delta_strain 	= lista_strain[bin_idx+1]-lista_strain[bin_idx]
				young_modulus 	= delta_stress / (delta_strain+1e-9) # to avoid divide by 0 errors
				kn = young_modulus*section_area/distance
				normalForceModule = prev_force + (distance-prev_distance) * kn
				myContact.phys.normalForce = normalForceModule * myContact.geom.normal
				vec_wire_variables[i][3] = distance
				vec_wire_variables[i][4] = normalForceModule

#def identifyUnbreakableWires(end1,end2):
	#if end1ddd

############################################################################
#                        THE ACTUAL MODEL                                  #
############################################################################
  
final_displacement = final_strain * height
sim_time = final_displacement/velocity*2


#for angle_idx in range(0,len(tensile_angle_bin)):
tensile_angle = ma.degrees(ma.atan(4.0/3))  #[deg] the angle of the bottom forces

final_displacement = final_strain * total_height
sim_time = final_displacement/velocity

velocity_lateral = velocity * ma.sin(ma.radians(tensile_angle))
velocity_vertical = velocity * ma.cos(ma.radians(tensile_angle))

#### Engines ####
O.engines=[
    ForceResetter(),
    InsertionSortCollider([Bo1_GridConnection_Aabb(),Bo1_Sphere_Aabb()]),
    InteractionLoop(
        [		
        Ig2_Sphere_Sphere_ScGeom(),
        Ig2_Box_Sphere_ScGeom(),
        Ig2_GridNode_GridNode_GridNodeGeom6D(),
        Ig2_Sphere_GridConnection_ScGridCoGeom(),
        Ig2_GridConnection_GridConnection_GridCoGridCoGeom()
        ],
        [Ip2_CohFrictMat_CohFrictMat_CohFrictPhys(setCohesionNow=True,setCohesionOnNewContacts=False),Ip2_FrictMat_FrictMat_FrictPhys()],
        [
        Law2_ScGeom_FrictPhys_CundallStrack(),			# contact law for sphere-sphere
        Law2_ScGridCoGeom_FrictPhys_CundallStrack(),	# contact law for cylinder-sphere
        Law2_ScGeom6D_CohFrictPhys_CohesionMoment(),	# contact law for "internal" cylinder forces
        Law2_GridCoGridCoGeom_FrictPhys_CundallStrack()	# contact law for cylinder-cylinder interaction
        ]
    ),
	#VTKRecorder(fileName='output_img/reality_',recorders=['all'],realPeriod=0.05),
    GlobalStiffnessTimeStepper(timestepSafetyCoefficient=0.7,label='ts'), 
    PyRunner(command='setPiecewiseFunction()',iterPeriod=1),
    NewtonIntegrator(gravity=(0,0,0),damping=0.5,label='newton'),
    PyRunner(command='exportForces()',realPeriod=1),
 #   qt.SnapshotEngine(realPeriod=1,fileBase='/home/mprevitali/Pictures/double_twist/dt-',label='snapshooter'),
]
        
        
O.materials.append(CohFrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=10,normalCohesion=1e33,shearCohesion=1e33,momentRotationLaw=True,label='cableMat'))
O.materials.append(FrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=ma.radians(contact_fric),label='contactMat')) 



nodePos = []
nodePos2 = []


Pos1 = [-height,  0, -total_height/2]
nodePos.append(Pos1)

Pos2 = [height,  0, -total_height/2]
nodePos2.append(Pos2)    

ut_ratio = height/(3*ma.pi)

#deltaAngle = 45
#num_dots = (360*num_twists-180)/deltaAngle
num_dots = int(floor(15*(inter_wire_distance/brad)/1.3))
deltaAngle = (360*num_twists-180)/num_dots
for i in range (0,num_dots+1):
    angle = i*ma.radians(deltaAngle);
    displacement = angle*ut_ratio;
    nodePos.append([inter_wire_distance*ma.sin(angle),inter_wire_distance*ma.cos(angle),-height/2+displacement])
    nodePos2.append([-inter_wire_distance*ma.sin(angle),-inter_wire_distance*ma.cos(angle),-height/2+displacement])

nodesIds =[]
cylIds = []

nodePos.append([-height,  0, total_height/2])
nodePos2.append([height,  0, total_height/2])

#cylinderConnection(nodePos,thickness/2,nodesIds,cylIds,fixed=False,color=[1,1,0],highlight=False,intMaterial='cableMat',extMaterial='contactMat')
#cylinderConnection(nodePos2,thickness/2,nodesIds,cylIds,fixed=False,color=[0,1,0],highlight=False,intMaterial='cableMat',extMaterial='contactMat')
#cylinderConnection(nodePos,thickness/2,nodesIds,cylIds,fixed=False,color=[0.5,0.5,0.5],highlight=False,intMaterial='cableMat',extMaterial='contactMat')
#cylinderConnection(nodePos2,thickness/2,nodesIds,cylIds,fixed=False,color=[0.5,0.5,0.5],highlight=False,intMaterial='cableMat',extMaterial='contactMat')
cylinderConnection(nodePos,thickness/2,nodesIds,cylIds,fixed=False,color=[0,0,0],highlight=False,intMaterial='cableMat',extMaterial='contactMat')
cylinderConnection(nodePos2,thickness/2,nodesIds,cylIds,fixed=False,color=[0,0,0],highlight=False,intMaterial='cableMat',extMaterial='contactMat')

# VECTORS FOR THE BODY ID OF EACH WIRE
vec_id_left_wire = np.arange(0,num_dots+2,1).tolist()
vec_id_right_wire = np.arange((num_dots+2)*2+1,(num_dots+2)*2+num_dots+4,1).tolist()


#extremities 
bl = O.bodies[0]                            #bottom left ball
tl = O.bodies[num_dots+2]                   #top left ball
br = O.bodies[(num_dots+2)*2+1]             #bottom right ball
tr = O.bodies[(num_dots+2)*2+num_dots+3]    #top right ball

bl.shape.color = [1,0,0]
br.shape.color = [1,0,0]

tl.shape.color = [0,0,1]
tr.shape.color = [0,0,1]

bl.state.blockedDOFs='xyzXYZ'
br.state.blockedDOFs='xyzXYZ'

tl.state.blockedDOFs='xyzXYZ'
tr.state.blockedDOFs='xyzXYZ'

tl.state.vel=(-velocity_lateral,0,velocity_vertical)
tr.state.vel=( velocity_lateral,0,velocity_vertical)

bl.state.vel=(-velocity_lateral,0,-velocity_vertical)
br.state.vel=( velocity_lateral,0,-velocity_vertical)


#actual DT extremities # actually not because they change when the wire is pulled # actually yes because you can also NOT pull them
bdtl = O.bodies[1]
tdtl = O.bodies[num_dots+1]
bdtr = O.bodies[(num_dots+2)*2+2]
tdtr = O.bodies[(num_dots+2)*2+num_dots+2]

bdtl.shape.color = [1,0,1]
tdtl.shape.color = [1,0,1]

bdtr.shape.color = [1,0,1]
tdtr.shape.color = [1,0,1]


#vec_id_left_wire = np.arange(0,num_dots+1,num_dots+1).tolist()
#vec_id_right_wire = np.arange((num_dots+2)*2+1,(num_dots+2)*2+num_dots+3,(num_dots+2)*2+num_dots+3-((num_dots+2)*2+1)).tolist()
vec_id_left_wire = np.arange(0,num_dots+2,1).tolist()
vec_id_right_wire = np.arange((num_dots+2)*2+1,(num_dots+2)*2+num_dots+4,1).tolist()


    

plot.plots={'t':('fxl','fxr','fyl','fyr','fzl','fzr',None,'strain',None,'fricEnergy','stress')} 

from yade import qt
qt.View()
rr=yade.qt.Renderer()
rr.wire = True
rr.intrPhys=True


plot.resetData()
O.trackEnergy = True





########################################################
# SET UP THE CONTACT VARIABLES FOR PIECEWISE FUNCTIONS #
########################################################
vec_wire_variables = [] 

##############################
# LIST OF VEC_WIRE_VARIABLES #
##############################
# END1
# END2
# REFERENCE DISTANCE
# CURRENT DISTANCE 
# CURRENT NORMAL FORCE
##############################


for i in O.interactions.all():
    distance = np.linalg.norm(O.bodies[i.id1].state.pos-O.bodies[i.id2].state.pos)
    force = 0
    initial_distance = distance
    vec_wire_variables.append([i.id1,i.id2,distance,initial_distance,force])
    
    
num_contacts = len(vec_wire_variables)

##########################################################



O.run(-1,True)

#O.loadTmp()
#  plot.plot()
  
  
  
  
  
