from builtins import range
from yade import pack, qt, plot
import math as ma, numpy as np
import matplotlib.pyplot as plt
from yade.gridpfacet import *
import sys

steel_density = 8050    #[kg/m3]
steel_poisson = 0.265   #[-]
steel_young = 301.949801453*1e6/0.0147744945568 # from thoeni et al. 2013, i consider only the first segment since strain is very limited
contact_fric = 5 #[deg] from Albaba et al., 2015
thickness = 0.0027
brad = thickness/2
inter_wire_distance = brad * 1.1 # how loose the twist should be

height = 0.04
total_height = 0.1

bl1 = 0
bl2 = 0
br1 = 0
br2 = 0
extraTime = 0
#global bl1 
#global bl2 
#global br1 
#global br2 

num_twists = 2
velocity = 5e-3
final_strain = 0.3 #[-]
tensile_angle_bin = [0,5,10,15,20,25,30,35,40,45,50,55,60]
O.saveTmp()

  
############################################################################
#                        ALL THE METHODS                                   #
############################################################################
  
  
def setup_analysis():
    if O.time > sim_time:
        
        
        # calculate the new positions of the double-twist extremities
        global bl1 
        global bl2 
        global br1 
        global br2 
        
        if tensile_angle>0:
            [bl1,bl2] = find_twist_start(vec_id_left_wire)
            [br1,br2] = find_twist_start(vec_id_right_wire)
        else:
            bl1 = bdtl.id
            br1 = bdtr.id
            bl2 = tdtl.id
            br2 = tdtr.id
            
        
        bl.shape.color = [1,1,1]
        br.shape.color = [1,1,1]
        tl.shape.color = [1,1,1]
        tr.shape.color = [1,1,1]
        
        bl.state.vel=(0,0,0)
        br.state.vel=(0,0,0)
        tl.state.vel=(0,0,velocity)
        tr.state.vel=(0,0,velocity)

        distance = abs(O.bodies[bl1].state.pos[2]-O.bodies[bl2].state.pos[2])
        
        final_distance = distance + distance * final_strain
        global extraTime
        extraTime = (final_distance-distance)/velocity
        
        
        # fix the positions and color them and start moving them
        O.bodies[bl1].state.blockedDOFs='xyzXYZ'
        O.bodies[bl2].state.blockedDOFs='xyzXYZ'
        O.bodies[br1].state.blockedDOFs='xyzXYZ'
        O.bodies[br2].state.blockedDOFs='xyzXYZ'
        
        O.bodies[bl1].shape.color = [1,0,0]
        O.bodies[bl2].shape.color = [0,1,0]        
        O.bodies[br1].shape.color = [1,0,0]
        O.bodies[br2].shape.color = [0,1,0]
        
        O.bodies[bl2].state.vel = (0,0,velocity)
        O.bodies[br2].state.vel = (0,0,velocity)
        
        O.engines[-1].dead=True
      #  O.pause()
        O.engines+=[PyRunner(command='export_forces(bl1,br1,extraTime)',iterPeriod=10)]

def find_twist_start(vec_id_wire):
    maxRatios1 = 0
    myId1 = 0
    maxRatios2 = 0
    myId2 = 0
    for i in vec_id_wire:
        myIntrs = O.bodies[int(i)].intrs()
        myForces = []
        O.bodies[int(i)].shape.color = [0,0,0]
        for j in myIntrs:
            #print(np.linalg.norm(j.phys.normalForce))
            myForces.append(np.linalg.norm(j.phys.normalForce))
        ratio = (max(myForces)+1e-3)/(min(myForces)+1e-3)
        if maxRatios2 < ratio:
            if ratio > maxRatios1:
                maxRatios2 = maxRatios1
                myId2 = myId1
                maxRatios1 = ratio
                myId1 = int(i)
            else:
                myRatios2 = ratio
                myId2 = int(i)
    
    if O.bodies[myId1].state.pos[2] > O.bodies[myId2].state.pos[2]:
        tempId = myId1
        myId1 = myId2
        myId2 = tempId
        
  #  O.bodies[myId1].shape.color = [1,0,0]
  #  O.bodies[myId2].shape.color = [0,1,0]
    
    return myId1, myId2
    
def export_forces(ball_left,ball_right, extraTime):
    
    # consider the following: we are measuring the force on the bottom of the double-twist, therefore the interaction we are interested into is the 2nd.
    blnf = O.bodies[ball_left].intrs()[1].phys.normalForce
    brnf = O.bodies[ball_right].intrs()[1].phys.normalForce
    
    strain = final_strain*((O.time-sim_time)/(+extraTime))
    
    fzl = blnf[2]
    fzr = brnf[2]
    fyl = blnf[1]
    fyr = brnf[1]
    fxl = blnf[0]
    fxr = brnf[0]
    
    fricEnergy = O.energy['plastDissip']
    plot.addData(t = O.time,strain=strain,fxl=fxl,fxr=fxr,fyl=fyl,fyr=fyr,fzl=fzl,fzr=fzr,fricEnergy=fricEnergy)
    
    print('Simulation: '+str(tensile_angle)+" deg, strain: "+str(strain)+" [-], "+str((O.time-sim_time)/extraTime*100)+"% complete")
    
    if O.time > sim_time+extraTime:
        plot.saveDataTxt('output/angle_new_'+str(tensile_angle)+'.txt',vars=('t','strain','fxl','fxr','fyl','fyr','fzl','fzr','fricEnergy'))
   #     O.pause()
       






############################################################################
#                        THE ACTUAL MODEL                                  #
############################################################################
  




for angle_idx in range(0,len(tensile_angle_bin)):
    tensile_angle = tensile_angle_bin[angle_idx]  #[deg] the angle of the bottom forces

    final_displacement = final_strain * total_height
    sim_time = final_displacement/velocity

    velocity_lateral = velocity * ma.sin(ma.radians(tensile_angle))


    #### Engines ####
    O.engines=[
        ForceResetter(),
        InsertionSortCollider([Bo1_GridConnection_Aabb(),Bo1_Sphere_Aabb()]),
        InteractionLoop(
            [		
            Ig2_Sphere_Sphere_ScGeom(),
            Ig2_Box_Sphere_ScGeom(),
            Ig2_GridNode_GridNode_GridNodeGeom6D(),
            Ig2_Sphere_GridConnection_ScGridCoGeom(),
            Ig2_GridConnection_GridConnection_GridCoGridCoGeom()
            ],
            [Ip2_CohFrictMat_CohFrictMat_CohFrictPhys(setCohesionNow=True,setCohesionOnNewContacts=False),Ip2_FrictMat_FrictMat_FrictPhys()],
            [
            Law2_ScGeom_FrictPhys_CundallStrack(),	# contact law for sphere-sphere
            Law2_ScGridCoGeom_FrictPhys_CundallStrack(),	# contact law for cylinder-sphere
            Law2_ScGeom6D_CohFrictPhys_CohesionMoment(),	# contact law for "internal" cylinder forces
            Law2_GridCoGridCoGeom_FrictPhys_CundallStrack()	# contact law for cylinder-cylinder interaction
            ]
        ),
        GlobalStiffnessTimeStepper(timestepSafetyCoefficient=0.1,label='ts'), 
        NewtonIntegrator(gravity=(0,0,-9.81),damping=0.5,label='newton'),
        PyRunner(command='setup_analysis()',realPeriod=1)
    ]
            
            
    O.materials.append(CohFrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=10,normalCohesion=1e33,shearCohesion=1e33,momentRotationLaw=True,label='cableMat'))
    O.materials.append(FrictMat(young=steel_young,poisson=steel_poisson,density=steel_density,frictionAngle=ma.radians(contact_fric),label='contactMat')) 



    nodePos = []
    nodePos2 = []


    Pos1 = [-height/2,  0, -total_height/2]
    nodePos.append(Pos1)

    Pos2 = [height/2,  0, -total_height/2]
    nodePos2.append(Pos2)    

    ut_ratio = height/(3*ma.pi)

    #deltaAngle = 45
    #num_dots = (360*num_twists-180)/deltaAngle
    num_dots = int(floor(15*(inter_wire_distance/brad)/1.3))
    deltaAngle = (360*num_twists-180)/num_dots
    for i in range (0,num_dots+1):
        angle = i*ma.radians(deltaAngle);
        displacement = angle*ut_ratio;
        nodePos.append([inter_wire_distance*ma.sin(angle),inter_wire_distance*ma.cos(angle),-height/2+displacement])
        nodePos2.append([-inter_wire_distance*ma.sin(angle),-inter_wire_distance*ma.cos(angle),-height/2+displacement])

    nodesIds =[]
    cylIds = []

    nodePos.append([-height/2,  0, total_height/2])
    nodePos2.append([height/2,  0, total_height/2])

    #cylinderConnection(nodePos,thickness/2,nodesIds,cylIds,fixed=False,color=[1,1,0],highlight=False,intMaterial='cableMat',extMaterial='contactMat')
    #cylinderConnection(nodePos2,thickness/2,nodesIds,cylIds,fixed=False,color=[0,1,0],highlight=False,intMaterial='cableMat',extMaterial='contactMat')
    cylinderConnection(nodePos,thickness/2,nodesIds,cylIds,fixed=False,color=[0.5,0.5,0.5],highlight=False,intMaterial='cableMat',extMaterial='contactMat')
    cylinderConnection(nodePos2,thickness/2,nodesIds,cylIds,fixed=False,color=[0.5,0.5,0.5],highlight=False,intMaterial='cableMat',extMaterial='contactMat')

    #extremities 
    bl = O.bodies[0]                            #bottom left ball
    tl = O.bodies[num_dots+2]                   #top left ball
    br = O.bodies[(num_dots+2)*2+1]             #bottom right ball
    tr = O.bodies[(num_dots+2)*2+num_dots+3]    #top right ball

    bl.shape.color = [1,0,0]
    br.shape.color = [1,0,0]

    tl.shape.color = [0,0,1]
    tr.shape.color = [0,0,1]

    bl.state.blockedDOFs='xyzXYZ'
    br.state.blockedDOFs='xyzXYZ'

    tl.state.blockedDOFs='xyzXYZ'
    tr.state.blockedDOFs='xyzXYZ'

    tl.state.vel=(-velocity_lateral,0,0)
    tr.state.vel=( velocity_lateral,0,0)

    bl.state.vel=(-velocity_lateral,0,0)
    br.state.vel=( velocity_lateral,0,0)


    #actual DT extremities # actually not because they change when the wire is pulled # actually yes because you can also NOT pull them
    bdtl = O.bodies[1]
    tdtl = O.bodies[num_dots+1]
    bdtr = O.bodies[(num_dots+2)*2+2]
    tdtr = O.bodies[(num_dots+2)*2+num_dots+2]

    bdtl.shape.color = [1,0,1]
    tdtl.shape.color = [1,0,1]

    bdtr.shape.color = [1,0,1]
    tdtr.shape.color = [1,0,1]


    #vec_id_left_wire = np.arange(0,num_dots+1,num_dots+1).tolist()
    #vec_id_right_wire = np.arange((num_dots+2)*2+1,(num_dots+2)*2+num_dots+3,(num_dots+2)*2+num_dots+3-((num_dots+2)*2+1)).tolist()
    vec_id_left_wire = np.arange(0,num_dots+2,1).tolist()
    vec_id_right_wire = np.arange((num_dots+2)*2+1,(num_dots+2)*2+num_dots+4,1).tolist()


        

    plot.plots={'t':('fxl','fxr','fyl','fyr','fzl','fzr',None,'strain',None,'fricEnergy')} 

#    from yade import qt
 #   qt.View()
  #  rr=yade.qt.Renderer()
   # rr.wire = True
    #rr.intrPhys=True


    plot.resetData()
    O.trackEnergy = True
    O.run(-1,True)

    O.loadTmp()
  #  plot.plot()
  
  
  
  
  
