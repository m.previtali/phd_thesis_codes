	figure(1)
	plot(mat_state(:,3)*1e3,mat_state(:,1));
	xlabel('u [mm]');
	ylabel('F [N]');
	figure(2)
	plot(mat_state(:,4),mat_state(:,2));
	xlabel('\omega [deg]');
	ylabel('M [N\cdot m]');
	
	
	vec_ok_nfs = linspace(2*params.ny,2*params.nult,5);
	npoints = 250;
	figure(3)
	hold on
		
	for i = 1:length(vec_ok_nfs)
	vec_F = linspace(0,vec_ok_nfs(i)/2,npoints);
	vec_F_neg = linspace(params.fy,0,npoints);
	nf = vec_ok_nfs(i);
	
	for j = 1:npoints
	F = vec_F(j);
	vec_M(j) =  -(params.D*(-params.mf)^(1/2)*(2*F + nf)^(1/2)*(2*F - nf)^(1/2))/(2*exp(((F + nf/2)/nf - 1/2)^2/params.beta)^(1/2));
	end
	MY = max(vec_M);
	for j = 1:npoints
	F = vec_F_neg(j);
	vec_M_cmp(j)=(MY*(params.fy - F)^(1/2)*(F + params.fy)^(1/2))/params.fy;
	end
	
	plot(vec_F,vec_M,'k');
	plot(-vec_F_neg,vec_M_cmp,'k');
	
	end
	xlabel('Force [N]')
	ylabel('Moment [N\cdot m]')