	function [f,updated_state] = yieldf(initial_state,increment,params,Imode)
	tiny = 1e-13; % to avoid divide by 0/negative sign errors

	f = yield(initial_state,params); %evaluate the yieldf
	
	if Imode == 0
	return
	end
	
	
	%% substepping for non-linear plasticity
	
	% initialization
	T_j=0.0;
	DT_j=1.0;
	
	
	ksub=0;
	
	while T_j<1.0		
	
	ksub=ksub+1;
	
	% stop if too many substeps
	
	if ksub > params.max_ksub
	disp ('--- ERROR: substep number exceeding maximum in trial state evaluation ---')
	error('    execution stopped in plastic update')
	end
	% calculate all the derivatives
	first_approximation_plastic = evaluate_plasticity(initial_state,increment, params);
	
	updated_state_2nd_order = initial_state + (DT_j/2.0)*first_approximation_plastic;
	
	second_approximation_plastic = evaluate_plasticity(updated_state_2nd_order,increment,params);
	
	updated_state_3rd_order = initial_state - DT_j*first_approximation_plastic + 2.0 * DT_j * second_approximation_plastic;
	
	third_approximation_plastic = evaluate_plasticity(updated_state_3rd_order,increment,params);
	% form approximate solution of 2nd and 3rd order
	
	state_hat = initial_state+DT_j*second_approximation_plastic;
	state_tilde=initial_state+DT_j*((1.0/6.0)*first_approximation_plastic+(2.0/3.0)*second_approximation_plastic+(1.0/6.0)*third_approximation_plastic);
	
	sig_tilde = state_tilde(1:2);
	alpha_tilde = state_tilde(5);
	
	% difference between stresses and hardening params
	delta_sig = state_tilde(1:2)-state_hat(1:2);
	delta_alpha = state_tilde(5)-state_hat(5);
	
	% local error estimate
	norm_sig = sqrt(sig_tilde*sig_tilde');
	norm_alpha = abs(alpha_tilde);
	
	if norm_sig < tiny      
	norm_sig=tiny;
	end
	
	if norm_alpha < tiny
	norm_alpha=tiny;
	end
	
	% residuals?
	Res = zeros(3,1);
	Res(1:2)=(1.0/norm_sig)*delta_sig;
	Res(3)=(1.0/norm_alpha)*delta_alpha;
	
	norm_Res=sqrt(Res'*Res);
	
	if norm_Res < tiny
	norm_Res = tiny;
	end
	
	NSS = 0.9*DT_j*(params.err_tol/norm_Res)^(1.0/3.0);
	
	if norm_Res<params.err_tol 				
	
	% substep is accepted, update y and T and estimate new substep size DT
	
	label=' -- plastic integration -- accepted; T = ';
	
	initial_state=state_tilde;
	
	T_j=T_j+DT_j;
	DT_j=min(4.0*DT_j,NSS);
	DT_j=min((1.0-T_j),DT_j);
	disp([label,num2str(T_j,4),', DT = ',num2str(DT_j,4),', error = ', num2str(norm_Res)])
	
	else
	
	% substep is rejected, recompute with new (smaller) substep size DT
	
	label=' -- plastic integration -- rejected; T = ';
	
	DT_j=max(0.25*DT_j,NSS);
	disp([label,num2str(T_j,'%10.5e'),' , DT = ',num2str(DT_j,'%10.5e'),', error = ', num2str(norm_Res)])
	
	end						
	
	end
	
	updated_state = initial_state;
	updated_state(10)= 1;
	end
	function [plastic_increments] = evaluate_plasticity(state,increment, params)
	% state variables
	F = state(1);
	M = state(2);
	up = state(6);
	omegap = state(8);
	D = params.D;
	
	u_dot = increment.deformation;
	omega_dot = increment.bending;
	
	
	axis_stiff =  params.E/params.L*params.A;
	bend_stiff =  params.E/params.L*params.I;
	
	eta = 1 - exp(-params.kf*abs(up)-params.km*abs(omegap));
	
	m = params.mzero + (params.mone-params.mzero)*eta;
	q = params.qzero + (params.qone-params.qzero)*eta;
	
	nf = 2 * (params.ny + eta*(params.nult-params.ny));
	
	dnf_dup = 2 * params.kf * exp(-params.km*abs(omegap)-params.kf*abs(up))*(params.nult-params.ny);
	dnf_domegap = 2 * params.km * exp(-params.km*abs(omegap)-params.kf*abs(up))*(params.nult-params.ny);
	
	Fstar = F+nf/2;
	
	if F<0
	
	MY = nf*params.D * sqrt(params.mf)/2;
	MYD = MY/params.D;
	dmy_dnf = D * sqrt(params.mf)/2;
	df_dF = 2 * F * MYD/(params.fy^2);
	df_dM = 2*M/params.D/MY;
	% associated
	df_dmy = (F^2/(params.fy)^2+M^2/MY^2-1)/D-2*M^2/D/MY^2;
	df_dnf = df_dmy * dmy_dnf;
	dg_dM = df_dM;
	dg_dF = df_dF;
	else
	
	tmp1 = exp(-1/params.beta*(Fstar/nf-0.5)^2);
	tmp2 = F-nf/2;
	df_dF = params.mf * tmp1 * tmp2 + params.mf * tmp1 *Fstar - 1.0/(nf*params.beta)*params.mf*tmp1*tmp2*Fstar*(Fstar/nf-0.5)*2;
	df_dM = 2*M/D^2;
	df_dnf = params.mf*tmp1*tmp2*0.5-params.mf*tmp1*Fstar*0.5+params.mf/params.beta * tmp1*tmp2*Fstar*(Fstar/nf-0.5)*(Fstar/nf^2 -1/nf*0.5)*2;
	dg_dF = 1/sqrt(q^2*(M/(F*D))^m+1);
	dg_dM = (q*(M/(F*D))^m)/sqrt(q^2*(M/(F*D))^(2*m)+1);
	if F==0
		dg_dM=1;
	end
	end
	
	numerator = df_dM * bend_stiff * omega_dot + df_dF * axis_stiff * u_dot;
	denominator = -1 * df_dnf * (dnf_domegap * dg_dM + dnf_dup * dg_dF) + df_dM*dg_dM*bend_stiff + df_dF*dg_dF * axis_stiff;
	
	lambda = numerator/denominator;
	
	u_p_dot = abs(lambda*dg_dF);
	F_dot = (u_dot-u_p_dot)*axis_stiff;
	
	omega_p_dot = abs(lambda*dg_dM);
	M_dot = (omega_dot-omega_p_dot)*bend_stiff;
	Nf_dot = dnf_dup * abs(u_p_dot) + dnf_domegap * abs(omega_p_dot);
	
	
	plastic_increments(1)=F_dot;
	plastic_increments(2)=M_dot;
	plastic_increments(3)=u_dot;
	plastic_increments(4)=omega_dot;
	plastic_increments(5)=abs(Nf_dot);
	plastic_increments(6)=abs(u_p_dot);
	plastic_increments(7)=(u_dot-u_p_dot);
	plastic_increments(8)=abs(omega_p_dot);
	plastic_increments(9)=(omega_dot-omega_p_dot);
	plastic_increments(10) = 0; 
	plastic_increments(11) = df_dF; 
	plastic_increments(12) = df_dM; 
	plastic_increments(13) = dg_dF; 
	plastic_increments(14) = dg_dM; 
	end
	
	function f = yield(state,params)
	F = state(1);
	M = state(2);
	up = state(6);
	omegap = state(8);
	
	MD = M/params.D;
	eta = 1-exp(-params.kf * abs(up)-params.km*abs(omegap));
	
	nf = 2 * (params.ny + eta*(params.nult-params.ny));
	
	Fstar = F+nf/2;
	h = exp(-1/params.beta*(Fstar/nf-0.5)^2);
	
	if F < 0
		MY = params.D * nf * sqrt(params.mf)/2;
	f = MY/params.D * (F^2/(params.fy)^2+M^2/MY^2-1);
	else
	f = MD^2+params.mf*h*(Fstar-nf)*Fstar;
	end
	
	end