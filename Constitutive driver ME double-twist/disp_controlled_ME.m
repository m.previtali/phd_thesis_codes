function updated_state = disp_controlled_ME(params,curr_state,increments)

% parameters
tolerance = 1e-6;


du_tot = increments.deformation;
domega_tot = increments.bending;

du = du_tot;
domega = domega_tot;


plastic = 0;
iter = 1;

while abs(du)<abs(du_tot) || abs(domega) < abs(domega_tot) || iter == 1
myIncrements = elastic_increments(params,[du,domega]);
trial_state = curr_state + [myIncrements,du,domega,0,0,du,0,domega,0,0,0,0,0];
f = yieldf(curr_state,increments,params,0);
ftrial = yieldf(trial_state,increments,params,0);
iter = iter+1;

if curr_state(10) > 0 && ftrial > f %if im loading and alraedy plastic
disp('still plastic, integrating the plasticity equations:')
increments.deformation = du;
increment.bending = domega;

[~,updated_state] = yieldf(curr_state,increments,params,1);


format long
disp('Position of the yield surface after the elastoplastic step: '+ string(yieldf(updated_state,increments,params,0)));
du = du_tot;
domega = domega_tot;
elseif plastic == 0 && ftrial <0
disp(['pure elastic increment, ftrial: ' num2str(ftrial)])
updated_state = trial_state;

% bisection algorithm
elseif plastic == 0 && f < -tolerance && ftrial > tolerance
disp('transition from elastic to plastic')

a0 = 0;
a1 = 1;
j=0;
while abs(ftrial)>tolerance
a = (a0 + a1)/2;
myIncrements = elastic_increments(params,[du,domega]*a);
trial_state = curr_state + [myIncrements,du*a,domega*a,0,0,du*a,0,domega*a,0,0,0,0,0];
ftrial = yieldf(trial_state,increments,params,0);

if ftrial>0
a1 = a;
elseif ftrial <0
a0 = a;
elseif ftrial == 0
break
end
j = j+1;

if j > 1000
error('Did not converge in 1000 iterations')
return
end
end
disp(['yield surface converges at: ' num2str(j) ' iterations'])
curr_state = trial_state;
du = du_tot * (1-a);
domega = domega_tot * (1-a);
plastic = 1;

elseif plastic == 1 || abs(f) < tolerance
disp('plastic increment, integrating the plasticity equations:')
increments.strain = du;
increment.bending = domega;
[~,updated_state] = yieldf(curr_state,increments,params,1);

disp('Position of the yield surface after the elastoplastic step: '+ string(yieldf(updated_state,increments,params,0)));
du = du_tot;
domega = domega_tot;
else
error('The stress state is outside the yield function')
end
end

end

function stress_increments = elastic_increments(params,strain_increments)
stress = strain_increments(1)/params.L * params.E * params.A;
moment = strain_increments(2)/params.L * params.E * params.I;
stress_increments = [stress,moment];
end
