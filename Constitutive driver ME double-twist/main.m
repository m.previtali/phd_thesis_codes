	clearvars
	% number of timesteps
	nsteps = 20000;
	% prescribed displacements
	finalDisp = 1e-2;
	finalRot = 0;

	% model parameters
	params.mf = 1.078e-4;                     % [-]
	params.beta = 2000;                       % [-]
	params.fy = 1510;                         % [N]
	params.fres = 550;                        % [N]
	params.mzero = 0.81;                      % [-]
	params.qzero = 6.09;                      % [-]
	params.mone = 1.54;                       % [-]
	params.qone = 9.04;                       % [-]
	params.ny = 2623;                         % [N]
	params.nult = 3255;                       % [N]
	params.kf = 4916;                         % [1/m]
	params.km = 0.1167;                       % [1/deg]
	params.E = 3.68e11;                       % [N/m^2]
	params.D = 2.7e-3;                        % [m]
	params.A = 5.72e-6;                       % [m^2]
	params.I = 2.0456e-14;                    % [m^4]
	params.L = 0.06;                          % [m]
	params.poisson = 0.3;                     % [-]


	% numerical parameters4plasticity
	params.max_ksub = 1e7;
	params.err_tol = 1e-5;

	%% the idea is to develop a 1D model that fits the experimental data from thoeni et al., i.e. calibrate the hardening function
	his_normal_displacement = linspace(0,finalDisp,nsteps)';
	his_bending_rotation = linspace(0,finalRot,nsteps)';

	%% state
	% 1. N
	% 2. M
	% 3. u
	% 4. omega
	% 5. N_f
	% 6. u_p
	% 7. u_e
	% 8. omega_p
	% 9. omega_e
	% 10. plastity flag
	% 11. df_dN
	% 12. df_dM
	% 13. dg_dN
	% 14. dg_dM

	curr_state = [0,0,0,0,2*params.ny,0,0,0,0,0,0,0,0,0]; %initial state
	mat_state = curr_state; %initialize the results matrix
	for step=2:nsteps

	disp(['Step number: ' num2str(step)]);

	increments.deformation = his_normal_displacement(step)-his_normal_displacement(step-1);
	increments.bending = his_bending_rotation(step)-his_bending_rotation(step-1);

	% populate the current state struct
	[updated_state]=disp_controlled_ME(params,curr_state,increments);

	curr_state = updated_state;
	mat_state(step,:)=updated_state;
	disp('-------------------------------');
	end
	plot_results
