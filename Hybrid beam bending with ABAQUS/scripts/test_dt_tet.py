from abaqusConstants import *
from section import *
from regionToolset import *
import displayGroupMdbToolset as dgm
from part import *
from material import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from optimization import *
from job import *
from sketch import *
from visualization import *
from xyPlot import *
import displayGroupOdbToolset as dgo
from connectorBehavior import *

#my own stuff
import os
import math as ma
import sys
import mesh
finalDisp = 0.0068
myDiskName = "D:/ABAQUS/fortran_double_twist2/"
myMeshSize = 0.0005
myContactFriction = 0.4 #not actually used
vec_fric_vals = [0.0,0.1,0.2,0.3,0.4,0.5]
files = os.listdir(myDiskName+'/geom/')
fric_idx = 1
#for fric_idx in range (0,len(vec_fric_vals)):
for file_idx in range(0,len(files)):
    myContactFriction = 0.3#vec_fric_vals[fric_idx]

    jobname = files[file_idx].replace('.igs','job_'+str(fric_idx)+'_'+str(file_idx))
    myModel = mdb.Model(name='Model-1')
    iges = mdb.openIges(myDiskName+'/geom/'+files[file_idx], msbo=False,
    trimCurve=DEFAULT, scaleFromFile=OFF)
    mdb.models['Model-1'].PartFromGeometryFile(name=files[file_idx].replace('.igs',''),
    geometryFile=iges, combine=False, stitchTolerance=1.0,
    dimensionality=THREE_D, type=DEFORMABLE_BODY, convertToAnalytical=1,
    stitchEdges=1, scale=0.001)
    p = mdb.models['Model-1'].parts[files[file_idx].replace('.igs','')]
    # create the material
    mdb.models['Model-1'].Material(name='Steel')
    mdb.models['Model-1'].materials['Steel'].Plastic(table=((476968005.0955,0),
    (528724512.705,0.0073684),
    (557396829.3992,0.014737),
    (573898026.1635,0.022105),
    (583985239.2015,0.029474),
    (590695632.9586,0.036842),
    (595631472.823,0.044211),
    (599638054.8932,0.051579),
    (603161342.3748,0.058947),
    (606436633.786,0.066316),
    (609588093.3525,0.073684),
    (612681257.4679,0.081053),
    (615750734.0551,0.088421),
    (618814815.1424,0.095789),
    (621883185.6118,0.10316),
    (624960989.8875,0.11053),
    (628050977.2858,0.11789),
    (631154633.7782,0.12526),
    (634272779.0441,0.13263),
    (637405881.4432,0.14)
    ))
    mdb.models['Model-1'].materials['Steel'].Elastic(table=((1.535005224017811e+11, 0.3),
    ))
    mdb.models['Model-1'].materials['Steel'].Density(table=((7.85e3, ), ))

    myName = files[file_idx].replace('.igs','')


    mdb.models['Model-1'].HomogeneousSolidSection(name='sec_STEEL',
    material='Steel', thickness=None)
    p = mdb.models['Model-1'].parts[files[file_idx].replace('.igs','')]
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#3 ]', ), )
    region = p.Set(cells=cells, name='Set-1')
    p = mdb.models['Model-1'].parts[files[file_idx].replace('.igs','')]
    p.SectionAssignment(region=region, sectionName='sec_STEEL', offset=0.0,
    offsetType=MIDDLE_SURFACE, offsetField='',
    thicknessAssignment=FROM_SECTION)
    p1 = mdb.models['Model-1'].parts[files[file_idx].replace('.igs','')]
    a = mdb.models['Model-1'].rootAssembly
    a = mdb.models['Model-1'].rootAssembly
    a.DatumCsysByDefault(CARTESIAN)
    p = mdb.models['Model-1'].parts[files[file_idx].replace('.igs','')]
    a.Instance(name=files[file_idx].replace('.igs','-1'), part=p, dependent=ON)
    mdb.models['Model-1'].StaticStep(name='Step-1', previous='Initial',
    description='huey', timePeriod=10.0, maxNumInc=1000000,
    initialInc=1e-09, minInc=1e-09, maxInc=0.1, nlgeom=ON)

    #meshing
    pickedRegions = c.getSequenceFromMask(mask=('[#3 ]', ), )
    p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    elemType1 = mesh.ElemType(elemCode=C3D20R)
    elemType2 = mesh.ElemType(elemCode=C3D15)
    elemType3 = mesh.ElemType(elemCode=C3D10)
    p = mdb.models['Model-1'].parts[myName]
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#3 ]', ), )
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
    elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD,
    secondOrderAccuracy=ON, distortionControl=DEFAULT)
    p = mdb.models['Model-1'].parts[myName]
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#3 ]', ), )
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
    elemType3))
    p = mdb.models['Model-1'].parts[myName]
    p.generateMesh()
    p = mdb.models['Model-1'].parts[files[file_idx].replace('.igs','')]
    p.seedPart(size=myMeshSize, deviationFactor=0.1, minSizeFactor=0.1)
    p = mdb.models['Model-1'].parts[files[file_idx].replace('.igs','')]
    p.generateMesh()
    a = mdb.models['Model-1'].rootAssembly
    a.regenerate()
    mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(variables=(
    'S', 'PE', 'PEEQ', 'PEMAG', 'LE', 'U', 'RF', 'CF', 'CSTRESS', 'CDISP',
    'COORD'))


    a = mdb.models['Model-1'].rootAssembly
    f1 = a.instances[files[file_idx].replace('.igs','-1')].faces
    faces1 = f1.getSequenceFromMask(mask=('[#24 ]', ), )
    a.Set(faces=faces1, name='Set-1')

    # apply bcs
    mdb.models['Model-1'].DisplacementBC(name='BC-1', createStepName='Step-1',
    region=region, u1=UNSET, u2=1.0, u3=1.0, ur1=UNSET, ur2=UNSET,
    ur3=UNSET, amplitude=UNSET, fixed=OFF, distributionType=USER_DEFINED,
    fieldName='', localCsys=None)


    a = mdb.models['Model-1'].rootAssembly
    f1 = a.instances[files[file_idx].replace('.igs','-1')].faces
    faces1 = f1.getSequenceFromMask(mask=('[#9 ]', ), )

    faces1 = f1.getSequenceFromMask(mask=('[#9 ]', ), )
    region = a.Set(faces=faces1, name='Set-2')
    mdb.models['Model-1'].DisplacementBC(name='BC-2', createStepName='Step-1',
    region=region, u1=UNSET, u2=1.0, u3=1.0, ur1=UNSET, ur2=UNSET,
    ur3=UNSET, amplitude=UNSET, fixed=OFF, distributionType=USER_DEFINED,
    fieldName='', localCsys=None)




    mdb.models['Model-1'].ContactProperty('IntProp-1')
    mdb.models['Model-1'].interactionProperties['IntProp-1'].TangentialBehavior(
    formulation=PENALTY, directionality=ISOTROPIC, slipRateDependency=OFF,
    pressureDependency=OFF, temperatureDependency=OFF, dependencies=0,
    table=((myContactFriction, ), ), shearStressLimit=None, maximumElasticSlip=FRACTION,
    fraction=0.005, elasticSlipStiffness=None)
    mdb.models['Model-1'].interactionProperties['IntProp-1'].NormalBehavior(
    pressureOverclosure=HARD, allowSeparation=ON,
    constraintEnforcementMethod=DEFAULT)
    a = mdb.models['Model-1'].rootAssembly
    s1 = a.instances[files[file_idx].replace('.igs','-1')].faces
    side1Faces1 = s1.getSequenceFromMask(mask=('[#2 ]', ), )
    region1=a.Surface(side1Faces=side1Faces1, name='m_Surf-1')
    a = mdb.models['Model-1'].rootAssembly
    s1 = a.instances[files[file_idx].replace('.igs','-1')].faces
    side1Faces1 = s1.getSequenceFromMask(mask=('[#10 ]', ), )
    region2=a.Surface(side1Faces=side1Faces1, name='s_Surf-1')
    #symmetric master-slave surface contact
    mdb.models['Model-1'].SurfaceToSurfaceContactStd(name='Int-1',
    createStepName='Step-1', master=region1, slave=region2,
    sliding=FINITE, thickness=ON, interactionProperty='IntProp-1',
    adjustMethod=NONE, initialClearance=OMIT, datumAxis=None,
    clearanceRegion=None)
    mdb.models['Model-1'].SurfaceToSurfaceContactStd(name='Int-2',
    createStepName='Step-1', master=region2, slave=region1,
    sliding=FINITE, thickness=ON, interactionProperty='IntProp-1',
    adjustMethod=NONE, initialClearance=OMIT, datumAxis=None,
    clearanceRegion=None)
    # create the job
    mdb.Job(name=jobname, model='Model-1', description='', type=ANALYSIS,
    atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90,
    memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True,
    explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF,
    modelPrint=OFF, contactPrint=OFF, historyPrint=OFF, userSubroutine='',
    scratch='', resultsFormat=ODB, multiprocessingMode=DEFAULT, numCpus=20,
    numDomains=20, numGPUs=1)
    mdb.models['Model-1'].steps['Step-1'].setValues(stabilizationMagnitude=0.0002,
    stabilizationMethod=DAMPING_FACTOR, continueDampingFactors=False,
    adaptiveDampingRatio=None)


    ff = files[file_idx].replace('.igs','')+'TENS_ELASTIC_TET_fric_'+str(myContactFriction)
    mdb.saveAs(pathName=myDiskName+'/models/geom_'+ff)


    print >> sys.__stdout__, 'Model: '+'geom_'+files[file_idx].replace('.igs','')+'_fric_'+str(myContactFriction)+' created, running job'

  #  mdb.saveAs(pathName='C:/marco_previtali/')
    mdb.jobs[jobname].writeInput(consistencyChecking=OFF)
