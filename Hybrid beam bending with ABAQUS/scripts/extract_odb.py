from abaqus import *
from abaqusConstants import *
import __main__

from abaqusConstants import *
from section import *
from regionToolset import *
import displayGroupMdbToolset as dgm
from part import *
from material import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from optimization import *
from job import *
from sketch import *
from visualization import *
from xyPlot import *
import displayGroupOdbToolset as dgo
from connectorBehavior import *
from abaqus import *
from abaqusConstants import *
import __main__

import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
import math as ma
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
#my own stuff
import os
import math as ma
import sys
import numpy as np

myResults = session.openOdb(name='myODB.odb')

print >> sys.__stdout__, 'Simulation end, extracting data..'
print( 'Simulation end, extracting data..')

myNd1 = myResults.rootAssembly.nodeSets['SET-1']
myNd2 = myResults.rootAssembly.nodeSets['SET-2']
fid_complete = open('myResults.txt','w')

data_out = []

num_frames = len(myResults.steps['Step-1'].frames)

session.mdbData.summary()
session.viewports['Viewport: 1'].setValues(
displayedObject=session.odbs['myODB.odb'])
o1 = session.openOdb(name='myODB.odb')

session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
        CONTOURS_ON_DEF, ))
session.viewports['Viewport: 1'].odbDisplay.setFrame(step=0, frame=num_frames )
session.viewports['Viewport: 1'].view.setViewpoint(viewVector=(1, 0, 0),
        cameraUpVector=(0, 0, 1))
session.viewports['Viewport: 1'].view.setProjection(projection=PARALLEL)
session.printToFile(fileName='state', format=PNG, canvasObjects=(
        session.viewports['Viewport: 1'], ))


for frame_idx in range (0,num_frames):
    Forces1 = myResults.steps['Step-1'].frames[frame_idx].fieldOutputs['RF'].getSubset(region=myNd1,position=NODAL).values
    Forces2 = myResults.steps['Step-1'].frames[frame_idx].fieldOutputs['RF'].getSubset(region=myNd2,position=NODAL).values
    Pos1 = myResults.steps['Step-1'].frames[frame_idx].fieldOutputs['COORD'].getSubset(region=myNd1,position=NODAL).values
    Pos2 = myResults.steps['Step-1'].frames[frame_idx].fieldOutputs['COORD'].getSubset(region=myNd2,position=NODAL).values
    for nidx in range(0,len(Forces1)):
        data_out.append([frame_idx,1,nidx, Pos1[nidx].data[0],Pos1[nidx].data[1],Pos1[nidx].data[2], Forces1[nidx].data[0],Forces1[nidx].data[1],Forces1[nidx].data[2]])
    for nidx in range(0,len(Forces2)):
        data_out.append([frame_idx,2,nidx+len(Forces1), Pos2[nidx].data[0],Pos2[nidx].data[1],Pos2[nidx].data[2],Forces2[nidx].data[0],Forces2[nidx].data[1],Forces2[nidx].data[2]])

for row in data_out:
    fid_complete.write(','.join(str(s) for s in row)+'\n')
fid_complete.close()
