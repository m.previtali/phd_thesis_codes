clearvars
close all
clc
fclose('all');

%% run the python script to generate the input file
cd('../scripts')
!abaqus cae nogui=test_dt_tet.py

%% move the input file to its proper folder
cd('..')

files_inp = dir('scripts/DT_CUT_*');
for fidx = 1:length(files_inp)
movefile(['scripts/' files_inp(fidx).name],['input/' files_inp(fidx).name])
nn = files_inp(fidx).name;
nn = nn(1:10);

cd input\
fix_x = false;
keep_obd = true;
peak_deg = 30;
num_points = 20;
vec_u = linspace(0,5e-2,num_points);
vec_t = linspace(peak_deg,0,num_points);
fractions = linspace(0,1,num_points);
wireRad = 1.35e-3;
mat_params = [];
for i = 1:num_points
dy = -cosd(vec_t(i))*wireRad* fractions(num_points-i+1);
dz = -sind(vec_t(i))*wireRad* fractions(num_points-i+1);
dz2 = vec_u(i) * fractions(i);
mat_params(i,:)=[dz,dy,dz2];
end

fid = fopen(files_inp(fidx).name);

mat_nodes = [];
node_set_rot1 = [];
node_set_rot2 = [];

%% get mesh info from the input file
for i = 1:9
fgetl(fid);
end
while ~feof(fid)
l = fgetl(fid);
l = strsplit(l);
if any(contains(l,"*Element"))
break;
else
l = strrep(l,',','');
mat_nodes(end+1,:) = str2double(l);
end
end
% first bc
while ~feof(fid)
l = fgetl(fid);
if any(contains(l,['*Nset, nset=Set-1, instance=' nn '-1']))
break;
end
end

while ~feof(fid)
l = fgetl(fid);
if any(contains(l,"*Elset"))
break;
else
l = strsplit(l);
l = strrep(l,',','');
l = str2double(l);
for i = 1:length(l)
if ~isnan(l(i))
node_set_rot1(end+1)=l(i);
end
end
end
end

% second bc
while ~feof(fid)
l = fgetl(fid);
if any(contains(l,['*Nset, nset=Set-2, instance=' nn '-1']))
break;
end
end
while ~feof(fid)
l = fgetl(fid);
if any(contains(l,"*Elset"))
break;
else
l = strsplit(l);
l = strrep(l,',','');
l = str2double(l);
for i = 1:length(l)
if ~isnan(l(i))
node_set_rot2(end+1)=l(i);
end
end
end
end

fclose('all');
minY = min(mat_nodes(:,4));
maxY = max(mat_nodes(:,4));
minZ = min(mat_nodes(:,5));
maxZ = max(mat_nodes(:,5));
centerY = mean([minY maxY]);
centerZ = mean([minZ maxZ]);
rangeY = (maxY-minY);
rangeZ = (maxZ-minZ);
rad = 1.35e-3;
myTime = 10;
cd ..
for sim_idx = 1:length(mat_params)
cd input\
myDisp = vec_u(sim_idx) ;
myAngle = vec_t(sim_idx);



%% write fortran file
mat_node_sets = [];
fid = fopen('myBCs.f','w');
fprintf(fid,"      SUBROUTINE DISP(U,KSTEP,KINC,TIME,NODE,NOEL,JDOF,COORDS)\n");
fprintf(fid,"      INCLUDE 'ABA_PARAM.INC'\n");
fprintf(fid,"      DIMENSION U(3),TIME(2),COORDS(3)\n");
fprintf(fid,"      AOMGT=TIME(2)\n");
mat_node_sets{1} = node_set_rot1;
mat_node_sets{2} = node_set_rot2;
mySign = [+1 -1];
for bc_idx = 1:2
node_set_rot = mat_node_sets{bc_idx};
ss = mySign(bc_idx);
for nidx = 1:length(node_set_rot)
myId = node_set_rot(nidx);
nodePos = mat_nodes(myId,:);
x = nodePos(3);
y = nodePos(4);
z = nodePos(5);

%value based on pos
dz = (z-centerZ)/2.7e-3;

dispZ = -dz*sind(myAngle)*rad * fractions(num_points-sim_idx+1);
dispY = ss * (myDisp*fractions(sim_idx) + dz*cosd(myAngle)*rad * fractions(num_points-sim_idx+1));
if nidx == 1 && bc_idx == 1
fprintf(fid,['      IF(NODE.EQ.' num2str(myId) '.AND.JDOF.EQ.2) THEN\n']);
else
fprintf(fid,['      ELSE IF(NODE.EQ.' num2str(myId) '.AND.JDOF.EQ.2) THEN\n']);
end
fprintf(fid,['          U(1) = ' num2str(dispY/myTime) '*AOMGT\n']); 
fprintf(fid,['          U(2) = 0\n']); 
fprintf(fid,['          U(3) = 0\n']); 
fprintf(fid,['      ELSE IF(NODE.EQ.' num2str(myId) '.AND.JDOF.EQ.3) THEN\n']); 
fprintf(fid,['          U(1) = ' num2str(dispZ/myTime) '*AOMGT\n']); 
fprintf(fid,['          U(2) = 0\n']); 
fprintf(fid,['          U(3) = 0\n']); 
if fix_x 
fprintf(fid,['      ELSE IF(NODE.EQ.' num2str(myId) '.AND.JDOF.EQ.1) THEN\n']); 
fprintf(fid,['          U(1) = 0\n']); 
fprintf(fid,['          U(2) = 0\n']); 
fprintf(fid,['          U(3) = 0\n']); 
end
end
end

fprintf(fid,'      ENDIF\n');
fprintf(fid,'      RETURN\n');
fprintf(fid,'      END\n');
fclose(fid);
cmd = ['abaqus job=' files_inp(fidx).name ' user=myBCs.f ask_delete=OFF & exit'];
mydlg = system(cmd);
flag = true;
tic

while flag
pause(30) 
t1 = toc;
disp(['EXE ' num2str(sim_idx) ' still running, time elapsed: ' num2str(t1) 's']);   
flag = isprocess('standard.exe'); 
end
disp('Done')
cd ..
movefile(['input/' strrep(files_inp(fidx).name,'inp','odb')],'scripts/myODB.odb','f');
cd scripts
!abaqus cae nogui=extract_odb.py
cd ..
if keep_obd
movefile('scripts/myODB.odb',['odb_files/output_TET_plastic_' files_inp(fidx).name '_' num2str(sim_idx) '.odb'],'f');
end
movefile('scripts/myResults.txt',['ascii_results/output_TET_plastic_' files_inp(fidx).name '_' num2str(sim_idx) '.txt'],'f');
movefile('scripts/state.png',['png_results/output_TET_plastic_' files_inp(fidx).name '_' num2str(sim_idx) '.png'],'f');

end

end
disp('Done done')
%%
function varargout = isprocess(pname)
%ISPROCESS checks if the input process name is running on the system
%The function returns True/False (0/1) along with the number of instances of the
%process and the process ID (PID) numbers.
%
% Syntax/Usage:  [result] = isprocess('fire*')
%                [result pid_total] = isprocess('firefox')
%                [result pid_total pid_nums] = isprocess('firefox')
%
% See also endtask

% Distribution Version 2.0
% Written by Varun Gandhi 13-May-2009

if ispc
if (strcmp(pname(end),'*'))
pname=pname(1:(end-1));
end
str=sprintf('tasklist /SVC /NH /FI "IMAGENAME eq %s*"',pname);
[sys_status,sys_out] = system(str);
if (sys_status == 0)
[matches, start_idx, end_idx, extents, tokens, names, splits] = regexp(sys_out,pname,'ignorecase', 'match');
else
disp(sys_out);
error('Unable to access system processes');

end

if (numel(matches) == 0)

varargout{1} = false;
varargout{2}=0;
varargout{3}='No Matching Process-IDs';
else

for i=1:numel(matches)
match_pid(i) = regexp(splits(i+1), '\d+', 'match', 'once');
pid_nums{i}=str2double(match_pid{i});
varargout{1}=true;
end
varargout{2}=numel(matches);
varargout{3}=pid_nums;
end

end

if isunix
[sys_status , sys_out] = system('ps -A -o cmd');
expr = ['\w*' pname '\w*'];
if (sys_status == 0)
matches = regexp(sys_out,expr,'ignorecase', 'match');
else
disp(sys_out);
error('Unable to access system processes');
end

if (numel(matches) == 0)
varargout{1} = false;
varargout{2}=0;
varargout{3}='No Matching Process-IDs';
else

pgrep_cmd = ['pgrep ' matches{1}];
[sys_status , sys_out] = system(pgrep_cmd);
pid_nums =regexp(sys_out,'\d+','ignorecase', 'match');
varargout{1}=true;
varargout{2}=numel(pid_nums);
varargout{3}=pid_nums;
end

end
end