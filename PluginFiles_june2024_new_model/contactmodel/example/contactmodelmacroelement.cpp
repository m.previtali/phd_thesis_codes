// contactmodellinear.cpp
#include "contactmodelmacroelement.h"

#include "version.txt"
#include "contactmodel/src/contactmodelthermal.h"
#include "fish/src/parameter.h"
#include "utility/src/tptr.h"
#include "shared/src/mathutil.h"
#include "base/src/basetoqt.h"

#include "kernel/interface/iprogram.h"
#include "module/interface/icontact.h"
#include "module/interface/icontactmechanical.h"
#include "module/interface/icontactthermal.h"
#include "module/interface/ifishcalllist.h"
#include "module/interface/ipiece.h"
#include "module/interface/ipiecemechanical.h"

#ifdef macroelement_LIB
#ifdef _WIN32
int __stdcall DllMain(void*, unsigned, void*)
{
    return 1;
}
#endif

extern "C" EXPORT_TAG const char* getName()
{
#if DIM==3
    return "contactmodelmechanical3dmacroelement";
#else
    return "contactmodelmechanical2dmacroelement";
#endif
}

extern "C" EXPORT_TAG unsigned getMajorVersion()
{
    return MAJOR_VERSION;
}

extern "C" EXPORT_TAG unsigned getMinorVersion()
{
    return MINOR_VERSION;
}

extern "C" EXPORT_TAG void* createInstance()
{
    cmodelsxd::ContactModelMacroelement* m = NEWC(cmodelsxd::ContactModelMacroelement());
    return (void*)m;
}
#endif // LINEARPBOND_EXPORTS

namespace cmodelsxd {
    static const quint32 linKnMask = 0x00002; // Base 1!
    static const quint32 linKsMask = 0x00004;
    static const quint32 linFricMask = 0x00008;

    using namespace itasca;

    int ContactModelMacroelement::index_ = -1;
    UInt ContactModelMacroelement::getMinorVersion() const { return MINOR_VERSION; }

    ContactModelMacroelement::ContactModelMacroelement() : inheritanceField_(linKnMask | linKsMask | linFricMask)
        , kn_(20437233929.8759 / 0.04)
        , ks_(20437233929.8759 / (2.0 * (1.3)) / 0.04) //useless
        , fric_(0.0)
        , me_young_(20437233929.8759) // i'm just setting the "correct" values in the constructor as default
        , me_ny_(1939.30752247858)// (301949801.453)
        , me_nlim_(1323.56116111172)
        , me_nbuck_(210.478048327887) // it's the analyical buckling solution multiplied by section area to convert it from stress to force
        , me_length_(0.05)
        , me_radius_(0.00135)
        , me_beta_init_(0.0)
        , me_mf_init_(0.0)
        , me_beta_ult_(0.0)
        , me_mf_ult_(0.0)
        , me_q1_up_(0.0)
        , me_q2_up_(0.0)
        , me_q3_up_(0.0)
        , me_q1_tp_(0.0)
        , me_q2_tp_(0.0)
        , me_q3_tp_(0.0)
        , me_a_up_(0.0)
        , me_b_up_(0.0)
        , me_c_up_(0.0)
        , me_a_tp_(0.0)
        , me_b_tp_(0.0)
        , me_c_tp_(0.0)
        , me_poisson_(0.3)
        , me_sa_(0.000005725552611)
        , me_im_(2.60870490846314E-12)
        , me_nf_(me_ny_)
        , me_theta_p_(0.0)
        , me_u_p_(0.0)
        , me_force_(0.0)
        , me_moment_(0.0)
        , me_interp_(0.0)
        , me_isplastic_(false)
        , me_is_dt_(false)
        //, me_status_.alpha_(0.0)
        , lin_F_(DVect(0.0))
        , lin_S_(false)
        , lin_mode_(0)
        , rgap_(0.0)
        , dpProps_(0)
        , pbProps_(0)
        , userArea_(0)
        , energies_(0)
        , me_energies_(0)
        , effectiveTranslationalStiffness_(DVect2(0.0)) {
        //    setFromParent(ContactModelMechanicalList::instance()->find(getName()));
    }

    ContactModelMacroelement::~ContactModelMacroelement() {
        if (dpProps_)
            delete dpProps_;
        if (pbProps_)
            delete pbProps_;
        if (energies_)
            delete energies_;
        if (me_energies_)
            delete me_energies_;
    }

    void ContactModelMacroelement::archive(ArchiveStream& stream) {
        stream& kn_;
        stream& ks_;
        stream& fric_;
        stream& me_young_;
        stream& me_ny_;
        stream& me_nlim_;
        stream& me_nbuck_;
        stream& me_length_;
        stream& me_radius_;
        stream& me_beta_init_;
        stream& me_mf_init_;
        stream& me_beta_ult_;
        stream& me_mf_ult_;
        stream& me_q1_up_;
        stream& me_q2_up_;
        stream& me_q3_up_;
        stream& me_q1_tp_;
        stream& me_q2_tp_;
        stream& me_q3_tp_;
        stream& me_a_up_;
        stream& me_b_up_;
        stream& me_c_up_;
        stream& me_a_tp_;
        stream& me_b_tp_;
        stream& me_c_tp_;
        stream& me_poisson_;
        stream& me_sa_;
        stream& me_im_;
        stream& me_nf_;
        stream& me_theta_p_;
        stream& me_u_p_;
        stream& me_force_;
        stream& me_moment_;
        stream& me_interp_;
        stream& me_isplastic_;
        stream& me_is_dt_;
        stream& lin_F_;
        stream& lin_S_;
        stream& lin_mode_;

        if (stream.getArchiveState() == ArchiveStream::Save) {
            bool b = false;
            if (dpProps_) {
                b = true;
                stream& b;
                stream& dpProps_->dp_nratio_;
                stream& dpProps_->dp_sratio_;
                stream& dpProps_->dp_mode_;
                stream& dpProps_->dp_F_;
            }
            else
                stream& b;

            b = false;
            if (energies_) {
                b = true;
                stream& b;
                stream& energies_->estrain_;
                stream& energies_->eslip_;
                stream& energies_->edashpot_;
                stream& energies_->epbstrain_;
            }
            else
                stream& b;
            b = false;
            if (me_energies_) {
                b = true;
                stream& me_energies_->me_estrain_;
                stream& me_energies_->me_eplastic_;
                stream& me_energies_->me_ebstrain_;
                stream& me_energies_->me_ebplastic_;
            }
            else
                stream& b;
            b = false;
            if (pbProps_) {
                b = true;
                stream& b;
                stream& pbProps_->pb_state_;
                stream& pbProps_->pb_rmul_;
                stream& pbProps_->pb_kn_;
                stream& pbProps_->pb_ks_;
                stream& pbProps_->pb_mcf_;
                stream& pbProps_->pb_ten_;
                stream& pbProps_->pb_coh_;
                stream& pbProps_->pb_fa_;
                stream& pbProps_->pb_F_;
                stream& pbProps_->pb_M_;
            }
            else
                stream& b;
        }
        else {
            bool b(false);
            stream& b;
            if (b) {
                if (!dpProps_)
                    dpProps_ = NEWC(dpProps());
                stream& dpProps_->dp_nratio_;
                stream& dpProps_->dp_sratio_;
                stream& dpProps_->dp_mode_;
                stream& dpProps_->dp_F_;
            }
            stream& b;
            if (b) {
                if (!energies_)
                    energies_ = NEWC(Energies());
                stream& energies_->estrain_;
                stream& energies_->eslip_;
                stream& energies_->edashpot_;
                stream& energies_->epbstrain_;
            if (!me_energies_)
             me_energies_ = NEWC(meEnergies());
            stream& me_energies_->me_estrain_;
            stream& me_energies_->me_eplastic_;
            stream& me_energies_->me_ebstrain_;
            stream& me_energies_->me_ebplastic_;
        }


            stream& b;
            if (b) {
                if (!pbProps_)
                    pbProps_ = NEWC(pbProps());
                stream& pbProps_->pb_state_;
                stream& pbProps_->pb_rmul_;
                stream& pbProps_->pb_kn_;
                stream& pbProps_->pb_ks_;
                stream& pbProps_->pb_mcf_;
                stream& pbProps_->pb_ten_;
                stream& pbProps_->pb_coh_;
                stream& pbProps_->pb_fa_;
                stream& pbProps_->pb_F_;
                stream& pbProps_->pb_M_;
            }
        }

        stream& inheritanceField_;
        stream& effectiveTranslationalStiffness_;

        if (stream.getArchiveState() == ArchiveStream::Save || stream.getRestoreVersion() == getMinorVersion())
            stream& rgap_;

        if (stream.getArchiveState() == ArchiveStream::Save || stream.getRestoreVersion() > 1)
            stream& userArea_;
    }

    void ContactModelMacroelement::copy(const ContactModel* cm) {
        ContactModelMechanical::copy(cm);
        const ContactModelMacroelement* in = dynamic_cast<const ContactModelMacroelement*>(cm);
        if (!in) throw std::runtime_error("Internal error: contact model dynamic cast failed.");
        kn(in->kn());
        ks(in->ks());
        fric(in->fric());
        me_young(in->me_young());
        me_ny(in->me_ny());
        me_nlim(in->me_nlim());
        me_nbuck(in->me_nbuck());
        me_length(in->me_length());
        me_radius(in->me_radius());
        me_q1_up(in->me_q1_up());
        me_q2_up(in->me_q2_up());
        me_q3_up(in->me_q3_up());
        me_q1_tp(in->me_q1_tp());
        me_q2_tp(in->me_q2_tp());
        me_q3_tp(in->me_q3_tp());
        me_a_up(in->me_a_up());
        me_b_up(in->me_b_up());
        me_c_up(in->me_c_up());
        me_a_tp(in->me_a_tp());
        me_b_tp(in->me_b_tp());
        me_c_tp(in->me_c_tp());
        me_beta_init(in->me_beta_init());
        me_mf_init(in->me_mf_init());
        me_beta_ult(in->me_beta_ult());
        me_mf_ult(in->me_mf_ult());
        me_poisson(in->me_poisson());
        me_sa(in->me_sa());
        me_im(in->me_im());
        me_nf(in->me_nf());
        me_theta_p(in->me_theta_p());
        me_u_p(in->me_u_p());
        me_force(in->me_force());
        me_moment(in->me_moment());
        me_interp(in->me_interp());
        me_isplastic(in->me_isplastic());
        me_is_dt(in->me_is_dt());
        lin_F(in->lin_F());
        lin_S(in->lin_S());
        lin_mode(in->lin_mode());
        rgap(in->rgap());

        if (in->hasDamping()) {
            if (!dpProps_)
                dpProps_ = NEWC(dpProps());
            dp_nratio(in->dp_nratio());
            dp_sratio(in->dp_sratio());
            dp_mode(in->dp_mode());
            dp_F(in->dp_F());
        }
        if (in->hasEnergies()) {
            if (!energies_)
                energies_ = NEWC(Energies());
            estrain(in->estrain());
            eslip(in->eslip());
            edashpot(in->edashpot());
            epbstrain(in->epbstrain());
            me_eelastic(in->me_eelastic());
            me_eplastic(in->me_eplastic());
            me_ebelastic(in->me_ebelastic());
            me_ebplastic(in->me_ebplastic());
        }
        if (in->hasPBond()) {
            if (!pbProps_)
                pbProps_ = NEWC(pbProps());
            pbState(in->pbState());
            pbRmul(in->pbRmul());
            pbKn(in->pbKn());
            pbKs(in->pbKs());
            pbMCF(in->pbMCF());
            pbTen(in->pbTen());
            pbCoh(in->pbCoh());
            pbFA(in->pbFA());
            pbF(in->pbF());
            pbM(in->pbM());
            pbTransStiff(in->pbTransStiff());
            pbAngStiff(in->pbAngStiff());
        }
        userArea_ = in->userArea_;
        inheritanceField(in->inheritanceField());
        effectiveTranslationalStiffness(in->effectiveTranslationalStiffness());
    }

    QVariant ContactModelMacroelement::getProperty(uint i, const IContact* con) const {
        QVariant var;
        switch (i) {
        case kwLinKn:        return kn_;
        case kwLinKs:        return ks_;
        case kwLinFric:      return fric_;
        case kwYoung:		 return me_young_;
        case kwNyield:		 return me_ny_;
        case kwNlim:		 return me_nlim_;
        case kwNbuck:		 return me_nbuck_;
        case kwLength:		 return me_length_;
        case kwRadius:		 return me_radius_;
        case kwQ1up:		 return me_q1_up_;
        case kwQ2up:		 return me_q2_up_;
        case kwQ3up:		 return me_q3_up_;
        case kwQ1tp:		 return me_q1_tp_;
        case kwQ2tp:		 return me_q2_tp_;
        case kwQ3tp:		 return me_q3_tp_;
        case kwAup:			 return me_a_up_;
        case kwBup:			 return me_b_up_;
        case kwCup:			 return me_c_up_;
        case kwAtp:			 return me_a_tp_;
        case kwBtp:			 return me_b_tp_;
        case kwCtp:			 return me_c_tp_;
        case kwBetaInit:	 return me_beta_init_;
        case kwMfInit:		 return me_mf_init_;
        case kwBetaUlt:		 return me_beta_ult_;
        case kwMfUlt:		 return me_mf_ult_;
        case kwPoisson:		 return me_poisson_;
        case kwSA:			 return me_sa_;
        case kwIM:			 return me_im_;
        case kwNf:			 return me_nf_;
        case kwThetaP:		 return me_theta_p_;
        case kwUP:			 return me_u_p_;
        case kwForce:		 return me_force_;
        case kwMoment:		 return me_moment_;
        case kwInterp:		 return me_interp_;
        case kwIsPlastic:	 return me_isplastic_;
        case kwLinMode:      return lin_mode_;
        case kwLinF:         var.setValue(lin_F_); return var;
        case kwLinS:		 return lin_S_;
        case kwRGap:	     return rgap_;

        case kwEmod: {
            const IContactMechanical* c(convert_getcast<IContactMechanical>(con));
            if (c == nullptr) return 0.0;
            double rsq(std::max(c->getEnd1Curvature().y(), c->getEnd2Curvature().y()));
            double rsum(0.0);
            if (c->getEnd1Curvature().y())
                rsum += 1.0 / c->getEnd1Curvature().y();
            if (c->getEnd2Curvature().y())
                rsum += 1.0 / c->getEnd2Curvature().y();
            if (userArea_) {
#ifdef THREED
                rsq = std::sqrt(userArea_ / dPi);
#else
                rsq = userArea_ / 2.0;
#endif        
                rsum = rsq + rsq;
                rsq = 1. / rsq;
            }
#ifdef TWOD             
            return (kn_ * rsum * rsq / 2.0);
#else                     
            return (kn_ * rsum * rsq * rsq) / dPi;
#endif                    
        }
        case kwKRatio:      return (ks_ == 0.0) ? 0.0 : (kn_ / ks_);
        case kwDpNRatio:    return dpProps_ ? dpProps_->dp_nratio_ : 0;
        case kwDpSRatio:    return dpProps_ ? dpProps_->dp_sratio_ : 0;
        case kwDpMode:      return dpProps_ ? dpProps_->dp_mode_ : 0;
        case kwUserArea:    return userArea_;
        case kwDpF: {
            dpProps_ ? var.setValue(dpProps_->dp_F_) : var.setValue(DVect(0.0));
            return var;
        }
        case kwPbState:     return pbProps_ ? pbProps_->pb_state_ : 0;
        case kwPbRMul:      return pbProps_ ? pbProps_->pb_rmul_ : 1.0;
        case kwPbKn:        return pbProps_ ? pbProps_->pb_kn_ : 0;
        case kwPbKs:        return pbProps_ ? pbProps_->pb_ks_ : 0;
        case kwPbMcf:       return pbProps_ ? pbProps_->pb_mcf_ : 1.0;
        case kwPbTStrength: return pbProps_ ? pbProps_->pb_ten_ : 0.0;
        case kwPbSStrength: {
            if (!pbProps_) return 0.0;
            const IContactMechanical* c(convert_getcast<IContactMechanical>(con));
            double pbArea = pbData(c).x();
            return pbShearStrength(pbArea);
        }
        case kwPbCoh:       return pbProps_ ? pbProps_->pb_coh_ : 0;
        case kwPbFa:        return pbProps_ ? pbProps_->pb_fa_ : 0;
        case kwPbSig: {
            if (!pbProps_ || pbProps_->pb_state_ < 3) return 0.0;
            const IContactMechanical* c(convert_getcast<IContactMechanical>(con));
            return pbSMax(c).x();
        }
        case kwPbTau: {
            if (!pbProps_ || pbProps_->pb_state_ < 3) return 0.0;
            const IContactMechanical* c(convert_getcast<IContactMechanical>(con));
            return pbSMax(c).y();
        }
        case kwPbF: {
            pbProps_ ? var.setValue(pbProps_->pb_F_) : var.setValue(DVect(0.0));
            return var;
        }
        case kwPbM: {
            pbProps_ ? var.setValue(pbProps_->pb_M_) : var.setValue(DAVect(0.0));
            return var;
        }
        case kwPbRadius: {
            if (!pbProps_) return 0.0;
            const IContactMechanical* c(convert_getcast<IContactMechanical>(con));
            double Cmax1 = c->getEnd1Curvature().y();
            double Cmax2 = c->getEnd2Curvature().y();
            double br = pbProps_->pb_rmul_ * 1.0 / std::max(Cmax1, Cmax2);
            if (userArea_)
#ifdef THREED
                br = std::sqrt(userArea_ / dPi);
#else
                br = userArea_ / 2.0;
#endif
            return br;
        }
        case kwPbEmod: {
            if (!pbProps_) return 0.0;
            const IContactMechanical* c(convert_getcast<IContactMechanical>(con));
            double rsum(0.0);
            if (c->getEnd1Curvature().y())
                rsum += 1.0 / c->getEnd1Curvature().y();
            if (c->getEnd2Curvature().y())
                rsum += 1.0 / c->getEnd2Curvature().y();
            if (userArea_) {
#ifdef THREED
                double rad = std::sqrt(userArea_ / dPi);
#else
                double rad = userArea_ / 2.0;
#endif        
                rsum = 2 * rad;
            }
            double emod = pbProps_->pb_kn_ * rsum;
            return emod;
        }
        case kwPbKRatio: {
            if (!pbProps_) return 0.0;
            return (pbProps_->pb_ks_ == 0.0) ? 0.0 : (pbProps_->pb_kn_ / pbProps_->pb_ks_);
        }
        }
       // assert(0);
        return QVariant();
    }

    bool ContactModelMacroelement::getPropertyGlobal(uint i) const {
        switch (i) {
        case kwLinF:
        case kwDpF:
        case kwPbF:
            return false;
        }
        return true;
    }

    bool ContactModelMacroelement::setProperty(uint i, const QVariant& v, IContact*) {
        pbProps pb;
        dpProps dp;

        switch (i) {
        case kwLinKn: {
            if (!v.canConvert<double>())
                throw Exception("kn must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative kn not allowed.");
            kn_ = val;
            return true;
        }
        case kwLinKs: {
            if (!v.canConvert<double>())
                throw Exception("ks must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative ks not allowed.");
            ks_ = val;
            return true;
        }
        case kwLinFric: {
            if (!v.canConvert<double>())
                throw Exception("fric must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative fric not allowed.");
            fric_ = val;
            return false;
        }



        // my stuff

        case kwYoung: {
            if (!v.canConvert<double>())
                throw Exception("Young modulus must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Young modulus must be a positive value.");
            me_young_ = val;
            return false;
        }
        case kwNyield: {
            if (!v.canConvert<double>())
                throw Exception("Yield force must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Yield force must be a positive value.");
            me_ny_ = val;
            return false;
        }
        case kwNlim: {
            if (!v.canConvert<double>())
                throw Exception("Limit force must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Limit force must be a positive value.");
            me_nlim_ = val;
            return false;
        }
        case kwNbuck: {
            if (!v.canConvert<double>())
                throw Exception("Buckling force must be a double.");
            double val(v.toDouble());
            if (val > 0.0)
                throw Exception("Buckling force must be a negative value.");
            me_nbuck_ = val;
            return false;
        }
        case kwLength: {
            if (!v.canConvert<double>())
                throw Exception("Wire length must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Wire length must be a positive value.");
            me_length_ = val;
            return false;
        }
        case kwRadius: {
            if (!v.canConvert<double>())
                throw Exception("Wire radius must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Young modulus must be a positive value.");
            me_radius_ = val;
            return false;
        }

        case kwBetaInit: {
            if (!v.canConvert<double>())
                throw Exception("Beta_init must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Beta_init must be a positive value.");
            me_beta_init_ = val;
            return false;
        }
        case kwMfInit: {
            if (!v.canConvert<double>())
                throw Exception("Mf_init must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Mf_init parameter must be a positive value.");
            me_mf_init_ = val;
            return false;
        }
        case kwBetaUlt: {
            if (!v.canConvert<double>())
                throw Exception("Beta_ult must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Beta_ult must be a positive value.");
            me_beta_ult_ = val;
            return false;
        }
        case kwMfUlt: {
            if (!v.canConvert<double>())
                throw Exception("Mf_ult must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Mf_ult parameter must be a positive value.");
            me_mf_ult_ = val;
            return false;
        }
        case kwQ1up: {
            if (!v.canConvert<double>())
                throw Exception("q1_up parameter must be a double.");
            double val(v.toDouble());
            me_q1_up_ = val;
            return false;
        }
        case kwQ2up: {
            if (!v.canConvert<double>())
                throw Exception("q2_up parameter must be a double.");
            double val(v.toDouble());
            me_q2_up_ = val;
            return false;
        }
        case kwQ3up: {
            if (!v.canConvert<double>())
                throw Exception("q3_up parameter must be a double.");
            double val(v.toDouble());
            me_q3_up_ = val;
            return false;
        }
        case kwQ1tp: {
            if (!v.canConvert<double>())
                throw Exception("q1_tp parameter must be a double.");
            double val(v.toDouble());
            me_q1_tp_ = val;
            return false;
        }
        case kwQ2tp: {
            if (!v.canConvert<double>())
                throw Exception("q2_tp parameter must be a double.");
            double val(v.toDouble());
            me_q2_tp_ = val;
            return false;
        }
        case kwQ3tp: {
            if (!v.canConvert<double>())
                throw Exception("q3_tp parameter must be a double.");
            double val(v.toDouble());
            me_q3_tp_ = val;
            return false;
        }
        case kwAup: {
            if (!v.canConvert<double>())
                throw Exception("a_up must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("a_up must be a positive value.");
            me_a_up_ = val;
            return false;
        }
        case kwBup: {
            if (!v.canConvert<double>())
                throw Exception("b_up must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("b_up must be a positive value.");
            me_b_up_ = val;
            return false;
        }
        case kwCup: {
            if (!v.canConvert<double>())
                throw Exception("c_up must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("c_up must be a positive value.");
            me_c_up_ = val;
            return false;
        }
        case kwAtp: {
            if (!v.canConvert<double>())
                throw Exception("a_tp must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("a_tp must be a positive value.");
            me_a_tp_ = val;
            return false;
        }
        case kwBtp: {
            if (!v.canConvert<double>())
                throw Exception("b_tp must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("b_tp must be a positive value.");
            me_b_tp_ = val;
            return false;
        }
        case kwCtp: {
            if (!v.canConvert<double>())
                throw Exception("c_tp must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("c_tp must be a positive value.");
            me_c_tp_ = val;
            return false;
        }

        case kwPoisson: {
            if (!v.canConvert<double>())
                throw Exception("Poisson ratio must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Poisson ratio must be a positive value.");
            if (val == 1.0)
                throw Exception("Poisson ratio must differ from 1.");
            me_poisson_ = val;
            return false;
        }
        case kwSA: {
            if (!v.canConvert<double>())
                throw Exception("The wire section area must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("The wire section area must be a positive value.");
            me_sa_ = val;
            return false;
        }
        case kwIM: {
            if (!v.canConvert<double>())
                throw Exception("The wire second M of inertia must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("The wire second M of inertia must be a positive value.");
            me_im_ = val;
            return false;
        }

        case kwLinF: {
            if (!v.canConvert<DVect>())
                throw Exception("lin_force must be a vector.");
            DVect val(v.value<DVect>());
            lin_F_ = val;
            return false;
        }
        case kwLinMode: {
            if (!v.canConvert<uint>())
                throw Exception("lin_mode must be 0 (absolute) or 1 (incremental).");
            uint val(v.toUInt());
            if (val > 1)
                throw Exception("lin_mode must be 0 (absolute) or 1 (incremental).");
            lin_mode_ = val;
            return false;
        }
        case kwRGap: {
            if (!v.canConvert<double>())
                throw Exception("Reference gap must be a double.");
            double val(v.toDouble());
            rgap_ = val;
            return false;
        }
        case kwPbRMul: {
            if (!v.canConvert<double>())
                throw Exception("pb_rmul must be a double.");
            double val(v.toDouble());
            if (val <= 0.0)
                throw Exception("pb_rmul must be positive.");
            if (val == 1.0 && !pbProps_)
                return false;
            if (!pbProps_)
                pbProps_ = NEWC(pbProps());
            pbProps_->pb_rmul_ = val;
            return false;
        }
        case kwPbKn: {
            if (!v.canConvert<double>())
                throw Exception("pb_kn must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative pb_kn not allowed.");
            if (val == 0.0 && !pbProps_)
                return false;
            if (!pbProps_)
                pbProps_ = NEWC(pbProps());
            pbProps_->pb_kn_ = val;
            return true;
        }
        case kwPbKs: {
            if (!v.canConvert<double>())
                throw Exception("pb_ks must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative pb_ks not allowed.");
            if (val == 0.0 && !pbProps_)
                return false;
            if (!pbProps_)
                pbProps_ = NEWC(pbProps());
            pbProps_->pb_ks_ = val;
            return true;
        }
        case kwPbMcf: {
            if (!v.canConvert<double>())
                throw Exception("pb_mcf must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative pb_mcf not allowed.");
            if (val > 1.0)
                throw Exception("pb_mcf must be lower or equal to 1.0.");
            if (val == 1.0 && !pbProps_)
                return false;
            if (!pbProps_)
                pbProps_ = NEWC(pbProps());
            pbProps_->pb_mcf_ = val;
            return false;
        }
        case kwPbTStrength: {
            if (!v.canConvert<double>())
                throw Exception("pb_ten must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative pb_ten not allowed.");
            if (val == 0.0 && !pbProps_)
                return false;
            if (!pbProps_)
                pbProps_ = NEWC(pbProps());
            pbProps_->pb_ten_ = val;
            return false;
        }
        case kwPbCoh: {
            if (!v.canConvert<double>())
                throw Exception("pb_coh must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative pb_coh not allowed.");
            if (val == 0.0 && !pbProps_)
                return false;
            if (!pbProps_)
                pbProps_ = NEWC(pbProps());
            pbProps_->pb_coh_ = val;
            return false;
        }
        case kwPbFa: {
            if (!v.canConvert<double>())
                throw Exception("pb_fa must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative pb_fa not allowed.");
            if (val >= 90.0)
                throw Exception("pb_fa must be lower than 90.0 degrees.");
            if (val == 0.0 && !pbProps_)
                return false;
            if (!pbProps_)
                pbProps_ = NEWC(pbProps());
            pbProps_->pb_fa_ = val;
            return false;
        }
        case kwPbF: {
            if (!v.canConvert<DVect>())
                throw Exception("pb_force must be a vector.");
            DVect val(v.value<DVect>());
            if (val.fsum() == 0.0 && !pbProps_)
                return false;
            if (!pbProps_)
                pbProps_ = NEWC(pbProps());
            pbProps_->pb_F_ = val;
            return false;
        }
        case kwPbM: {
            DAVect val(0.0);
#ifdef TWOD               
            if (!v.canConvert<DAVect>() && !v.canConvert<double>())
                throw Exception("pb_moment must be an angular vector.");
            if (v.canConvert<DAVect>())
                val = DAVect(v.value<DAVect>());
            else
                val = DAVect(v.toDouble());
#else
            if (!v.canConvert<DAVect>() && !v.canConvert<DVect>())
                throw Exception("pb_moment must be an angular vector.");
            if (v.canConvert<DAVect>())
                val = DAVect(v.value<DAVect>());
            else
                val = DAVect(v.value<DVect>());
#endif
            if (val.fsum() == 0.0 && !pbProps_)
                return false;
            if (!pbProps_)
                pbProps_ = NEWC(pbProps());
            pbProps_->pb_M_ = val;
            return false;
        }
        case kwDpNRatio: {
            if (!v.canConvert<double>())
                throw Exception("dp_nratio must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative dp_nratio not allowed.");
            if (val == 0.0 && !dpProps_)
                return false;
            if (!dpProps_)
                dpProps_ = NEWC(dpProps());
            dpProps_->dp_nratio_ = val;
            return true;
        }
        case kwDpSRatio: {
            if (!v.canConvert<double>())
                throw Exception("dp_sratio must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative dp_sratio not allowed.");
            if (val == 0.0 && !dpProps_)
                return false;
            if (!dpProps_)
                dpProps_ = NEWC(dpProps());
            dpProps_->dp_sratio_ = val;
            return true;
        }
        case kwDpMode: {
            if (!v.canConvert<int>())
                throw Exception("The viscous mode dp_mode must be 0, 1, 2, or 3.");
            int val(v.toInt());
            if (val == 0 && !dpProps_)
                return false;
            if (val < 0 || val > 3)
                throw Exception("The viscous mode dp_mode must be 0, 1, 2, or 3.");
            if (!dpProps_)
                dpProps_ = NEWC(dpProps());
            dpProps_->dp_mode_ = val;
            return false;
        }
        case kwDpF: {
            if (!v.canConvert<DVect>())
                throw Exception("dp_force must be a vector.");
            DVect val(v.value<DVect>());
            if (val.fsum() == 0.0 && !dpProps_)
                return false;
            if (!dpProps_)
                dpProps_ = NEWC(dpProps());
            dpProps_->dp_F_ = val;
            return false;
        }
        case kwUserArea: {
            if (!v.canConvert<double>())
                throw Exception("user_area must be a double.");
            double val(v.toDouble());
            if (val < 0.0)
                throw Exception("Negative user_area not allowed.");
            userArea_ = val;
            return true;
        }
        }
        //    assert(0);
        return false;
    }

    bool ContactModelMacroelement::getPropertyReadOnly(uint i) const {
        switch (i) {
        case kwDpF:
        case kwLinS:
        case kwEmod:
        case kwKRatio:
        case kwPbState:
        case kwPbRadius:
        case kwPbSStrength:
        case kwPbSig:
        case kwPbTau:
        case kwPbEmod:
        case kwPbKRatio:
        case kwNf:
        case kwThetaP:
        case kwUP:
        case kwForce:
        case kwInterp:
        case kwIsPlastic:
        case kwIsDT:
            return true;
        default:
            break;
        }
        return false;
    }

    bool ContactModelMacroelement::supportsInheritance(uint i) const {
        switch (i) {
        case kwLinKn:
        case kwLinKs:
        case kwLinFric:
        case kwNyield:
        case kwNlim:
        case kwNbuck:
        case kwLength:
        case kwRadius:
        case kwQ1up:
        case kwQ2up:
        case kwQ3up:
        case kwQ1tp:
        case kwQ2tp:
        case kwQ3tp:
        case kwAup:
        case kwBup:
        case kwCup:
        case kwAtp:
        case kwBtp:
        case kwCtp:
        case kwBetaInit:
        case kwMfInit:
        case kwBetaUlt:
        case kwMfUlt:
            return true;
        default:
            break;
        }
        return false;
    }

    QString  ContactModelMacroelement::getMethodArguments(uint i) const {
        QString def = QString();
        switch (i) {
        case kwDeformability:
            return "emod,kratio";
        case kwPbDeformability:
            return "emod,kratio";
        case kwPbBond:
            return "gap";
        case kwPbUnbond:
            return "gap";
        }
        return def;
    }

    bool ContactModelMacroelement::setMethod(uint i, const QVector<QVariant>& vl, IContact* con) {
        IContactMechanical* c(convert_getcast<IContactMechanical>(con));
        switch (i) {
        case kwDeformability: {
            double emod;
            double krat;
            if (vl.at(0).isNull())
                throw Exception("Argument emod must be specified with method deformability in contact model %1.", getName());
            emod = vl.at(0).toDouble();
            if (emod < 0.0)
                throw Exception("Negative emod not allowed in contact model %1.", getName());
            if (vl.at(1).isNull())
                throw Exception("Argument kratio must be specified with method deformability in contact model %1.", getName());
            krat = vl.at(1).toDouble();
            if (krat < 0.0)
                throw Exception("Negative linear stiffness ratio not allowed in contact model %1.", getName());
            double rsq(std::max(c->getEnd1Curvature().y(), c->getEnd2Curvature().y()));
            double rsum(0.0);
            if (c->getEnd1Curvature().y())
                rsum += 1.0 / c->getEnd1Curvature().y();
            if (c->getEnd2Curvature().y())
                rsum += 1.0 / c->getEnd2Curvature().y();
            if (userArea_) {
#ifdef THREED
                rsq = std::sqrt(userArea_ / dPi);
#else
                rsq = userArea_ / 2.0;
#endif        
                rsum = rsq + rsq;
                rsq = 1. / rsq;
            }
#ifdef TWOD
            kn_ = 2.0 * emod / (rsq * rsum);
#else
            kn_ = dPi * emod / (rsq * rsq * rsum);
#endif
            ks_ = (krat == 0.0) ? 0.0 : kn_ / krat;
            setInheritance(1, false);
            setInheritance(2, false);
            return true;
        }
        case kwPbDeformability: {
            //if (!pbProps_ || pbProps_->pb_state_ != 3) return false;
            double emod;
            double krat;
            if (vl.at(0).isNull())
                throw Exception("Argument emod must be specified with method pb_deformability in contact model %1.", getName());
            emod = vl.at(0).toDouble();
            if (emod < 0.0)
                throw Exception("Negative emod not allowed in contact model %1.", getName());
            if (vl.at(1).isNull())
                throw Exception("Argument kratio must be specified with method pb_deformability in contact model %1.", getName());
            krat = vl.at(1).toDouble();
            if (krat < 0.0)
                throw Exception("Negative parallel bond stiffness ratio not allowed in contact model %1.", getName());
            double rsum(0.0);
            if (c->getEnd1Curvature().y())
                rsum += 1.0 / c->getEnd1Curvature().y();
            if (c->getEnd2Curvature().y())
                rsum += 1.0 / c->getEnd2Curvature().y();
            if (!pbProps_)
                pbProps_ = NEWC(pbProps());
            if (userArea_)
#ifdef THREED
                rsum = 2 * std::sqrt(userArea_ / dPi);
#else
                rsum = 2 * userArea_ / 2.0;
#endif
            pbProps_->pb_kn_ = emod / rsum;
            pbProps_->pb_ks_ = (krat == 0.0) ? 0.0 : pbProps_->pb_kn_ / krat;
            return true;
        }
        case kwPbBond: {
            if (pbProps_ && pbProps_->pb_state_ == 3) return false;
            double mingap = -1.0 * limits<double>::max();
            double maxgap = 0;
            if (vl.at(0).canConvert<Double>())
                maxgap = vl.at(0).toDouble();
            else if (vl.at(0).canConvert<DVect2>()) {
                DVect2 value = vl.at(0).value<DVect2>();
                mingap = value.minComp();
                maxgap = value.maxComp();
            }
            else if (!vl.at(0).isNull())
                throw Exception("gap value %1 not recognized in method bond in contact model %2.", vl.at(0), getName());
            double gap = c->getGap();
            if (gap >= mingap && gap <= maxgap) {
                if (pbProps_)
                    pbProps_->pb_state_ = 3;
                else {
                    pbProps_ = NEWC(pbProps());
                    pbProps_->pb_state_ = 3;
                }
                return true;
            }
            return false;
        }
        case kwPbUnbond: {
            if (!pbProps_ || pbProps_->pb_state_ == 0) return false;
            double mingap = -1.0 * limits<double>::max();
            double maxgap = 0;
            if (vl.at(0).canConvert<double>())
                maxgap = vl.at(0).toDouble();
            else if (vl.at(0).canConvert<DVect2>()) {
                DVect2 value = vl.at(0).value<DVect2>();
                mingap = value.minComp();
                maxgap = value.maxComp();
            }
            else if (!vl.at(0).isNull())
                throw Exception("gap value %1 not recognized in method unbond in contact model %2.", vl.at(0), getName());
            double gap = c->getGap();
            if (gap >= mingap && gap <= maxgap) {
                pbProps_->pb_state_ = 0;
                return true;
            }
            return false;
        }
        case kwArea: {
            if (!userArea_) {
                double rsq(1. / std::max(c->getEnd1Curvature().y(), c->getEnd2Curvature().y()));
#ifdef THREED
                userArea_ = rsq * rsq * dPi;
#else
                userArea_ = rsq * 2.0;
#endif                            
            }
            return true;
        }
        }
        return false;
    }

    double ContactModelMacroelement::getEnergy(uint i) const {
        double ret(0.0);
        if (!energies_)
            return ret;
        switch (i) {
        case kwEStrain:  return energies_->estrain_;
        case kwESlip:    return energies_->eslip_;
        case kwEDashpot: return energies_->edashpot_;
        case kwEPbStrain: return energies_->epbstrain_;
        case kwMeEStrain:  return energies_->me_estrain_;
        case kwMeEPlastic: return energies_->me_eplastic_;
        case kwMeEBStrain: return energies_->me_ebstrain_;
        case kwMeEBPlastic:return energies_->me_ebplastic_;
        }
        assert(0);
        return ret;
    }

    bool ContactModelMacroelement::getEnergyAccumulate(uint i) const {
        switch (i) {
        case kwEStrain:   return false;
        case kwESlip:     return true;
        case kwEDashpot:  return true;
        case kwEPbStrain: return false;
        case kwMeEStrain:  return false;
        case kwMeEPlastic: return true;
        case kwMeEBStrain: return false;
        case kwMeEBPlastic:return true;
        }
        assert(0);
        return false;
    }

    void ContactModelMacroelement::setEnergy(uint i, const double& d) {
        if (!energies_) return;
        switch (i) {
        case kwEStrain:   energies_->estrain_ = d; return;
        case kwESlip:     energies_->eslip_ = d; return;
        case kwEDashpot:  energies_->edashpot_ = d; return;
        case kwEPbStrain: energies_->epbstrain_ = d; return;
        case kwMeEStrain:   energies_->me_estrain_ = d; return;
        case kwMeEPlastic:  energies_->me_eplastic_ = d; return;
        case kwMeEBStrain:  energies_->me_ebstrain_ = d; return;
        case kwMeEBPlastic: energies_->me_ebplastic_ = d; return;
        }
        assert(0);
        return;
    }

    bool ContactModelMacroelement::validate(ContactModelMechanicalState* state, const double&) {
        assert(state);
        const IContactMechanical* c = state->getMechanicalContact();
        assert(c);

        if (state->trackEnergy_)
            activateEnergy();

        if (inheritanceField_ & linKnMask)
            updateKn(c);
        if (inheritanceField_ & linKsMask)
            updateKs(c);
        if (inheritanceField_ & linFricMask)
            updateFric(c);

        updateEffectiveStiffness(state);
        return checkActivity(state->gap_);
    }

    static const QString knstr("kn");
    bool ContactModelMacroelement::updateKn(const IContactMechanical* con) {
        assert(con);
        QVariant v1 = con->getEnd1()->getProperty(knstr);
        QVariant v2 = con->getEnd2()->getProperty(knstr);
        if (!v1.isValid() || !v2.isValid())
            return false;
        double kn1 = v1.toDouble();
        double kn2 = v2.toDouble();
        double val = kn_;
        if (kn1 && kn2)
            kn_ = kn1 * kn2 / (kn1 + kn2);
        else if (kn1)
            kn_ = kn1;
        else if (kn2)
            kn_ = kn2;
        return ((kn_ != val));
    }

    static const QString ksstr("ks");
    bool ContactModelMacroelement::updateKs(const IContactMechanical* con) {
        assert(con);
        QVariant v1 = con->getEnd1()->getProperty(ksstr);
        QVariant v2 = con->getEnd2()->getProperty(ksstr);
        if (!v1.isValid() || !v2.isValid())
            return false;
        double ks1 = v1.toDouble();
        double ks2 = v2.toDouble();
        double val = ks_;
        if (ks1 && ks2)
            ks_ = ks1 * ks2 / (ks1 + ks2);
        else if (ks1)
            ks_ = ks1;
        else if (ks2)
            ks_ = ks2;
        return ((ks_ != val));
    }

    static const QString fricstr("fric");
    bool ContactModelMacroelement::updateFric(const IContactMechanical* con) {
        assert(con);
        QVariant v1 = con->getEnd1()->getProperty(fricstr);
        QVariant v2 = con->getEnd2()->getProperty(fricstr);
        if (!v1.isValid() || !v2.isValid())
            return false;
        double fric1 = std::max(0.0, v1.toDouble());
        double fric2 = std::max(0.0, v2.toDouble());
        double val = fric_;
        fric_ = std::min(fric1, fric2);
        return ((fric_ != val));
    }

    bool ContactModelMacroelement::endPropertyUpdated(const QString& name, const IContactMechanical* c) {
        assert(c);
        QStringList availableProperties = getProperties().simplified().replace(" ", "").split(",", QString::SkipEmptyParts);
        QRegExp rx(name, Qt::CaseInsensitive);
        int idx = availableProperties.indexOf(rx) + 1;
        bool ret = false;

        if (idx <= 0)
            return ret;

        switch (idx) {
        case kwLinKn: { //kn
            if (inheritanceField_ & linKnMask)
                ret = updateKn(c);
            break;
        }
        case kwLinKs: { //ks
            if (inheritanceField_ & linKsMask)
                ret = updateKs(c);
            break;
        }
        case kwLinFric: { //fric
            if (inheritanceField_ & linFricMask)
                updateFric(c);
            break;
        }
        }
        return ret;
    }

    void ContactModelMacroelement::updateEffectiveStiffness(ContactModelMechanicalState* state) {
        DVect2 ret(kn_, ks_);
        // account for viscous damping
        if (dpProps_) {
            DVect2 correct(1.0);
            if (dpProps_->dp_nratio_)
                correct.rx() = sqrt(1.0 + dpProps_->dp_nratio_ * dpProps_->dp_nratio_) - dpProps_->dp_nratio_;
            if (dpProps_->dp_sratio_)
                correct.ry() = sqrt(1.0 + dpProps_->dp_sratio_ * dpProps_->dp_sratio_) - dpProps_->dp_sratio_;
            ret /= (correct * correct);
        }
        effectiveTranslationalStiffness_ = ret;
        if (isBonded()) {
            double Cmin1 = state->end1Curvature_.x();
            double Cmax1 = state->end1Curvature_.y();
            double Cmax2 = state->end2Curvature_.y();
            double dthick = (Cmin1 == 0.0) ? 1.0 : 0.0;
            double br = pbProps_->pb_rmul_ * 1.0 / std::max(Cmax1, Cmax2);
            if (userArea_)
#ifdef THREED
                br = std::sqrt(userArea_ / dPi);
#else
                br = userArea_ / 2.0;
#endif
            double br2 = br * br;
            double pbArea = dthick <= 0.0 ? dPi * br2 : 2.0 * br * dthick;
            double bi = dthick <= 0.0 ? 0.25 * pbArea * br2 : 2.0 * br * br2 * dthick / 3.0;
            pbProps_->pbTransStiff_.rx() = pbProps_->pb_kn_ * pbArea;
            pbProps_->pbTransStiff_.ry() = pbProps_->pb_ks_ * pbArea;
#if DIM==3 
            pbProps_->pbAngStiff_.rx() = pbProps_->pb_ks_ * 2.0 * bi;
            pbProps_->pbAngStiff_.ry() = pbProps_->pb_kn_ * bi;
#endif
            pbProps_->pbAngStiff_.rz() = pbProps_->pb_kn_ * bi;
        }
    }

    double ContactModelMacroelement::pbStrainEnergy() const {
        double ret(0.0);
        if (pbProps_->pb_kn_)
            ret = 0.5 * pbProps_->pb_F_.x() * pbProps_->pb_F_.x() / pbProps_->pbTransStiff_.x();
        if (pbProps_->pb_ks_) {
            DVect tmp = pbProps_->pb_F_;
            tmp.rx() = 0.0;
            double smag2 = tmp.mag2();
            ret += 0.5 * smag2 / pbProps_->pbTransStiff_.y();
        }

#ifdef THREED
        if (pbProps_->pbAngStiff_.x())
            ret += 0.5 * pbProps_->pb_M_.x() * pbProps_->pb_M_.x() / pbProps_->pbAngStiff_.x();
#endif
        if (pbProps_->pbAngStiff_.z()) {
            DAVect tmp = pbProps_->pb_M_;
#ifdef THREED
            tmp.rx() = 0.0;
            double smag2 = tmp.mag2();
#else
            double smag2 = tmp.z() * tmp.z();
#endif
            ret += 0.5 * smag2 / pbProps_->pbAngStiff_.z();
        }
        return ret;
    }


    double ContactModelMacroelement::yieldf(double u_p, double theta_p, double N, double M) {

        me_interp_ = (me_q2_up_ + me_q1_up_ * me_u_p_) / (me_q3_up_ + me_u_p_) + (me_q2_tp_ + me_q1_tp_ * me_theta_p_) / (me_q3_tp_ + me_theta_p_);
        me_interp_ = std::max(0.0, std::min(me_interp_, 1.0));

        double gamma = 0.0;

        if (me_is_dt_) {
            gamma = (me_b_up_ + me_a_up_ * me_u_p_) / (me_c_up_ + me_u_p_) + me_a_tp_ * pow(me_theta_p_, me_b_tp_);
        }
        else {
            gamma = me_c_tp_ + me_c_up_ + me_a_up_ * pow(me_u_p_, me_b_up_) + me_a_tp_ * pow(me_theta_p_, me_b_tp_);
        }
        gamma = std::max(0.0, std::min(gamma, 1.0));
        
        me_nf_ = 2.0 * me_ny_ + 2.0 * gamma * me_nlim_;
        double f;
        double h = exp(-pow(0.5 - (N + me_nf_ / 2.0) / me_nf_, 2.0) / me_beta_ult_);
        double h_init = exp(-pow(0.5 - (N + me_nf_ / 2.0) / me_nf_, 2.0) / me_beta_init_);
        if (N > 0) {
            
            double f1 = me_interp_ * (pow(M, 2.0) + me_mf_ult_ * h * (N - me_nf_ / 2.0) * (N + me_nf_ / 2.0));
            if (abs(f1) < 1.0e-30)
                f1 = 0.0;
            double f2 = -(me_interp_ - 1.0);
            double f3 = (pow(M, 2.0) + me_mf_init_ * h_init * (N - me_nf_ / 2.0) * (N + me_nf_ / 2.0));
            f = f1 + f2 * f3;
        }
        else {
            if (abs(N) < 1.0e-30)
                N = 0.0;
            double M_peak = pow(pow(me_nf_, 2.0) * (me_mf_init_ * h_init + me_mf_ult_ * me_interp_ * h - me_mf_init_ * h_init * me_interp_), 0.5) / 2.0;
            double f_N = pow(N, 2.0) / pow(me_nbuck_, 2.0);
            double f_M = pow(M, 2.0) / pow(M_peak, 2.0);
            f = f_N + f_M - 1.0;
        }
        return f;
    }


    ContactModelMacroelement::systemStatus ContactModelMacroelement::updateStatus(systemStatus myStatus, epIncrements myIncrements) {

        myStatus.nf_ += myIncrements.nf_dot_;
        myStatus.u_p_ += myIncrements.u_p_dot_;
        myStatus.theta_p_ += myIncrements.theta_p_dot_;
        myStatus.moment_ += myIncrements.moment_dot_;
        myStatus.force_ += myIncrements.force_dot_;

        return myStatus;
    }

    ContactModelMacroelement::epIncrements ContactModelMacroelement::epIncProduct(epIncrements myIncrements, double myMult) {
        myIncrements.nf_dot_ *= myMult;
        myIncrements.u_p_dot_ *= myMult;
        myIncrements.theta_p_dot_ *= myMult;
        myIncrements.moment_dot_ *= myMult;
        myIncrements.force_dot_ *= myMult;
        return myIncrements;
    }

    ContactModelMacroelement::epIncrements ContactModelMacroelement::epIncSum(epIncrements myIncrements1, epIncrements myIncrements2) {
        epIncrements newIncrement;
        newIncrement.nf_dot_ = myIncrements1.nf_dot_ + myIncrements2.nf_dot_;
        newIncrement.u_p_dot_ = myIncrements1.u_p_dot_ + myIncrements2.u_p_dot_;
        newIncrement.force_dot_ = myIncrements1.force_dot_ + myIncrements2.force_dot_;
        newIncrement.moment_dot_ = myIncrements1.moment_dot_ + myIncrements2.moment_dot_;
        newIncrement.theta_p_dot_ = myIncrements1.theta_p_dot_ + myIncrements2.theta_p_dot_;

        return newIncrement;
    }


    ContactModelMacroelement::systemStatus ContactModelMacroelement::plasticIncrements(systemStatus curr_state, dfIncrements dfinc) {

        // numerical params and vars
        double tiny = 1.0e-9;// 13;
        double error_tol = 1.0e-6;
        int ksub = 0;
        int max_ksub = 1e6;// e8; // clown emoji

        // init
        double T_j = 0.0;
        double DT_j = 1.0;


        while (T_j < 1.0) {


            ksub++;
            if (ksub > max_ksub) {
                throw Exception("Too many iterations in the plasticity substepping");
            }

            // calculate the approximate low order solutions
            epIncrements first_approximation = evaluatePlasticity(curr_state, dfinc);
            systemStatus updated_state_2nd_order = updateStatus(curr_state, epIncProduct(first_approximation, DT_j / 2.0));
            epIncrements second_approximation = evaluatePlasticity(updated_state_2nd_order, dfinc);
            systemStatus updated_state_3rd_order = updateStatus(curr_state, epIncSum(epIncProduct(first_approximation, -DT_j), epIncProduct(second_approximation, DT_j * 2.0)));
            epIncrements third_approximation = evaluatePlasticity(updated_state_3rd_order, dfinc);

            // form approximate solution of 2nd and 3rd order
            systemStatus state_hat = updateStatus(curr_state, epIncProduct(second_approximation, DT_j));
            systemStatus state_tilde = updateStatus(curr_state, epIncSum(epIncSum(epIncProduct(first_approximation, DT_j * 1.0 / 6.0), epIncProduct(second_approximation, DT_j * 2.0 / 3.0)), epIncProduct(third_approximation, DT_j * 1.0 / 6.0)));


            // calculate the error residuals
            double sig_tilde[]{ state_tilde.force_,state_tilde.moment_ };
            double nf_tilde = state_tilde.nf_;

            double delta_sig[]{ sig_tilde[0] - state_hat.force_,sig_tilde[1] - state_hat.moment_ };
            double delta_nf = nf_tilde - state_hat.nf_;

            // this is equivalent of matlab sqrt(norm_sig * norm_sig'), where norm_sig is an horizontal array. it works okay
            double norm_sig = sqrt(std::inner_product(std::begin(sig_tilde), std::end(sig_tilde), std::begin(sig_tilde), 0.0));
            double norm_nf = abs(nf_tilde);

            if (norm_sig < tiny)
                norm_sig = tiny;
            if (norm_nf < tiny)
                norm_nf = tiny;

            double vec_residual[3]{ 0.0,0.0,0.0 };
            vec_residual[0] = (1.0 / norm_sig) * delta_sig[0];
            vec_residual[1] = (1.0 / norm_sig) * delta_sig[1];
            vec_residual[2] = (1.0 / norm_nf) * delta_nf;
            double norm_Res = sqrt(std::inner_product(std::begin(vec_residual), std::end(vec_residual), std::begin(vec_residual), 0.0));


            if (norm_Res < tiny)
                norm_Res = tiny;

            // tstep
            double NSS = 0.9 * DT_j * pow(error_tol / norm_Res, 1.0 / 3.0);

            if (norm_Res < error_tol) {
                curr_state = state_tilde;
                T_j = T_j + DT_j;
                DT_j = min(4.0 * DT_j, NSS);
                DT_j = min(1.0 - T_j, DT_j);
            }
            else {
                DT_j = max(0.25 * DT_j, NSS);
            }

        }
        systemStatus updated_state = curr_state;
        me_nf(updated_state.nf_);
        me_u_p(updated_state.u_p_);
        me_theta_p(updated_state.theta_p_);
        me_force(updated_state.force_);
        me_moment(updated_state.moment_);
        return updated_state;
    }

    ContactModelMacroelement::epIncrements ContactModelMacroelement::evaluatePlasticity(systemStatus myState, dfIncrements dfinc) {
        // strain increments
        double theta_dot = dfinc.theta_dot_;
        double u_dot = dfinc.u_dot_;


        // wire parameters
        double sa = me_sa_ * 1.0e6;
        double D = sqrt(sa / dPi);
        double im = me_im_ * 1.0e12;
        double L = (myState.gap_ + 2.0 * me_radius_) * 1.0e3;
        double E = me_young_ * 1.0e-6;
        // get the plastic strains. >0 for gamma calc
        double up = std::max(0.0, myState.u_p_);
        double tp = std::max(0.0, myState.theta_p_);


        // get the system state
        double N = myState.force_;
        double M = myState.moment_;

        double alpha = 0.5;
        double fbuck = me_nbuck_;


        double axis_stiff = E / L * sa;
        double bend_stiff = E / L * im;

        // define Nf derivatives
        double dNf_dup = 1.0;
        double dNf_dtp = 1.0;
        double gamma = 0.0;

        if (me_is_dt_) {
            dNf_dup = 2.0 * me_nlim_ * (me_a_up_ / (me_c_up_ + up) - (me_b_up_ + me_a_up_ * up) / pow(me_c_up_ + up, 2.0));
            dNf_dtp = 2.0 * me_a_tp_ * me_b_tp_ * me_nlim_ * pow(tp, me_b_tp_ - 1.0);
            gamma = (me_b_up_ + me_a_up_ * up) / (me_c_up_ + up) + me_a_tp_ * pow(tp, me_b_tp_);
        }
        else
        {
            dNf_dup = 2.0 * me_a_up_ * me_b_up_ * me_nlim_ * pow(up, me_b_up_ - 1.0);
            dNf_dtp = 2.0 * me_a_tp_ * me_b_tp_ * me_nlim_ * pow(tp, me_b_tp_ - 1.0);
            gamma = me_c_tp_ + me_c_up_ + me_a_tp_ * pow(tp, me_b_tp_) + me_a_up_ * pow(up, me_b_up_);
        }
        
        gamma = std::max(0.0, std::min(gamma, 1.0));
        if (gamma == 1.0) {
            dNf_dup = 0.0;
            dNf_dtp = 0.0;
        }
        me_nf_ = 2.0 * me_ny_ + 2.0 * gamma * me_nlim_;




        double h = exp(-pow(0.5 - (N + me_nf_ / 2.0) / me_nf_, 2.0) / me_beta_ult_);
        double h_init = exp(-pow(0.5 - (N + me_nf_ / 2.0) / me_nf_, 2.0) / me_beta_init_);

        me_interp_ = (me_q2_up_ + me_q1_up_ * me_u_p_) / (me_q3_up_ + me_u_p_) + (me_q2_tp_ + me_q1_tp_ * me_theta_p_) / (me_q3_tp_ + me_theta_p_);
        me_interp_ = std::max(0.0, std::min(me_interp_, 1.0));

        double dint_dup = me_q1_up_ / (me_q3_up_ + up) - (me_q2_up_ + me_q1_up_ * up) / pow(me_q3_up_ + up, 2.0);
        double dint_dtp = me_q1_tp_ / (me_q3_tp_ + tp) - (me_q2_tp_ + me_q1_tp_ * tp) / pow(me_q3_tp_ + tp, 2.0);
        if (me_interp_ == 1.0) {
            dint_dup = 0.0;
            dint_dtp = 0.0;
        }
        double df_dN = 0.0;
        double df_dM = 0.0;
        double df_dNf = 0.0;
        double df_dint = 0.0;

        if (N > 0) {
            df_dN = me_interp_ * (me_mf_ult_ * h * (N - me_nf_ / 2.0) + me_mf_ult_ * h * (N + me_nf_ / 2.0)) - (me_interp_ - 1.0) * (me_mf_init_ * h_init * (N - me_nf_ / 2.0) + me_mf_init_ * h_init * (N + me_nf_ / 2.0));
            df_dM = 2.0 * M * me_interp_ - 2.0 * M * (me_interp_ - 1.0);
            df_dNf = me_interp_ * ((me_mf_ult_ * h * (N - me_nf_ / 2.0)) / 2.0 - (me_mf_ult_ * h * (N + me_nf_ / 2.0)) / 2.0) - ((me_mf_init_ * h_init * (N - me_nf_ / 2.0)) / 2.0 - (me_mf_init_ * h_init * (N + me_nf_ / 2.0)) / 2.0) * (me_interp_ - 1.0);
            df_dint = me_mf_ult_ * h * (N - me_nf_ / 2.0) * (N + me_nf_ / 2.0) - me_mf_init_ * h_init * (N - me_nf_ / 2.0) * (N + me_nf_ / 2.0);
        }
        else
        {
            if (abs(N) < 1.0e-30)
                N = 0.0;
            double M_peak = pow(pow(me_nf_, 2.0) * (me_mf_init_ * h_init + me_mf_ult_ * me_interp_ * h - me_mf_init_ * h_init * me_interp_), 0.5) / 2.0;
            df_dN = (2.0 * N) / pow(me_nbuck_, 2.0);
            df_dM = (2.0 * M) / pow(M_peak, 2.0);//(pow(me_nf_, 2.0) * (me_mf_init_ * h_init + me_mf_ult_ * h * me_interp_ - me_mf_init_ * h_init * me_interp_));
            df_dNf = -(8.0 * pow(M, 2.0)) / (pow(me_nf_, 3.0) * (me_mf_init_ * h_init + me_mf_ult_ * h * me_interp_ - me_mf_init_ * h_init * me_interp_));
            double df_dint_num = -(4.0 * pow(M, 2.0) * (me_mf_ult_ * h - me_mf_init_ * h_init));
            double df_dint_denom =  (pow(me_nf_, 2.0) * pow(me_mf_init_ * h_init + me_mf_ult_ * h * me_interp_ - me_mf_init_ * me_interp_ * h_init, 2.0));
            df_dint = df_dint_num / df_dint_denom;
        }

        double dg_dN = df_dN;
        double dg_dM = df_dM;
        if (isinf(dNf_dup)) {
            dNf_dup = 1.0e9;
        }
        if (isinf(dNf_dtp)) {
            dNf_dtp = 1.0e9;
        }

        double numeratore = (u_dot * df_dN * axis_stiff + theta_dot * df_dM * bend_stiff);
        double denominatore = (dg_dN * df_dN * axis_stiff + dg_dM * df_dM * bend_stiff - df_dNf * (dNf_dup * dg_dN + dNf_dtp * dg_dM) - df_dint * (dint_dup * dg_dN + dint_dtp * dg_dM));
        double lambda = numeratore / denominatore;


        // increments
        epIncrements myIncrements;

        myIncrements.u_p_dot_ = lambda * dg_dN;
        myIncrements.theta_p_dot_ = lambda * dg_dM;
        myIncrements.force_dot_ = E * sa * (u_dot - myIncrements.u_p_dot_) / (L);
        myIncrements.moment_dot_ = E * im * (theta_dot - myIncrements.theta_p_dot_) / (L);
        myIncrements.nf_dot_ = dNf_dup * lambda * dg_dN + dNf_dtp * lambda * dg_dM;

        return myIncrements;

    }



    void ContactModelMacroelement::stressStateEvolution(systemStatus initial_state, DVect trans, DAVect ang, double gap) {

        double tolerance = 5.0e-4;
        int iteration = 1;
        double sign_tension = -1.0;

        // wire parameters
        double sa = me_sa_ * 1e6;
        double im = me_im_ * 1e12;
        double im_polar = im * 2.0;
        double E = me_young_ * 1.0e-6;


        double current_length = 1e3 * (gap + 2.0 * me_radius_);


        double shear_modulus = E / (2.0 * (1.0 + me_poisson_));

        double d_u_tot = trans.x() * 1e3;

        DAVect vec_d_theta_tot = ang;

        double curr_force = sign_tension * (pbProps_->pb_F_.x());
        DAVect vec_curr_moment = pbProps_->pb_M_;


        /////////////////////////////////////////
        ///////////// M CALCULATIONS ////////////
        /////////////////////////////////////////

        double curr_moment = sqrt(vec_curr_moment.y() * vec_curr_moment.y() + vec_curr_moment.z() * vec_curr_moment.z());


        // the direction of the angle increment (i.e. to have negative bending M)
        int sign_yb = (vec_d_theta_tot.y() > 0) - (vec_d_theta_tot.y() < 0);
        int sign_zb = (vec_d_theta_tot.z() > 0) - (vec_d_theta_tot.z() < 0);

        // calculate the sign of the bending increments (i.e. if by bending in a certain direction, the bending M is increasing or not)
        //int sign_yb_inc = ((vec_d_theta_tot.y()*vec_curr_moment.y()) > 0) - ((vec_d_theta_tot.y()*vec_curr_moment.y()) < 0);// NOTE: I AM ASSUMING THE BENDING M HAS OPPOSITE DIRECTION AS THE BEND DIRECTION
        //int sign_zb_inc = ((vec_d_theta_tot.z()*vec_curr_moment.z()) > 0) - ((vec_d_theta_tot.z()*vec_curr_moment.z()) < 0);
        int sign_yb_inc = ((vec_d_theta_tot.y() * vec_curr_moment.y()) < 0) - ((vec_d_theta_tot.y() * vec_curr_moment.y()) > 0);// NOTE: I AM ASSUMING THE BENDING M HAS OPPOSITE DIRECTION AS THE BEND DIRECTION
        int sign_zb_inc = ((vec_d_theta_tot.z() * vec_curr_moment.z()) < 0) - ((vec_d_theta_tot.z() * vec_curr_moment.z()) > 0);


        // I use the signs to see whether the angle increment is overall an increment in bending M or not (i.e. the direction)
        double mysum_bend = (vec_d_theta_tot.y() * vec_d_theta_tot.y() * sign_yb_inc + vec_d_theta_tot.z() * vec_d_theta_tot.z() * sign_zb_inc);
        int sign_sum_bend = (mysum_bend > 0.0) - (mysum_bend < 0.0);

        // this is useless as my sign function SHOULD be safe but might as well add a check
        if (curr_moment == 0.0) {
            sign_yb_inc = 1.0;
            sign_zb_inc = 1.0;
            sign_sum_bend = 1.0;
        }


        // then i calculate the modulus of the overall increment and multiply it by the sign calculated just above to obtain the overall increment
        double d_theta_tot = sqrt(abs(vec_d_theta_tot.y() * vec_d_theta_tot.y() * sign_yb_inc + vec_d_theta_tot.z() * vec_d_theta_tot.z() * sign_zb_inc)) * sign_sum_bend;

        // set the initial increment values for the iteration
        double d_u = d_u_tot;
        double d_theta = d_theta_tot;

        // instantiate status
        //systemStatus updated_state;
        //systemStatus initial_state = curr_state;
        // allocation
        systemStatus curr_state = initial_state;
        systemStatus updated_state;


        while (abs(d_u) < abs(d_u_tot) || abs(d_theta) < abs(d_theta_tot) || iteration == 1) {

            iteration += 1;

            double trial_force = curr_force + E / current_length * sa * d_u; // or minus not sure if we're in compression or tension
            double trial_moment = curr_moment + E / current_length * im * d_theta;
            systemStatus trial_state = curr_state;
            trial_state.force_ = trial_force;
            trial_state.moment_ = trial_moment;

            double f = yieldf(me_u_p_, me_theta_p_, me_force_, me_moment_);
            double ftrial = yieldf(me_u_p_, me_theta_p_, trial_force, trial_moment);

            dfIncrements myIncrements;
            myIncrements.u_dot_ = d_u;
            myIncrements.theta_dot_ = d_theta;
            if (me_isplastic() && f == 0.0 && ftrial < 0) {
                updated_state = trial_state;
                me_isplastic(false);
            }
            else if (me_isplastic() && ftrial > f) {//d_u >= 0 && d_theta >= 0) { // pure plastic increment
                updated_state = plasticIncrements(curr_state, myIncrements);
                d_u = d_u_tot;
                d_theta = d_theta_tot;
                me_isplastic(true);
            }
            else if (!me_isplastic() && ftrial < tolerance) { //pure elastic increment
             //me_force(trial_force);
             //me_M(trial_moment);
                updated_state = trial_state;
                //something something 
            }
            else if (!me_isplastic() && f < -tolerance && ftrial > tolerance) {
                double a0 = 0.0;
                double a1 = 1.0;
                double a = 0.5;
                int ksub = 0;
                int maxksub = 500;
                while (abs(ftrial) > tolerance) {

                    a = (a0 + a1) / 2.0;



                    trial_force = curr_force + E / current_length * sa * d_u * a;
                    trial_moment = curr_moment + E / current_length * im * d_theta * a;


                    ftrial = yieldf(me_u_p(), me_theta_p(), trial_force, trial_moment);


                    if (ftrial > 0.0)
                        a1 = a;
                    else if (ftrial < 0.0)
                        a0 = a;
                    else if (ftrial == 0.0)
                        break;

                    ksub++;
                    if (ksub > maxksub) {
                        throw Exception("Too many iterations while trying to find the intersection with the yieldf");
                    }

                }
                trial_state.force_ = trial_force;
                trial_state.moment_ = trial_moment;
                //me_force(trial_force);
                //me_M(trial_moment);
                curr_state = trial_state;
                d_u = d_u_tot * (1.0 - a);
                d_theta = d_theta_tot * (1.0 - a);
                me_isplastic(true);

            }
            else if (abs(f) < tolerance) {
                updated_state = plasticIncrements(curr_state, myIncrements); // NOTE: NEED TO PUT SOMETHING TO SPLIT THE PLASTIC INCREMENTS IN THE TWO BENDING DIRECTIONS OR MAYBE NOT?
                d_u = d_u_tot;
                d_theta = d_theta_tot;
                me_isplastic(true);
            }
            else if (me_isplastic() && abs(f) > tolerance) {
                updated_state = plasticIncrements(curr_state, myIncrements); // Need to add a drift here
                d_u = d_u_tot;
                d_theta = d_theta_tot;
                me_isplastic(true);
            }
            else if (me_isplastic() && ftrial < 0) { // elastic unloading
                updated_state = trial_state;
                me_isplastic(false);
            }
            else {
                throw Exception("There is an issue with finding the yieldf after plasticity?");
            }


        }



        // i have the updated state in terms of stress increment and bending M increment
        double M_increment = updated_state.moment_ - initial_state.moment_;
        double force_increment = updated_state.force_ - initial_state.force_;
        double thetap_increment = updated_state.theta_p_ - initial_state.theta_p_;
        //double shear_force_increment = - shear_modulus * dshear_eps_tot * sa;

        // so im calculating the portion of total increment to split into the two angles
        double portion_y_bend = sqrt(vec_d_theta_tot.y() * vec_d_theta_tot.y() / abs(vec_d_theta_tot.y() * vec_d_theta_tot.y() * sign_yb_inc + vec_d_theta_tot.z() * vec_d_theta_tot.z() * sign_zb_inc));// *sign_yb_inc;
        double portion_z_bend = sqrt(vec_d_theta_tot.z() * vec_d_theta_tot.z() / abs(vec_d_theta_tot.y() * vec_d_theta_tot.y() * sign_yb_inc + vec_d_theta_tot.z() * vec_d_theta_tot.z() * sign_zb_inc));// *sign_zb_inc;

        if (abs(vec_d_theta_tot[1]) < 1e-26) {
            portion_y_bend = 0.0;
        }
        if (abs(vec_d_theta_tot[2]) < 1e-26) {
            portion_z_bend = 0.0;
        }



        double correction = 1.0;
        double overlap = 0.04 - (1.35e-3 * 2.0);

        correction = -1.0 * overlap / d_u_tot;


        if (correction < 0)
            correction = 1.0;


        DVect u_s = trans;
        u_s.rx() = 0;
        double mydist = (gap + me_radius_ * 2.0);
        double myks = shear_modulus / (current_length);//me_length_;
        DVect sforce = pbProps_->pb_F_ + u_s * myks * correction;
        sforce.rx() = 0.0;


        DAVect vec_M_increment;
        vec_M_increment[0] = ang[0] * shear_modulus * im_polar;
        vec_M_increment[1] = M_increment * portion_y_bend * sign_yb_inc * sign_sum_bend;
        vec_M_increment[2] = M_increment * portion_z_bend * sign_zb_inc * sign_sum_bend;


        pbProps_->pbAngStiff_.rx() = myks * 2.0 * im;
        pbProps_->pbAngStiff_.ry() = E / current_length * im;

        pbProps_->pbAngStiff_.rz() = E / current_length * im;

        DAVect pbMInc = ang * pbProps_->pbAngStiff_ * (-1.0);


        if (thetap_increment == 0)
            pbProps_->pb_M_ += pbMInc;
        else
            pbProps_->pb_M_ += vec_M_increment;


        //lin_F_.x(lin_F_[0] + force_increment);

        ////////////////////////////////////////////////////////////////////////////////////////
        /*
        DVect vec_force_increment;
        vec_force_increment[0] = force_increment;
        vec_force_increment[1] = shear_force_increment * portion_y_shear * sign_ys_inc * sign_ys;
        vec_force_increment[2] = shear_force_increment * portion_z_shear * sign_zs_inc * sign_zs;
        */
        //DVect newForce = lin_F_ + vec_force_increment;
        //sforce[1] = 0; //no friction
        //sforce[2] = 0;
        DVect force_inc;
        force_inc.rx() = sign_tension * force_increment;
        force_inc.ry() = u_s.y() * myks * me_sa_;
        force_inc.rz() = u_s.z() * myks * me_sa_;
        pbProps_->pb_F_ += force_inc;

        ///////////////////////////////////////////////////////////////////////////////
        // I DO NOT UNDERSTAND: SHEAR FORCE IS BOTH IN THE LINEAR MODEL AND THE BOND. IN THE LATTER ITS MULTIPLIED BY CONTACT AREA


        me_force(sign_tension * (pbProps_->pb_F_.x()));
        me_moment(updated_state.moment_);
        //me_u_p(updated_state.u_p_);
        //me_theta_p(updated_state.theta_p_);
        //me_alpha(updated_state.alpha_);
    }


    bool ContactModelMacroelement::forceDisplacementLaw(ContactModelMechanicalState* state, const double& timestep) {
        assert(state);

        double overlap = rgap_ - state->gap_;
        DVect trans = state->relativeTranslationalIncrement_;
        double correction = 1.0;


        DVect old_lin_F = lin_F();
        DAVect old_pb_M_ = pbProps_->pb_M_;
        DAVect old_pb_F_ = pbProps_->pb_F_;
        DAVect ang = state->relativeAngularIncrement_;




        systemStatus myStatus;
        myStatus.nf_ = me_nf();
        myStatus.u_p_ = me_u_p();
        myStatus.theta_p_ = me_theta_p();
        myStatus.force_ = me_force();
        myStatus.moment_ = me_moment();
        myStatus.gap_ = state->gap_;

        double previous_force = me_force();
        double previous_u_p_ = me_u_p();
        double previous_moment = me_moment();
        double previous_theta_p_ = me_theta_p();

        pbProps_->pb_M_ = old_pb_M_;
        pbProps_->pb_F_ = old_pb_F_;
        lin_F_ = old_lin_F;
        double gap = state->gap_;
        stressStateEvolution(myStatus, trans, ang, gap);

        if (state->trackEnergy_) {
            assert(energies_);
            energies_->me_estrain_ = -me_force() * me_force() * 0.5 / me_young_; // maybe need to account for section area and inertia M?
            energies_->me_eplastic_ -= previous_force * me_force() * (me_u_p() - previous_u_p_) * 0.5;
            energies_->me_ebstrain_ = me_moment() * me_moment() * 0.5 / me_young_;
            energies_->me_ebplastic_ += previous_moment * me_moment() * (me_theta_p() - previous_theta_p_) * 0.5;
        }


        if (state->trackEnergy_) {
            assert(energies_);
        }


        assert(lin_F_ == lin_F_);
        return checkActivity(state->gap_);
    }

    bool ContactModelMacroelement::thermalCoupling(ContactModelMechanicalState* ms, ContactModelThermalState* ts, IContactThermal* ct, const double&) {
        bool ret = false;
        if (!pbProps_) return ret;
        if (pbProps_->pb_state_ < 3) return ret;
        int idx = ct->getModel()->getContactModel()->isProperty("thexp");
        if (idx <= 0) return ret;

        double thexp = (ct->getModel()->getContactModel()->getProperty(idx)).toDouble();
        double length = ts->length_;
        double delTemp = ts->tempInc_;
        double delUn = length * thexp * delTemp;
        if (delUn == 0.0) return ret;

        double dthick = 0.0;
        double Cmin1 = ms->end1Curvature_.x();
        double Cmax1 = ms->end1Curvature_.y();
        double Cmin2 = ms->end2Curvature_.x();
        double Cmax2 = ms->end2Curvature_.y();

        Cmin2;
        if (Cmin1 == 0.0)
            dthick = 1.0;

        double br = pbProps_->pb_rmul_ * 1.0 / std::max(Cmax1, Cmax2);
        if (userArea_)
#ifdef THREED
            br = std::sqrt(userArea_ / dPi);
#else
            br = userArea_ / 2.0;
#endif
        double br2 = br * br;
        double pbArea = dthick <= 0.0 ? dPi * br2 : 2.0 * br * dthick;
        //
        DVect finc(0.0);
        finc.rx() = 1.0;
        finc *= pbProps_->pb_kn_ * pbArea * delUn;
        pbProps_->pb_F_ += finc;

        //ms->force_ += finc;

        // The state force has been updated - update the state with the resulting torques
        //ms->getMechanicalContact()->updateResultingTorquesLocal(ms->force_,&ms->momentOn1_,&ms->momentOn2_);

        ret = true;
        return ret;
    }

    void ContactModelMacroelement::setForce(const DVect& v, IContact* c) {
        lin_F(v);
        if (v.x() > 0)
            rgap_ = c->getGap() + v.x() / kn_;
    }

    void ContactModelMacroelement::propagateStateInformation(IContactModelMechanical* old, const CAxes& oldSystem, const CAxes& newSystem) {
        // Only do something if the contact model is of the same type
        if (old->getContactModel()->getName().compare("macroelement", Qt::CaseInsensitive) == 0 && !isBonded()) {
            ContactModelMacroelement* oldCm = (ContactModelMacroelement*)old;
#ifdef THREED
            // Need to rotate just the shear component from oldSystem to newSystem

            // Step 1 - rotate oldSystem so that the normal is the same as the normal of newSystem
            DVect axis = oldSystem.e1() & newSystem.e1();
            double c, ang, s;
            DVect re2;
            if (!checktol(axis.abs().maxComp(), 0.0, 1.0, 1000)) {
                axis = axis.unit();
                c = oldSystem.e1() | newSystem.e1();
                if (c > 0)
                    c = std::min(c, 1.0);
                else
                    c = std::max(c, -1.0);
                ang = acos(c);
                s = sin(ang);
                double t = 1. - c;
                DMatrix<3, 3> rm;
                rm.get(0, 0) = t * axis.x() * axis.x() + c;
                rm.get(0, 1) = t * axis.x() * axis.y() - axis.z() * s;
                rm.get(0, 2) = t * axis.x() * axis.z() + axis.y() * s;
                rm.get(1, 0) = t * axis.x() * axis.y() + axis.z() * s;
                rm.get(1, 1) = t * axis.y() * axis.y() + c;
                rm.get(1, 2) = t * axis.y() * axis.z() - axis.x() * s;
                rm.get(2, 0) = t * axis.x() * axis.z() - axis.y() * s;
                rm.get(2, 1) = t * axis.y() * axis.z() + axis.x() * s;
                rm.get(2, 2) = t * axis.z() * axis.z() + c;
                re2 = rm * oldSystem.e2();
            }
            else
                re2 = oldSystem.e2();
            // Step 2 - get the angle between the oldSystem rotated shear and newSystem shear
            axis = re2 & newSystem.e2();
            DVect2 tpf;
            DMatrix<2, 2> m;
            if (!checktol(axis.abs().maxComp(), 0.0, 1.0, 1000)) {
                axis = axis.unit();
                c = re2 | newSystem.e2();
                if (c > 0)
                    c = std::min(c, 1.0);
                else
                    c = std::max(c, -1.0);
                ang = acos(c);
                if (!checktol(axis.x(), newSystem.e1().x(), 1.0, 100))
                    ang *= -1;
                s = sin(ang);
                m.get(0, 0) = c;
                m.get(1, 0) = s;
                m.get(0, 1) = -m.get(1, 0);
                m.get(1, 1) = m.get(0, 0);
                tpf = m * DVect2(oldCm->lin_F_.y(), oldCm->lin_F_.z());
            }
            else {
                m.get(0, 0) = 1.;
                m.get(0, 1) = 0.;
                m.get(1, 0) = 0.;
                m.get(1, 1) = 1.;
                tpf = DVect2(oldCm->lin_F_.y(), oldCm->lin_F_.z());
            }
            DVect pforce = DVect(0, tpf.x(), tpf.y());
#else
            oldSystem;
            newSystem;
            DVect pforce = DVect(0, oldCm->lin_F_.y());
#endif
            for (int i = 1; i < dim; ++i)
                lin_F_.rdof(i) += pforce.dof(i);
            if (lin_mode_ && oldCm->lin_mode_)
                lin_F_.rx() = oldCm->lin_F_.x();
            oldCm->lin_F_ = DVect(0.0);
            if (dpProps_ && oldCm->dpProps_) {
#ifdef THREED
                tpf = m * DVect2(oldCm->dpProps_->dp_F_.y(), oldCm->dpProps_->dp_F_.z());
                pforce = DVect(oldCm->dpProps_->dp_F_.x(), tpf.x(), tpf.y());
#else
                pforce = oldCm->dpProps_->dp_F_;
#endif
                dpProps_->dp_F_ += pforce;
                oldCm->dpProps_->dp_F_ = DVect(0.0);
            }
            if (oldCm->getEnergyActivated()) {
                activateEnergy();
                energies_->estrain_ = oldCm->energies_->estrain_;
                energies_->eslip_ = oldCm->energies_->eslip_;
                energies_->edashpot_ = oldCm->energies_->edashpot_;
                energies_->epbstrain_ = oldCm->energies_->epbstrain_;
                oldCm->energies_->estrain_ = 0.0;
                oldCm->energies_->edashpot_ = 0.0;
                oldCm->energies_->eslip_ = 0.0;
                oldCm->energies_->epbstrain_ = 0.0;
            }
            rgap_ = oldCm->rgap_;
        }
        assert(lin_F_ == lin_F_);
    }

    void ContactModelMacroelement::setNonForcePropsFrom(IContactModel* old) {
        // Only do something if the contact model is of the same type
        if (old->getName().compare("macroelement", Qt::CaseInsensitive) == 0 && !isBonded()) {
            ContactModelMacroelement* oldCm = (ContactModelMacroelement*)old;
            kn_ = oldCm->kn_;
            ks_ = oldCm->ks_;
            fric_ = oldCm->fric_;
            lin_mode_ = oldCm->lin_mode_;
            rgap_ = oldCm->rgap_;
            userArea_ = oldCm->userArea_;

            if (oldCm->dpProps_) {
                if (!dpProps_)
                    dpProps_ = NEWC(dpProps());
                dpProps_->dp_nratio_ = oldCm->dpProps_->dp_nratio_;
                dpProps_->dp_sratio_ = oldCm->dpProps_->dp_sratio_;
                dpProps_->dp_mode_ = oldCm->dpProps_->dp_mode_;
            }

            if (oldCm->pbProps_) {
                if (!pbProps_)
                    pbProps_ = NEWC(pbProps());
                pbProps_->pb_rmul_ = oldCm->pbProps_->pb_rmul_;
                pbProps_->pb_kn_ = oldCm->pbProps_->pb_kn_;
                pbProps_->pb_ks_ = oldCm->pbProps_->pb_ks_;
                pbProps_->pb_mcf_ = oldCm->pbProps_->pb_mcf_;
                pbProps_->pb_fa_ = oldCm->pbProps_->pb_fa_;
                pbProps_->pb_state_ = oldCm->pbProps_->pb_state_;
                pbProps_->pb_coh_ = oldCm->pbProps_->pb_coh_;
                pbProps_->pb_ten_ = oldCm->pbProps_->pb_ten_;
                pbProps_->pbTransStiff_ = oldCm->pbProps_->pbTransStiff_;
                pbProps_->pbAngStiff_ = oldCm->pbProps_->pbAngStiff_;
            }
        }
    }

    DVect ContactModelMacroelement::getForce(const IContactMechanical*) const {
        DVect ret(lin_F_);
        if (dpProps_)
            ret += dpProps_->dp_F_;
        if (pbProps_)
            ret += pbProps_->pb_F_;
        return ret;
    }

    DAVect ContactModelMacroelement::getMomentOn1(const IContactMechanical* c) const {
        DVect force = getForce(c);
        DAVect ret(0.0);
        if (pbProps_)
            ret = pbProps_->pb_M_;
        c->updateResultingTorqueOn1Local(force, &ret);
        return ret;
    }

    DAVect ContactModelMacroelement::getMomentOn2(const IContactMechanical* c) const {
        DVect force = getForce(c);
        DAVect ret(0.0);
        if (pbProps_)
            ret = pbProps_->pb_M_;
        c->updateResultingTorqueOn2Local(force, &ret);
        return ret;
    }

    DVect3 ContactModelMacroelement::pbData(const IContactMechanical* c) const {
        double Cmax1 = c->getEnd1Curvature().y();
        double Cmax2 = c->getEnd2Curvature().y();
        double br = pbProps_->pb_rmul_ * 1.0 / std::max(Cmax1, Cmax2);
        if (userArea_)
#ifdef THREED
            br = std::sqrt(userArea_ / dPi);
#else
            br = userArea_ / 2.0;
#endif
        double br2 = br * br;
#ifdef TWOD
        double pbArea = 2.0 * br;
        double bi = 2.0 * br * br2 / 3.0;
#else
        double pbArea = dPi * br2;
        double bi = 0.25 * pbArea * br2;
#endif
        return DVect3(pbArea, bi, br);
    }

    DVect2 ContactModelMacroelement::pbSMax(const IContactMechanical* c) const {
        DVect3 data = pbData(c);
        double pbArea = data.x();
        double bi = data.y();
        double br = data.z();
        /* maximum stresses */
        double dbend = sqrt(pbProps_->pb_M_.y() * pbProps_->pb_M_.y() + pbProps_->pb_M_.z() * pbProps_->pb_M_.z());
        double dtwist = pbProps_->pb_M_.x();
        DVect bfs(pbProps_->pb_F_);
        bfs.rx() = 0.0;
        double dbfs = bfs.mag();
        double nsmax = -(pbProps_->pb_F_.x() / pbArea) + pbProps_->pb_mcf_ * dbend * br / bi;
        double ssmax = dbfs / pbArea + pbProps_->pb_mcf_ * std::abs(dtwist) * 0.5 * br / bi;
        return DVect2(nsmax, ssmax);
    }

    double ContactModelMacroelement::pbShearStrength(const double& pbArea) const {
        if (!pbProps_) return 0.0;
        double sig = -1.0 * pbProps_->pb_F_.x() / pbArea;
        double nstr = pbProps_->pb_state_ > 2 ? pbProps_->pb_ten_ : 0.0;
        return sig <= nstr ? pbProps_->pb_coh_ - std::tan(dDegrad * pbProps_->pb_fa_) * sig
            : pbProps_->pb_coh_ - std::tan(dDegrad * pbProps_->pb_fa_) * nstr;
    }

    void ContactModelMacroelement::setDampCoefficients(const double& mass, double* vcn, double* vcs) {
        *vcn = dpProps_->dp_nratio_ * 2.0 * sqrt(mass * (kn_));
        *vcs = dpProps_->dp_sratio_ * 2.0 * sqrt(mass * (ks_));
    }

} // namespace itascaxd
// EoF